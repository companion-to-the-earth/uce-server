#
# Predeploy script
# Executed in the deploy step of the pipeline
#
# This repository can be accessed through the $CLONE_PATH variable
#

# copy db-setup folder so the image can be built
mkdir -p db-setup
cp -rp "$CLONE_PATH"/db-setup  .

# copy auth alembic folder so volume can be mounted
mkdir -p auth/alembic
cp -rp "$CLONE_PATH"/auth/alembic auth/