#
# Progress indicator
#

ifneq ($(words $(MAKECMDGOALS)),1)
.DEFAULT_GOAL = all
%:
		@$(MAKE) $@ --no-print-directory -rRf $(firstword $(MAKEFILE_LIST))
else
ifndef ECHO
T := $(shell $(MAKE) $(MAKECMDGOALS) --no-print-directory \
      -nrRf $(firstword $(MAKEFILE_LIST)) \
      ECHO="COUNTTHIS" | grep -c "COUNTTHIS")

N := x
C = $(words $N)$(eval N := x $N)
ECHO = echo "`expr " [\`expr $C '*' 100 / $T\`" : '.*\(....\)$$'`%]"
endif


#
# Default target, build everything
#

.PHONY: all
all: check requirements schemas
	@$(ECHO) Done


#
# Build requirements.txt files
#

REQS_IN = $(wildcard */requirements*.in)
REQS_OUT := $(REQS_IN:.in=.txt)

.PHONY: requirements
requirements: $(REQS_OUT)

%.txt: %.in
	@$(ECHO) Building $@
	@pip-compile -q --output-file $@ $<


#
# Build BARE schemas
#

SCHEMAS_IN = $(wildcard */src/schemas/*.bare)
SCHEMAS_OUT := $(SCHEMAS_IN:.bare=.py)

.PHONY: schemas
schemas: $(SCHEMAS_OUT)

%.py: %.bare
	@$(ECHO) Building $@
	@bare $< $@


#
# Check dependencies
#

REQUIRED_BINS := pip-compile bare

.PHONY: check
check:
	$(info Checking dependencies)
	$(foreach bin,$(REQUIRED_BINS),\
		$(if $(shell command -v $(bin) 2> /dev/null),$(info found `$(bin)`),$(error `$(bin)` not installed)))


#
# Remove all build artifacts
#

.PHONY: clean
clean:
	@$(ECHO) Removing requirements...
	@rm -f $(REQS_OUT)
	@$(ECHO) Removing compiled schemas
	@rm -f $(SCHEMAS_OUT)
	@$(ECHO) Done

endif
