# Utrecht Companion To The Earth - auth

## API
### POST user/login
Attempts to login the user with the provided credentials.<br/>
Password should be provided as SHA256 hash of password.<br/>

Returns 200 and JWT if success.<br/>
Returns 400 if invalid request.<br/>
Returns 404 if users/password combination is not found.<br/>
Returns 500 if server error occurred.<br/>

##### Example
Request URL: `api/user/login`<br/>
Request body: `username={USERNAME}&password={PASSWORDHASH}`

### POST user/create
Attempts to create a dummy account which is linked to an email and has a provided authorisation level. Sends an email to provided email address with account register link.<br/>

Returns 200 if success.<br/>
Returns 400 if invalid request.<br/>
Returns 401 if not provided with JWT.<br/>
Returns 403 if not authorised to create accounts.<br/>
Returns 500 if server error occurred.<br/>

#### Example
Request URL: `api/user/create`<br/>
Request header: `Authorization: Bearer {JWT}`<br/>
Request body: `email={EMAIL}&group={GROUP_ID}&auth_lvl={AUTH_LVL}`<br/>

### POST user/register
Attempts to register an account with provided username, password and register\_id. In reality, this updates a dummy account with the new username and password. Password should be sent unhashed. It will get hashed to SHA256 during the registration process.<br/>

Returns 200 if success. (if {REGISER\_ID} could not be invalidated, this is stated in the body of the response)<br/>
Returns 400 if invalid request.<br/>
Returns 403 if {REGISTER\_ID} is not valid.<br/>
Returns 409 if {USERNAME} is in use.<br/>

#### Example
Request URL: `api/user/register`<br/>
Request body: `username={USERNAME}&password={PASSWORD}&register_id={REGISTER_ID}`

### GET user/register
Provides the register form. {REGISTER\_ID} is filled in the form automatically if it is provided in the url.<br/>

Returns HTML form for registering<br/>

#### Example
Request URl: `api/user/register?ver={REGISTER_ID}`

### POST user/update/username
Attempts to update user's username. You need a valid JWT for this. (Currently you need to own the account). You also need the correct password (hashed SHA256).<br/>

Returns 200 and new JWT if success.<br/>
Returns 400 if invalid request.<br/>
Returns 403 if invalid password or invalid JWT (defined in body of response).<br/>
Returns 500 if server error occurred.<br/>

#### Example
Request URL: `api/user/update/username`<br/>
Request header: `Authorization: Bearer {JWT}`<br/>
Request body: `username={OLD_USERNAME}&new_username={NEW_USERNAME}&password={PASSWORD_HASH}`<br/>

### POST user/update/password
Attempts to update user's password. You need a valid JWT for this. (Currently you need to own the account). You also need the correct password (hashed SHA256). New password needs to be hashed aswell.<br/>

Returns 200 if success.<br/>
Returns 400 if invalid request.<br/>
Returns 403 if invalid password or invalid JWT (defined in body of response).<br/>
Returns 500 if server error occurred.<br/>

#### Example
Request URL: `api/user/update/password`<br/>
Request header: `Authorization: Bearer {JWT}`<br/>
Request body: `username={USERNAME}&password={OLD_PASSWORD_HASH}&new_password={NEW_PASSWORD_HASH}`<br/>

### POST user/update/auth\_lvl
Attempts to update user's authorisation level. You need a valid JWT for this. (Minimum of auth\_lvl 3, auth\_lvl 4 is needed to update another account to auth\_lvl 4).<br/>

Returns 200 if success.<br/>
Returns 400 if invalid request.<br/>
Returns 403 if invalid JWT.<br/>
Returns 500 if server error occurred.<br/>

#### Example
Request URL: `api/user/update/auth_lvl`<br/>
Request header: `Authorization: Bearer {JWT}`<br/>
Request body: `username={USERNAME}&auth_lvl={AUTH_LVL}`<br/>

### POST user/validate
Attempts to validate the provided JWT.<br/>

Returns 200 if token is valid.<br/>
Returns 400 if invalid request.<br/>
Returns 401 if token invalid.<br/>

## Libraries
* lib-middleware-connector:<https://gitlab.com/companion-to-the-earth/lib-middleware-connector>
