"""
Flask app initialiser
"""

# disable unnecessary pylint warnings: print_pid and print_tid are used,
# just not in the way pylint expects
# pylint: disable=unused-variable

from os import getpid, environ
from threading import get_ident, Thread
import secrets
from flask import Flask
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from src.auth import auth
from src.users import users
from src import internal_api
from src.log import log

def get_app():
    """
    Flask app initialiser

    :return Flask: the auth flask app
    """
    app = Flask(__name__)
    app.register_blueprint(auth)
    app.register_blueprint(users)

    # we don't need CSRF protection on our end
    app.config['WTF_CSRF_ENABLED'] = False

    pid = getpid()
    log.info("Starting Flask app on process %s", pid)

    engine = create_engine(environ['DB_URI']) # add echo=True for debugging
    session_factory = sessionmaker(bind=engine)
    app.config['session_factory'] = session_factory

    # start internal API listener on seperate thread
    Thread(target=internal_api.run, args=(session_factory,), daemon=True).start()

    @app.before_request
    def print_tid():
        tid = get_ident()
        log.debug("Handling request on thread %s", tid)

    return app

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
