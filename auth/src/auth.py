"""
Auth blueprint, contains routes for api/user and api/token
"""

from flask import Blueprint, request
from src.process import Auth
from src.errors import APIError, BadRequestError, InvalidGrantTypeError, UnauthorizedError
from src.log import log
from src import utils

auth = Blueprint('auth', __name__)

@auth.route('/auth/token', methods=['POST'])
def token_request():
    """
    Token request route, checks request, calls token_login_procedure or
    token_refresh_procedure depending on grant_type
    """
    try:
        grant_type = request.json['grant_type']
    except (KeyError, TypeError):
        raise BadRequestError("No grant_type specified")

    if grant_type == "password":
        try:
            email = request.json['email']
            password = request.json['password']
        except KeyError:
            raise BadRequestError()

        return Auth.token_login_procedure(email, password)

    if grant_type == "refresh_token":
        try:
            session_id = int(request.json['session_id'])
            refresh_token = request.json['refresh_token']
        except (KeyError, ValueError):
            raise BadRequestError()

        return Auth.token_refresh_procedure(session_id, refresh_token)

    raise InvalidGrantTypeError()

@auth.route('/auth/sessions', methods=['GET'])
def get_sessions():
    """
    Get all active sessions for the user
    """
    jwt = utils.get_jwt(request.headers)
    if not jwt:
        raise UnauthorizedError()

    return Auth.get_sessions_procedure(jwt)

@auth.route("/auth/sessions/<session_id>", methods=['DELETE'])
def delete_session(session_id):
    """
    Delete an existing session
    """
    jwt = utils.get_jwt(request.headers)
    if not jwt:
        raise UnauthorizedError()

    try:
        session_id = int(session_id)
    except:
        raise BadRequestError()

    return Auth.revoke_session_procedure(session_id, jwt)

@auth.errorhandler(Exception)
def handle_error(error):
    """
    Flask error handler

    APIErrors are returned to the client, any other exception is logged
    and returned as an internal server error so no internal details are leaked
    """
    if isinstance(error, APIError):
        return utils.simple_json_response(error.status_code, error.message)

    log.error("%s: %s", type(error).__name__, error)

    return ("Internal server error", 500)

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
