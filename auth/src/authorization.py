"""
Module for JWT generation and validation
"""

import datetime
import os
import jwt
from src.log import log

class Authorization:
    """
    Class containing authorization functions
    """

    @classmethod
    def generate_jwt(cls, user_id, auth_time):
        """
        Generate JWT

        :param long user_id: user id for the token
        :param long auth_time: last time the user logged in with a password
        :return tuple: JWT and expires_in
        """
        key = cls.__get_key()
        current_time = datetime.datetime.utcnow()
        expires_in = datetime.timedelta(hours=6) #TODO: make configurable?
        exp_time = current_time + expires_in

        token = jwt.encode({
            'sub': user_id,
            'iss': "https://uce.science.uu.nl", #TODO: Get hostname from config
            'iat': current_time,
            'exp': exp_time,
            'auth_time': auth_time
        },
        key, algorithm='HS256')

        return (token, int(expires_in.total_seconds()))

    @classmethod
    def validate_jwt(cls, token):
        """
        Check if the provided JWT is valid

        :param str token: the JWT to validate
        :return dict: Token payload if the token is valid, None otherwise
        """
        try:
            return cls.__decode_jwt(token)
        except Exception as e:
            log.info("JWT validation failed: %s: %s", type(e).__name__, e)
            return None

    @classmethod
    def __decode_jwt(cls, token):
        """
        Decode the JWT, fails if expired or signature invalid

        :param str token: the JWT to decode
        :return dict: decoded JWT payload
        """
        key = cls.__get_key()
        return jwt.decode(token, key, algorithms='HS256', options={
            "require": ["exp", "iss", "iat"]
        },
        issuer="https://uce.science.uu.nl") # TODO: Get hostname from config

    @staticmethod
    def __get_key():
        """
        Get secret key for JWTs

        :return str: secret key
        """
        return os.environ['JWT_KEY']

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
