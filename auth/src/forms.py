"""Module containing all forms"""

from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, IntegerField
from wtforms.fields.html5 import EmailField
from wtforms.validators import DataRequired, Email, EqualTo, NumberRange

class UserRegistrationForm(FlaskForm):
    """User registration form"""
    class_id = IntegerField('Class ID', validators=[DataRequired()])
    name = StringField('Name', validators=[DataRequired()])
    email = EmailField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired(), EqualTo('verify_password', message="Passwords must match")])
    verify_password = PasswordField('Verify password', validators=[DataRequired()])

class DummyUserCreationForm(FlaskForm):
    """Form used to create a dummy user to be verified later"""
    email = EmailField('Email', validators=[DataRequired(), Email()])
    auth_level = IntegerField('Auth level', validators=[DataRequired(), NumberRange(1, 4)])
    class_id = IntegerField('Class ID', validators=[DataRequired()])
    user_group_id = IntegerField('User group ID')

class UserVerificationForm(FlaskForm):
    """Form used to verify a dummy user"""
    name = StringField('Name', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired(), EqualTo('verify_password', message="Passwords must match")])
    verify_password = PasswordField('Verify password', validators=[DataRequired()])

class PasswordResetForm(FlaskForm):
    """Form used to reset a password"""
    password = PasswordField('Password', validators=[DataRequired(), EqualTo('verify_password', message="Passwords must match")])
    verify_password = PasswordField('Verify password', validators=[DataRequired()])

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
