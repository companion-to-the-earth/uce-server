"""
Internal API for auth service
"""

# disable unnecessary pylint warnings
# pylint: disable=wildcard-import, unused-wildcard-import

from pika import ConnectionParameters
from middleware_connector import middleware
from sqlalchemy.sql import func
from src.schemas.auth import *
from src.authorization import Authorization
from src.models import User
from src.log import log

class InternalAPIError(Exception):
    """
    Generic internal API error class
    """
    error_type = None
    message = None

    def __init__(self, message=None):
        if message:
            self.message = message
        super().__init__(message)

class InvalidRequestError(InternalAPIError):
    """
    InvalidRequestError
    Raised if request is invalid (no schema or missing required parameters)
    """
    error_type = ErrorType.INVALID_REQUEST
    message = "Invalid request"

class InvalidTokenError(InternalAPIError):
    """
    InvalidTokenError
    Raised if the provided access token is invalid
    """
    error_type = ErrorType.INVALID_TOKEN
    message = "Invalid access token"

class InvalidUserIDError(InternalAPIError):
    """
    InvalidUserIDError
    Raised if the provided user id is invalid
    """
    error_type = ErrorType.INVALID_USER_ID
    message = "Invalid user id"

class InternalAPI:
    """
    Internal API class, handles API requests
    """

    def __init__(self, session_factory):
        self.__session_factory = session_factory

    def get_user_info(self, request_body):
        """
        Handles the user info request and returns the user info if the provided
        token or user id is valid

        :param request_body UserInfoRequest: user info request body
        :returns: the user info response
        """
        # at least one of the two needs to be set
        if request_body.access_token is None and request_body.user_id is None:
            raise InvalidRequestError

        if request_body.access_token is not None:
            decoded_token = Authorization.validate_jwt(request_body.access_token)

            if decoded_token is None:
                raise InvalidTokenError

            user_id = decoded_token['sub']
        else:
            user_id = request_body.user_id

        with self.__get_db_session() as session:
            user = session.query(User).get(user_id)

            if user is None:
                raise InvalidUserIDError

            user_info = UserInfo()
            user_info.id = user.id
            user_info.email = user.email
            user_info.name = user.name
            user_info.class_id = user.class_id
            user_info.pin_group_id = user.pin_group_id
            user_info.auth_level = user.auth_level

        response = AuthResponse()
        response.response_type = ResponseType.USER_INFO_RESPONSE
        response.response_body = user_info.pack()

        return response

    def get_class_member_count(self, request_body):
        """
        Handles the class member count request and returns the number of accounts
        associated with the specified class

        :param request_body ClassMemberCountRequest: class member count request body
        :returns: the number of members of the class
        """

        with self.__get_db_session() as session:
            member_count = session.execute(
                session.query(User)
                       .filter(User.class_id==request_body.class_id)
                       .statement.with_only_columns([func.count()])
                       .order_by(None)
            ).scalar()

            response_body = ClassMemberCountResponse()
            response_body.member_count = member_count

        response = AuthResponse()
        response.response_type = ResponseType.CLASS_MEMBER_COUNT_RESPONSE
        response.response_body = response_body.pack()

        return response

    def __get_db_session(self):
        return self.__session_factory.begin()

class InternalAPIMiddleware(middleware.Middleware):
    """
    Internal API for auth service
    """

    def __init__(self, connection_params, session_factory):
        self.api = InternalAPI(session_factory)
        super().__init__(connection_params)

    def on_message_received(self, _ch, _method, properties, body):
        try:
            log.info("Internal API: Handling request from %s", properties.reply_to)
            request = AuthRequest.unpack(body)

            if request.request_type is RequestType.USER_INFO_REQUEST:
                response = self.api.get_user_info(UserInfoRequest.unpack(request.request_body))
            elif request.request_type is RequestType.CLASS_MEMBER_COUNT_REQUEST:
                response = self.api.get_class_member_count(ClassMemberCountRequest.unpack(request.request_body))
            else:
                raise InvalidRequestError

        except InternalAPIError as e:
            response = AuthResponse()
            response.response_type = ResponseType.ERROR_RESPONSE
            response.error_type = e.error_type
            response.error_message = e.message

        except BaseException as e:
            log.error("Internal API: %s, %s", type(e).__name__, e)
            response = AuthResponse()
            response.response_type = ResponseType.ERROR_RESPONSE
            response.error_type = ErrorType.INVALID_REQUEST
            response.error_message = "Invalid request"

        self.publish(properties.reply_to, response.pack(), '', properties.correlation_id)

def run(session_factory):
    """
    Start internal API middleware, this needs to run on a separate thread
    as it blocks and doesn't return
    """
    connection_params = ConnectionParameters(host='middleware')
    api = InternalAPIMiddleware(connection_params, session_factory)

    # restart consume if it crashes, otherwise internal api is unavailable
    # until the server is restarted
    while True:
        try:
            log.info("Internal API: starting consume")
            api.consume('auth', True)
        except BaseException as e:
            log.error("Internal API: %s: %s", type(e).__name__, e)
            log.info("Internal API: restarting")

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
