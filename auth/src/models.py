"""
Database models
"""

from sqlalchemy import Column, BigInteger, Integer, String, CHAR, TIMESTAMP, Boolean, ForeignKey
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class User(Base):
    """
    User

    id           - random 64-bit user id
    email        - email used for login
    password     - password used for login
    name         - user display name, not used for login, doesn't need to be unique
    class_id     - id of the class the user belongs to, NULL for admin accounts
    pin_group_id - id of the user's pin group
    auth_level   - user's authorization level, see documentation for specific levels
    auth_time    - timestamp of the last time the user logged in with email and password
    """
    __tablename__ = "users"

    id           = Column(BigInteger,               primary_key=True, autoincrement=False)
    email        = Column(String(320),              nullable=False,   unique=True)
    password     = Column(CHAR(60),                 nullable=False)
    name         = Column(String(50),               nullable=False)
    class_id     = Column(BigInteger,               nullable=True)
    pin_group_id = Column(BigInteger,               nullable=False)
    auth_level   = Column(Integer,                  nullable=False)
    auth_time    = Column(TIMESTAMP(timezone=True), nullable=True)

    def __str__(self):
        """
        String representation of a User
        Displayed when seeding the database, password is hidden
        """
        return f"User {{ id: {self.id}, email: {self.email}, name: {self.name}, "\
               f"class_id: {self.class_id}, pin_group_id: {self.pin_group_id}, "\
               f"auth_level: {self.auth_level}, auth_time: {self.auth_time} }}"

class RefreshToken(Base):
    """
    RefreshToken - used to obtain a new access token without logging in again

    session_id   - each time the user logs in with email and password, a new session is started
    user_id      - the user account this refresh token is for
    token        - the refresh token
    issued_at    - timestamp of when the token was generated
    invalid      - indicates if the token has been revoked
    """
    __tablename__ = "refresh_tokens"

    session_id   = Column(BigInteger,               primary_key=True, autoincrement=False)
    user_id      = Column(BigInteger, ForeignKey("users.id", ondelete="CASCADE"), nullable=False)
    token        = Column(String(100),              nullable=False,   unique=True)
    issued_at    = Column(TIMESTAMP(timezone=True), nullable=False)
    invalid      = Column(Boolean,                  nullable=False,   default=False)

class DummyUser(Base):
    """
    DummyUser - unverified user, created on POST request to /users
                data stored here will be used on verification

    token         - token used in verification link
    email         - email address for the new account
    auth_level    - auth level for the new account
    class_id      - class id for the new account
    user_group_id - optional, if set the user will be added to this group on verification
    created_on    - timestamp when the dummy user was created
    """
    __tablename__ = "dummy_users"

    token         = Column(String(100), primary_key=True, autoincrement=False)
    email         = Column(String(320), nullable=False)
    auth_level    = Column(Integer,     nullable=False)
    class_id      = Column(BigInteger,  nullable=False)
    user_group_id = Column(BigInteger,  nullable=True)
    created_on    = Column(TIMESTAMP(timezone=True), nullable=False)

    def __str__(self):
        """
        String representation of a User
        Displayed when seeding the database, password is hidden
        """
        return f"DummyUser {{ token: {self.token}, email: {self.email}, "\
               f"auth_level: {self.auth_level}, class_id: {self.class_id}, "\
               f"user_group_id: {self.user_group_id}, created_on: {self.created_on} }}"

class PasswordResetToken(Base):
    """
    PasswordResetToken - used for password reset, only one allowed per user

    token   - password reset token
    user_id - account to reset the password of
    expires - timestamp of expiration time
    """
    __tablename__ = "password_reset_tokens"

    token   = Column(String(100), primary_key=True, autoincrement=False)
    user_id = Column(BigInteger, ForeignKey("users.id", ondelete="CASCADE"), nullable=False, unique=True)
    expires = Column(TIMESTAMP(timezone=True), nullable=False)

    def __str__(self):
        """
        String representation of a User
        Displayed when seeding the database, password is hidden
        """
        return f"PasswordResetToken {{ token: {self.token}, user_id: {self.user_id}, "\
               f"expires: {self.expires} }}"

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
