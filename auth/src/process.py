"""
Module containing authentication / authorization procedures
"""

import secrets
from datetime import datetime, timezone, timedelta
import bcrypt
import requests
from flask import current_app as app
from flask import jsonify
from sqlalchemy.orm.exc import NoResultFound

from src.authorization import Authorization
from src.models import User, RefreshToken, DummyUser, PasswordResetToken
from src import errors
from src import utils

class Auth:
    """
    Class containing authentication / authorization procedures
    """

    @classmethod
    def token_login_procedure(cls, email, password):
        """
        Login procedure

        :param str email: user's email
        :param str password: user's password
        :return dict: access token response JSON object
        """
        with utils.get_db_session() as session:
            try:
                user = session.query(User).filter(User.email==email).one()
            except NoResultFound:
                raise errors.InvalidCredentialsError()

            if not cls.__verify_pw(password, user.password):
                raise errors.InvalidCredentialsError()

            # keep track of last password authentication
            user.auth_time = datetime.now(timezone.utc)

            (access_token, expires_in) = Authorization.generate_jwt(
                user.id, int(user.auth_time.timestamp()))

            session_id = secrets.randbits(63)
            refresh_token = secrets.token_urlsafe(64)

            session.add(RefreshToken(session_id=session_id,
                                     user_id=user.id,
                                     token=refresh_token,
                                     issued_at=datetime.now(timezone.utc)))

        return {
            'session_id': session_id,
            'session_id_str': str(session_id),
            'access_token': access_token,
            'token_type': "bearer",
            'expires_in': expires_in,
            'refresh_token': refresh_token
        }

    @classmethod
    def token_refresh_procedure(cls, session_id, token):
        """
        Token refresh procedure

        :param int session_id: id of the session to refresh
        :param str refresh_token: refresh token
        :return dict: access token response JSON object
        """
        with utils.get_db_session() as session:
            refresh_token = session.query(RefreshToken).get(session_id)

            if refresh_token is None:
                raise errors.InvalidRefreshTokenError()

            if refresh_token.token != token:
                raise errors.InvalidRefreshTokenError()

            # NOTE: auth time is not updated when refreshing an existing session
            user = session.query(User).get(refresh_token.user_id)

            (access_token, expires_in) = Authorization.generate_jwt(
                user.id, int(user.auth_time.timestamp()))

            new_token = secrets.token_urlsafe(64)
            refresh_token.token = new_token
            refresh_token.issued_at = datetime.now(timezone.utc)

        return {
            'session_id': session_id,
            'session_id_str': str(session_id),
            'access_token': access_token,
            'token_type': "bearer",
            'expires_in': expires_in,
            'refresh_token': new_token
        }

    @classmethod
    def get_sessions_procedure(cls, jwt):
        """
        Get active sessions

        :param str jwt: access token sent in the request headers
        :return list: list of active sessions
        """
        decoded = Authorization.validate_jwt(jwt)

        if not decoded:
            raise errors.UnauthorizedError()

        user_id = decoded['sub']

        with utils.get_db_session() as session:
            refresh_tokens = session.query(RefreshToken).filter(RefreshToken.user_id==user_id).all()

            # build results
            results = []
            for item in refresh_tokens:
                results.append({
                    'id': item.session_id,
                    'id_str': str(item.session_id),
                    'last_refreshed': int(item.issued_at.timestamp())
                })

        return jsonify(results), 200

    @classmethod
    def revoke_session_procedure(cls, session_id, jwt):
        """
        Revokes an existing session

        :param int session_id: the id of the session to revoke
        :param str jwt: access token sent in the request headers
        """
        decoded = Authorization.validate_jwt(jwt)

        if not decoded:
            raise errors.UnauthorizedError()

        user_id = decoded['sub']

        with utils.get_db_session() as session:
            refresh_token = session.query(RefreshToken).get(session_id)

            # check if the session exists
            if not refresh_token:
                raise errors.BadRequestError()

            # check if session is associated with current user
            if refresh_token.user_id != user_id:
                raise errors.BadRequestError()

            # in both cases above BadRequestError is raised so we don't leak
            # information about whether a session exists

            # session exists and is associated with the user id in the token
            # so it can be deleted
            session.delete(refresh_token)

        return '', 200

    @classmethod
    def register_user_procedure(cls, email, password, name, class_id):
        """
        Create a new user without dummy account

        :param str email: Email address for the new account
        :param str password: Password for the new account
        :param str name: Display name for the new account
        :param int class_id: Class id for the new account
        """
        with utils.get_db_session() as session:
            if session.query(User.id).filter(User.email==email).first() is not None:
                return {
                    'errors': {
                        'user': ["A user with this email address already exists."]
                    }
                }, 400

            if not cls.__check_class_id(class_id):
                raise errors.BadRequestError()

            pin_group_id = cls.__create_pin_group(name)

            hashed_pw = cls.__hash_pw(password)

            user = User(
                id=secrets.randbits(63), # TODO: check for collisions
                email=email,
                password=hashed_pw,
                name=name,
                class_id=class_id,
                pin_group_id=pin_group_id,
                auth_level=1,
                auth_time=None
            )

            session.add(user)

        return '', 201

    @classmethod
    def create_dummy_user_procedure(cls, jwt, email, auth_level, class_id, user_group_id):
        """
        Create a dummy user to be verified later

        :param dict jwt: Access token sent in request headers
        :param str email: Email address for the new account
        :param int auth_level: Auth level for the new account
        :param int class_id: Class id for the new account
        """
        with utils.get_db_session() as session:
            requester = session.query(User).get(jwt['sub'])

            # this should never happen, so log it if it somehow does
            if not requester:
                raise Exception("Invalid user id in valid JWT")

            if requester.auth_level < 3:
                raise errors.ForbiddenError()

            if session.query(User).filter(User.email==email).first() is not None:
                return {
                    'errors': {
                        'user': ["A user with this email address already exists."]
                    }
                }, 400

            if auth_level > requester.auth_level:
                raise errors.ForbiddenError()

            if not cls.__check_class_id(class_id):
                raise errors.BadRequestError()

            if user_group_id is not None:
                if not cls.__check_user_group_id(user_group_id, class_id):
                    raise errors.BadRequestError()

            token = secrets.token_urlsafe(8)

            dummy_user = DummyUser(
                            token=token,
                            email=email,
                            auth_level=auth_level,
                            class_id=class_id,
                            user_group_id=user_group_id,
                            created_on=datetime.now(timezone.utc))
            session.add(dummy_user)

            # send verification email with token
            res = requests.post('http://mail:5000', json={
                'mail_type': 'user_created',
                'to': email,
                'token': token
            })

            if res.status_code != 200:
                raise Exception("Unable to send mail, aborting user creation")

        return '', 201

    @classmethod
    def verify_dummy_user_procedure(cls, token, name, password):
        """
        Verifies a dummy user, turning it into a real user account
        """
        with utils.get_db_session() as session:
            dummy_user = session.query(DummyUser).get(token)

            pin_group_id = cls.__create_pin_group(name)

            hashed_pw = cls.__hash_pw(password)

            user_id = secrets.randbits(63)

            user = User(
                id=user_id,
                email=dummy_user.email,
                password=hashed_pw,
                name=name,
                class_id=dummy_user.class_id,
                pin_group_id=pin_group_id,
                auth_level=dummy_user.auth_level,
                auth_time=None
            )
            session.add(user)

            if dummy_user.user_group_id is not None:
                cls.__join_user_group(user_id, dummy_user.user_group_id)

            session.delete(dummy_user)

    @classmethod
    def get_users_procedure(cls, jwt, class_id, group_id):
        """
        Get all users

        :param dict jwt: access token send in request headers
        :param int class_id: optional, used to filter on class_id
        :param int group_id: optional, used to filter on user group
        """
        with utils.get_db_session() as session:
            requester = session.query(User).get(jwt['sub'])

            if class_id is not None:
                if requester.auth_level < 2 and requester.class_id != class_id:
                    raise errors.ForbiddenError()

                users = session.query(User).filter(User.class_id == class_id).all()
            elif group_id is not None:
                # TODO: check if requester is in same class as group, otherwise forbidden

                group_members = cls.__get_user_group_members(group_id)
                users = session.query(User).filter(User.id.in_(group_members)).all()
            else:
                users = session.query(User).all()

            result = []
            for user in users:
                result.append({'id': user.id, 'id_str': str(user.id), 'name': user.name})

        return jsonify(result), 200


    @classmethod
    def update_user_procedure(cls, user_id, jwt, new_email, new_name, new_auth_level, new_class_id):
        """
        Update user info of an existing account

        :param int user_id: provided user id to update
        :param str jwt: access token sent in the request headers
        :param str new_email: new email, or None if no update necessary
        :param str new_name: new name, or None if no update necessary
        :param int new_auth_level: new auth level, or None if no update necessary
        :param int new_class_id: new class id, or None if no update necessary
        """
        decoded = Authorization.validate_jwt(jwt)

        if not decoded:
            raise errors.UnauthorizedError()

        with utils.get_db_session() as session:

            user = session.query(User).get(user_id)
            requester = session.query(User).get(decoded['sub'])

            if user is None:
                raise errors.ForbiddenError()

            if decoded['sub'] != user_id and requester.auth_level < 2:
                raise errors.ForbiddenError()

            if new_email is not None:
                user.email = new_email

            if new_name is not None:
                user.name = new_name

            if new_auth_level is not None:
                # don't allow users to change their own auth level
                if decoded['sub'] == user_id:
                    raise errors.ForbiddenError()

                # auth level of requester must be higher than the user's auth level
                if requester.auth_level <= user.auth_level:
                    raise errors.ForbiddenError()

                # can't set auth level higher than own auth level
                if new_auth_level > requester.auth_level:
                    raise errors.ForbiddenError()

                user.auth_level = new_auth_level

            if new_class_id is not None:
                if not cls.__check_class_id(new_class_id):
                    raise errors.BadRequestError()

                # admins shouldn't be associated with a class
                if user.auth_level == 4:
                    raise errors.BadRequestError()

                user.class_id = new_class_id

        return '', 200

    @classmethod
    def update_password_procedure(cls, user_id, jwt, new_password, verify_password, current_password):
        """
        Update the password of an existing user account

        :param int user_id: provided user id to update
        :param str jwt: access token sent in the request headers
        :param str new_password: desired new password
        :param str verify_password: repeated new password
        :param str current_password: current password for verification
        """
        decoded = Authorization.validate_jwt(jwt)

        if not decoded:
            raise errors.UnauthorizedError()

        if user_id != decoded['sub']:
            raise errors.ForbiddenError()

        with utils.get_db_session() as session:
            user = session.query(User).get(user_id)

            if user is None:
                raise errors.ForbiddenError()

            if new_password != verify_password:
                raise errors.BadRequestError()

            if not cls.__verify_pw(current_password, user.password):
                raise errors.ForbiddenError()

            user.password = cls.__hash_pw(new_password)

            return '', 200

    @classmethod
    def delete_user_procedure(cls, jwt, user_id):
        """
        Delete the specified user after leaving the user group and deleting the
        pin group

        :param dict jwt: access token send in the request headers
        :param int user_id: user id to delete
        """
        with utils.get_db_session() as session:
            requester = session.query(User).get(jwt['sub'])

            if jwt['sub'] != user_id and requester.auth_level < 3:
                raise errors.ForbiddenError()

            user = session.query(User).get(user_id)

            if user is None:
                raise errors.ForbiddenError()

            cls.__leave_user_group(user.id)

            cls.__delete_pin_group(user.pin_group_id)

            session.delete(user)

        return '', 200

    @classmethod
    def request_password_reset_procedure(cls, email):
        """
        Request a password reset for the account associated with the
        email address.

        :param str email: Email address of the account to reset
        """
        with utils.get_db_session() as session:
            try:
                user = session.query(User).filter(User.email==email).one()
            except NoResultFound:
                raise errors.BadRequestError()

            token = secrets.token_urlsafe(8)
            expires = datetime.now(timezone.utc) + timedelta(hours=6)

            try:
                reset_token = session.query(PasswordResetToken).filter(PasswordResetToken.user_id==user.id).one()
                reset_token.token = token
                reset_token.expires = expires
            except NoResultFound:
                reset_token = PasswordResetToken(
                    token=token,
                    user_id=user.id,
                    expires=expires
                )
                session.add(reset_token)

            res = requests.post('http://mail:5000', json={
                'mail_type': 'password_reset',
                'to': email,
                'name': user.name,
                'token': token
            })

            if res.status_code != 200:
                raise Exception("Unable to send mail, aborting password reset")

        return '', 200

    @classmethod
    def reset_password_procedure(cls, token, password):
        """
        Reset an account password

        :param str token: Password reset token sent in the email
        :param str password: New password
        """
        with utils.get_db_session() as session:
            reset_token = session.query(PasswordResetToken).get(token)

            if reset_token is None:
                raise errors.BadRequestError()

            if datetime.now(timezone.utc) > reset_token.expires:
                raise errors.BadRequestError()

            user = session.query(User).get(reset_token.user_id)

            user.password = cls.__hash_pw(password)

            session.delete(reset_token)

    @staticmethod
    def __verify_pw(password, hashed_password):
        """
        Verify a password using a bcrypt hashed password

        :param str password: The password provided by the user
        :param str hashed_password: The bcrypt hashed password to verify with
        """
        return bcrypt.checkpw(password.encode(), hashed_password.encode())

    @staticmethod
    def __hash_pw(password):
        """
        Hash the provided password using bcrypt

        :param str password: The password to hash
        """
        return bcrypt.hashpw(bytes(password, encoding='UTF-8'), bcrypt.gensalt()).decode()

    @staticmethod
    def __check_class_id(class_id):
        """
        Verify if the given class id exists

        :param int class_id: The class id to check
        """
        res = requests.get('http://group_management:5000/internal/classes')

        if res.status_code != 200:
            raise Exception("Unable to get class ids from group management service")

        class_ids = res.json()

        if class_id in class_ids:
            return True

        return False

    @staticmethod
    def __check_user_group_id(user_group_id, class_id):
        """
        Verify if a user group exists in a specific class

        :param int user_group_id: User group id to check
        :param int class_id: ID of the class supposed to contain the user group
        """
        res = requests.get('http://group_management:5000/internal/user-groups', params={'classID':class_id})

        if res.status_code != 200:
            raise Exception("Unable to get user group ids from group management service")

        user_group_ids = res.json()

        if user_group_id in user_group_ids:
            return True

        return False

    @staticmethod
    def __get_user_group_members(group_id):
        """
        Get all user group members

        :param int group_id: The group id to query
        """
        res = requests.get(f'http://group_management:5000/internal/user-groups/{group_id}')

        if res.status_code != 200:
            raise Exception("Unable to get members from user group")

        return res.json()

    @staticmethod
    def __join_user_group(user_id, group_id):
        """
        Add a user to a user group

        :param int user_id: The user to add to the group
        :param int group_id: The user group to add them to
        """
        res = requests.post(f'http://group_management:5000/internal/user-groups/{group_id}/join', json={'user_id':user_id})

        if res.status_code != 200:
            raise Exception("Unable to add new user to user group")

    @staticmethod
    def __leave_user_group(user_id):
        """
        Remove the specified user from the user group they're in

        :param int user_id: The user to remove from a group
        """
        res = requests.post(f'http://group_management:5000/internal/user-groups/leave', json={'user_id': user_id})

        if res.status_code != 200:
            raise Exception("Unable to leave user group")

    @staticmethod
    def __create_pin_group(name):
        """
        Create a new pin group

        :param str name: Name for the new pin group
        """
        res = requests.post('http://pins:5000/internal/pin-groups', json={'name': name})

        if res.status_code != 201:
            raise Exception("Unable to create pin group")

        return res.json()['id']

    @staticmethod
    def __delete_pin_group(pin_group_id):
        """
        Remove the specified pin group

        :param int pin_group_id: Pin group to remove
        """
        res = requests.delete(f'http://pins:5000/internal/pin-groups/{pin_group_id}')

        if res.status_code != 200:
            raise Exception("Unable to delete pin group")

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
