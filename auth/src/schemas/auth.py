from collections import OrderedDict
from enum import Enum

import bare
from bare.bare_ast import TypeKind, BarePrimitive, StructType, OptionalType, NamedType, ArrayType, MapType, UnionType, UnionValue


class RequestType(Enum):
	USER_INFO_REQUEST = 0
	CLASS_MEMBER_COUNT_REQUEST = 1


class ResponseType(Enum):
	ERROR_RESPONSE = 0
	USER_INFO_RESPONSE = 1
	CLASS_MEMBER_COUNT_RESPONSE = 2


class ErrorType(Enum):
	INVALID_REQUEST = 0
	INVALID_TOKEN = 1
	INVALID_USER_ID = 2


class AuthRequest:
	_ast = StructType(OrderedDict(
	request_type=NamedType("RequestType"),
	request_body=BarePrimitive(TypeKind.Data),
))

	def __init__(self):
		self.request_type = None
		self.request_body = None

	def pack(self):
		return bare.pack(self)

	@classmethod
	def unpack(cls, data, offset=0):
		instance = cls()
		bare.unpack(instance, data, offset=offset)
		return instance


class AuthResponse:
	_ast = StructType(OrderedDict(
	response_type=NamedType("ResponseType"),
	response_body=OptionalType(BarePrimitive(TypeKind.Data)),
	error_type=OptionalType(NamedType("ErrorType")),
	error_message=OptionalType(BarePrimitive(TypeKind.String)),
))

	def __init__(self):
		self.response_type = None
		self.response_body = None
		self.error_type = None
		self.error_message = None

	def pack(self):
		return bare.pack(self)

	@classmethod
	def unpack(cls, data, offset=0):
		instance = cls()
		bare.unpack(instance, data, offset=offset)
		return instance


class UserInfoRequest:
	_ast = StructType(OrderedDict(
	access_token=OptionalType(BarePrimitive(TypeKind.String)),
	user_id=OptionalType(BarePrimitive(TypeKind.I64)),
))

	def __init__(self):
		self.access_token = None
		self.user_id = None

	def pack(self):
		return bare.pack(self)

	@classmethod
	def unpack(cls, data, offset=0):
		instance = cls()
		bare.unpack(instance, data, offset=offset)
		return instance


class UserInfo:
	_ast = StructType(OrderedDict(
	id=BarePrimitive(TypeKind.I64),
	name=BarePrimitive(TypeKind.String),
	email=BarePrimitive(TypeKind.String),
	auth_level=BarePrimitive(TypeKind.I32),
	class_id=OptionalType(BarePrimitive(TypeKind.I64)),
	pin_group_id=BarePrimitive(TypeKind.I64),
))

	def __init__(self):
		self.id = None
		self.name = None
		self.email = None
		self.auth_level = None
		self.class_id = None
		self.pin_group_id = None

	def pack(self):
		return bare.pack(self)

	@classmethod
	def unpack(cls, data, offset=0):
		instance = cls()
		bare.unpack(instance, data, offset=offset)
		return instance


class ClassMemberCountRequest:
	_ast = StructType(OrderedDict(
	class_id=BarePrimitive(TypeKind.I64),
))

	def __init__(self):
		self.class_id = None

	def pack(self):
		return bare.pack(self)

	@classmethod
	def unpack(cls, data, offset=0):
		instance = cls()
		bare.unpack(instance, data, offset=offset)
		return instance


class ClassMemberCountResponse:
	_ast = StructType(OrderedDict(
	member_count=BarePrimitive(TypeKind.I64),
))

	def __init__(self):
		self.member_count = None

	def pack(self):
		return bare.pack(self)

	@classmethod
	def unpack(cls, data, offset=0):
		instance = cls()
		bare.unpack(instance, data, offset=offset)
		return instance


