"""
User blueprint, contains routes for user/ endpoints
"""

from flask import Blueprint, request, render_template, flash
import requests
import bcrypt
import secrets
from datetime import datetime, timezone
from src import errors
from src.models import User, DummyUser, PasswordResetToken
from src.authorization import Authorization
from src.process import Auth
from src import utils
from src.log import log
from src import forms

users = Blueprint('users', __name__, template_folder='templates/')

@users.route('/users/', methods=['POST'])
def create_new_user():
    """
    Create a new dummy user acount and send an email with verification link
    """
    jwt = utils.get_jwt(request.headers)
    if not jwt:
        raise errors.UnauthorizedError()

    decoded = Authorization.validate_jwt(jwt)

    if not decoded:
        raise errors.UnauthorizedError()

    form = forms.DummyUserCreationForm()

    if form.validate_on_submit():
        return Auth.create_dummy_user_procedure(decoded,
                                                form.email.data,
                                                form.auth_level.data,
                                                form.class_id.data,
                                                form.user_group_id.data)

    return {
        'errors': form.errors
    }, 400

@users.route('/users/', methods=['GET'])
def get_users():
    """
    Query users
    """
    
    jwt = utils.get_jwt(request.headers)
    if not jwt:
        raise errors.UnauthorizedError()

    decoded = Authorization.validate_jwt(jwt)

    if not decoded:
        raise errors.UnauthorizedError()

    class_id = request.args.get('class')
    group_id = request.args.get('group')

    if class_id is not None:
        try:
            class_id = int(class_id)
        except ValueError:
            raise errors.BadRequestError()

    if group_id is not None:
        try:
            group_id = int(group_id)
        except ValueError:
            raise errors.BadRequestError()

    return Auth.get_users_procedure(decoded, class_id, group_id)

@users.route('/users/info', methods=['GET'])
def get_user_by_token():
    """
    Get user info of user currently logged in
    """
    jwt = utils.get_jwt(request.headers)
    if not jwt:
        raise errors.UnauthorizedError()

    decoded = Authorization.validate_jwt(jwt)

    if not decoded:
        raise errors.UnauthorizedError()

    return get_user_info(decoded['sub'], decoded)

@users.route('/users/<user_id>', methods=['GET'])
def get_user_by_id(user_id):
    """
    Get user info of specified user
    """
    jwt = utils.get_jwt(request.headers)
    if not jwt:
        raise errors.UnauthorizedError()

    decoded = Authorization.validate_jwt(jwt)

    if not decoded:
        raise errors.UnauthorizedError()

    try:
        user_id = int(user_id)
    except:
        raise errors.BadRequestError()

    return get_user_info(user_id, decoded)

def get_user_info(user_id, decoded_jwt):
    """
    Get user info from user_id, using decoded_jwt for authorization
    """
    with utils.get_db_session() as session:
        user = session.query(User).get(user_id)

        if not user:
            raise errors.ForbiddenError() # 403 to not reveal which user ids exist

        # if the user id doesn't match the id in the token, check if the auth level is high enough
        if user_id != decoded_jwt['sub']:
            requester = session.query(User).get(decoded_jwt['sub'])
            if requester.auth_level < 3:
                raise errors.ForbiddenError()

        email = user.email
        name = user.name
        class_id = user.class_id
        auth_level = user.auth_level

    class_id_str = None
    if class_id is not None:
        class_id_str = str(class_id)

    return {
        'id': user_id,
        'id_str': str(user_id),
        'email': email,
        'name': name,
        'class_id': class_id,
        'class_id_str': class_id_str,
        'auth_level': auth_level,
    }

@users.route('/users/<user_id>', methods=['PUT'])
def update_user(user_id):
    """
    Update user account info
    """
    jwt = utils.get_jwt(request.headers)
    if not jwt:
        raise errors.UnauthorizedError()

    try:
        user_id = int(user_id)
    except ValueError:
        raise errors.BadRequestError()

    try:
        email = request.json['email']
    except KeyError:
        email = None

    try:
        name = request.json['name']
    except KeyError:
        name = None

    try:
        auth_level = int(request.json['auth_level'])
    except (KeyError, ValueError):
        auth_level = None

    try:
        class_id = int(request.json['class_id'])
    except (KeyError, ValueError):
        class_id = None

    return Auth.update_user_procedure(user_id, jwt, email, name, auth_level, class_id)

@users.route('/users/<user_id>/password', methods=['PUT'])
def update_password(user_id):
    """
    Change password
    """
    jwt = utils.get_jwt(request.headers)
    if not jwt:
        raise errors.UnauthorizedError()

    try:
        user_id = int(user_id)
        new_password = request.json['new_password']
        verify_password = request.json['verify_password']
        current_password = request.json['current_password']
    except (KeyError, ValueError, TypeError):
        raise errors.BadRequestError()

    return Auth.update_password_procedure(user_id, jwt, new_password, verify_password, current_password)

@users.route('/users/<user_id>', methods=['DELETE'])
def delete_user(user_id):
    """
    Delete user
    """
    jwt = utils.get_jwt(request.headers)
    if not jwt:
        raise errors.UnauthorizedError()

    decoded = Authorization.validate_jwt(jwt)

    if not decoded:
        raise errors.UnauthorizedError()

    try:
        user_id = int(user_id)
    except ValueError:
        raise errors.BadRequestError()

    return Auth.delete_user_procedure(decoded, user_id)

@users.route('/users/register', methods=['POST'])
def register_user():
    """
    Register a user for a class

    For some errors we return more info because this will be presented to the
    user trying to create the account.
    """
    form = forms.UserRegistrationForm()

    if form.validate_on_submit():
        return Auth.register_user_procedure(form.email.data,
                                            form.password.data,
                                            form.name.data,
                                            form.class_id.data)

    return {
        'errors': form.errors
    }, 400

@users.route('/verify', methods=['GET', 'POST'])
def verify_user():
    """
    Verify a dummy user account, creating a real user account
    """
    token = request.args.get('token')
    if token is None:
        return render_template('errors/token_required.html'), 400

    with utils.get_db_session() as session:
        dummy_user = session.query(DummyUser).get(token)

        if dummy_user is None:
            return render_template('errors/token_required.html'), 400

    form = forms.UserVerificationForm()
    if form.validate_on_submit():
        Auth.verify_dummy_user_procedure(token, form.name.data, form.password.data)
        return render_template('account_created.html')

    status_code = 200
    if form.errors:
        status_code = 400

    return render_template('forms/verification_form.html', form=form), status_code

@users.route('/users/forgot-password', methods=['POST'])
def request_password_reset():
    """
    Request a password reset emai
    """
    try:
        email = request.json['email']
    except (TypeError, KeyError):
        raise errors.BadRequestError()

    return Auth.request_password_reset_procedure(email)

@users.route('/reset-password', methods=['GET', 'POST'])
def reset_password():
    """
    Reset a forgotten password
    """
    token = request.args.get('token')
    if token is None:
        return render_tempate('errors/token_required.html'), 400

    with utils.get_db_session() as session:
        reset_token = session.query(PasswordResetToken).get(token)

        if reset_token is None:
            return render_template('errors/token_required.html'), 400

        if datetime.now(timezone.utc) > reset_token.expires:
            session.delete(reset_token)
            return render_template('errors/token_expired.html'), 400

    form = forms.PasswordResetForm()
    if form.validate_on_submit():
        Auth.reset_password_procedure(token, form.password.data)
        return render_template('password_reset.html')

    status_code = 200
    if form.errors:
        status_code = 400

    return render_template('forms/password_reset_form.html', form=form), status_code

@users.errorhandler(Exception)
def handle_error(error):
    """
    Flask error handler

    APIErrors are returned to the client, any other exception is logged and
    returned as an internal server error
    """
    if isinstance(error, errors.APIError):
        return utils.simple_json_response(error.status_code, error.message)

    log.error("%s: %s", type(error).__name__, error)

    return ("Internal server error", 500)

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
