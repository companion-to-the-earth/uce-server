"""
Auth service utilities
"""

from flask import current_app as app
from json import dumps

def get_jwt(headers):
    """
    Obtains the JWT from the request headers
    """
    # get the JWT from the header
    if 'authorization' not in headers:
        return None
    authorization = headers['authorization']

    # make sure the JWT is valid
    if not authorization.startswith('Bearer '):
        return None

    # cut off the prefix
    return authorization[7:]

def get_db_session():
    """
    Starts a new db session
    """
    return app.config['session_factory'].begin()

def simple_json_response(status_code, message, headers={}):
    """
    Generate a simple JSON response with a status code, message and headers.

    :param int status_code: the status code
    :param str message: the message
    :param dict headers: the headers
    :return tuple: http data for response
    """

    # match the correct status to the status code
    if 200 <= status_code <= 299:
        status = 'success'
    elif 400 <= status_code <= 599:
        status = 'error'
    else:
        raise NotImplementedError(f'Status code not supported: {status_code}')

    # format a simple response
    response = dumps({status: message})

    # set the application type
    headers['Content-Type'] = 'application/json'

    return response, status_code, headers

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
