"""Integration test configuration"""

HOST = 'http://auth:5000'

ACCESS_TOKEN_REGEX = r'[a-zA-Z\d\-\_]+\.[a-zA-Z\d\-\_]+\.[a-zA-Z\d\-\_]+'
REFRESH_TOKEN_REGEX = r'[a-zA-Z\d\-\_]+'

token_password_valid = {
    'grant_type': 'password',
    'email': 'admin@uce.nl',
    'password': '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8'
}

token_password_invalid = {
    'grant_type': 'password',
    'email': 'invalid',
    'password': 'also_invalid'
}

token_refresh_invalid = {
    'grant_type': 'refresh_token',
    'session_id': -1,
    'refresh_token': 'invalid'
}

user_1 = {
    'id': 1,
    'id_str': '1',
    'name': 'admin'
}

user_2 = {
    'id': 2,
    'id_str': '2',
    'name': 'user'
}

expected_user_info = {
    'auth_level': 4,
    'class_id': None,
    'class_id_str': None,
    'email': 'admin@uce.nl',
    'id': 1,
    'id_str': '1',
    'name': 'admin',
}

expected_user_info_2 = {
    'auth_level': 1,
    'class_id': 10000,
    'class_id_str': '10000',
    'email': 'user@uce.nl',
    'id': 2,
    'id_str': '2',
    'name': 'user',
}

update_user_valid = {
    'email': 'updateuser@uce.nl',
    'name': 'updateuser',
    'auth_level': 3,
    'class_id': 20000
}

update_user_valid_revert = {
    'email': 'user@uce.nl',
    'name': 'user',
    'auth_level': 1,
    'class_id': 10000
}

update_user_invalid_classid = {
    'class_id': -1
}

update_user_invalid_auth_level = {
    'auth_level': 5
}

update_user_invalid_own_auth_level = {
    'auth_level': 2
}

update_password_valid = {
    'new_password': 'updated_password',
    'verify_password': 'updated_password',
    'current_password': '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8'
}

update_password_valid_revert = {
    'new_password': '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8',
    'verify_password': '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8',
    'current_password': 'updated_password'
}

update_password_invalid = {
    'new_password': 'invalid',
    'verify_password': 'invalid2',
    'password': '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8'
}

register_user_valid = {
    'class_id': 10000,
    'email': 'newuser@uce.nl',
    'name': 'newuser',
    'password': 'password',
    'verify_password': 'password'
}

register_user_invalid_classid = {
    'class_id': -1,
    'email': 'user3@uce.nl',
    'name': 'user3',
    'password': 'password',
    'verify_password': 'password'
}

register_user_invalid_email = {
    'class_id': 10000,
    'email': 'invalid',
    'name': 'user3',
    'password': 'password',
    'verify_password': 'password'
}

register_user_invalid_password = {
    'class_id': 10000,
    'email': 'user3@uce.nl',
    'name': 'user3',
    'password': 'passwords',
    'verify_password': 'do not match'
}

create_dummy_user_valid = {
    'email': 'newdummy@uce.nl',
    'auth_level': 2,
    'class_id': 10000,
    'user_group_id': 1000
}

create_dummy_user_invalid_email = {
    'email': 'invalid',
    'auth_level': 2,
    'class_id': 10000,
    'user_group_id': 1000
}

create_dummy_user_email_in_use = {
    'email': 'user@uce.nl',
    'auth_level': 2,
    'class_id': 10000
}

create_dummy_user_invalid_auth_level = {
    'email': 'newdummy@uce.nl',
    'auth_level': 7,
    'class_id': 10000,
    'user_group_id': 1000
}

create_dummy_user_invalid_class_id = {
    'email': 'newdummy@uce.nl',
    'auth_level': 2,
    'class_id': -1
}

create_dummy_user_invalid_user_group_id = {
    'email': 'newdummy@uce.nl',
    'auth_level': 2,
    'class_id': 10000,
    'user_group_id': -1
}

verify_dummy_user_token = 'abcdefg'

verify_dummy_user_valid = {
    'name': 'Dummy User',
    'password': 'password123',
    'verify_password': 'password123'
}

verify_dummy_user_invalid_password = {
    'name': 'Dummy User',
    'password': 'password1',
    'verify_password': 'password2'
}

request_password_reset_valid = {
    'email': 'user@uce.nl'
}

request_password_reset_invalid_user = {
    'email': 'invalid'
}

reset_password_token = 'abc'
reset_password_token_expired = 'def'

reset_password_valid = {
    'password': 'newpass',
    'verify_password': 'newpass'
}

reset_password_invalid_password = {
    'password': 'password1',
    'verify_password': 'password2'
}

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
