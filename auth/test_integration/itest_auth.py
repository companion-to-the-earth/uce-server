"""Auth integration tests"""

# disable unnecessary pylint warnings
# pylint: disable=undefined-variable, wildcard-import

import unittest
import requests
from pika import ConnectionParameters
from middleware_connector import middleware
from app.src.schemas.auth import *
from . import config

class ITestMiddleware(middleware.Middleware):
    """Middleware class for integration tests"""

    MESSAGES = []

    def on_message_received(self, _ch, _method, properties, body):
        self.MESSAGES.append((body, properties))

class TestAuth(unittest.TestCase):
    """Integration test class"""

    @classmethod
    def setUpClass(cls):
        """Set up middleware connection"""

        connection_params = ConnectionParameters(host='middleware')
        cls.MW = ITestMiddleware(connection_params)

    def test_token(self):
        """Test token endpoint"""
        url = '/auth/token'

        with self.subTest("password grant, success"):
            res = requests.post(config.HOST + url, json=config.token_password_valid)

            self.assertEqual(res.status_code, 200)

            response = res.json()
            self.assertIsInstance(response['session_id'], int)
            self.assertIsInstance(response['session_id_str'], str)
            self.assertRegex(response['access_token'], config.ACCESS_TOKEN_REGEX)
            self.assertIsInstance(response['expires_in'], int)
            self.assertRegex(response['refresh_token'], config.REFRESH_TOKEN_REGEX)
            self.assertEqual(response['token_type'], "bearer")

            # save session id and refresh token for later
            session_id = response['session_id']
            refresh_token = response['refresh_token']

        with self.subTest("password grant, invalid"):
            res = requests.post(config.HOST + url, json=config.token_password_invalid)
            self.assertEqual(res.status_code, 400)

        with self.subTest("refresh token grant, valid"):
            request_data = {
                'grant_type': 'refresh_token',
                'session_id': session_id,
                'refresh_token': refresh_token
            }
            res = requests.post(config.HOST + url, json=request_data)

            self.assertEqual(res.status_code, 200)

            response = res.json()
            self.assertEqual(response['session_id'], session_id)
            self.assertEqual(response['session_id_str'], str(session_id))
            self.assertRegex(response['access_token'], config.ACCESS_TOKEN_REGEX)
            self.assertIsInstance(response['expires_in'], int)
            self.assertRegex(response['refresh_token'], config.REFRESH_TOKEN_REGEX)
            self.assertEqual(response['token_type'], "bearer")

        with self.subTest("refresh token grant, invalid"):
            res = requests.post(config.HOST + url, json=config.token_refresh_invalid)
            self.assertEqual(res.status_code, 400)

    def test_get_sessions(self):
        """Test get sessions endpoint"""
        url = '/auth/sessions'

        with self.subTest("get sessions, valid"):
            # get access token and session id
            res = requests.post(config.HOST + '/auth/token', json=config.token_password_valid).json()
            access_token = res['access_token']
            session_id = res['session_id']

            headers = {'Authorization': f'Bearer {access_token}'}
            res = requests.get(config.HOST + url, headers=headers)

            self.assertEqual(res.status_code, 200)

            # check if response contains our session and if all properties are
            # correct
            contains_session = False
            for session in res.json():
                self.assertIsInstance(session['id'], int)
                self.assertIsInstance(session['id_str'], str)
                self.assertIsInstance(session['last_refreshed'], int)

                if session['id'] == session_id:
                    contains_session = True

            self.assertTrue(contains_session)

        with self.subTest("get sessions, missing token"):
            res = requests.get(config.HOST + url)

            self.assertEqual(res.status_code, 401)

        with self.subTest("get sessions, invalid token"):
            headers = {'Authorization': 'invalid'}
            res = requests.get(config.HOST + url, headers=headers)

            self.assertEqual(res.status_code, 401)

    def test_delete_session(self):
        """Test delete session endpoint"""
        url = '/auth/sessions'

        with self.subTest("delete session, valid"):
            # get access token and session id
            res = requests.post(config.HOST + '/auth/token', json=config.token_password_valid).json()
            access_token = res['access_token']
            session_id = res['session_id']

            headers = {'Authorization': f'Bearer {access_token}'}
            res = requests.delete(config.HOST + url + f'/{session_id}', headers=headers)

            self.assertEqual(res.status_code, 200)

        with self.subTest("delete session, nonexistent"):
            # reuse session id from previous test as this should no longer exist
            headers = {'Authorization': f'Bearer {access_token}'}
            res = requests.delete(config.HOST + url + f'/{session_id}', headers=headers)

            self.assertEqual(res.status_code, 400)

        with self.subTest("delete session, invalid id"):
            headers = {'Authorization': f'Bearer {access_token}'}
            res = requests.delete(config.HOST + url + f'/invalid', headers=headers)

            self.assertEqual(res.status_code, 400)

        with self.subTest("delete session, missing token"):
            res = requests.delete(config.HOST + url + f'/{session_id}')

            self.assertEqual(res.status_code, 401)

        with self.subTest("delete session, invalid token"):
            headers = {'Authorization': 'invalid'}
            res = requests.delete(config.HOST + url + f'/{session_id}', headers=headers)

            self.assertEqual(res.status_code, 401)

    def test_invalid_request(self):
        """Test whether invalid requests are handled properly"""
        self.MW.publish('auth', "invalid", 'itest')
        self.MW.consume('itest', False)
        response = self.MW.MESSAGES.pop()[0]

        response = AuthResponse.unpack(response)
        self.assertEqual(response.response_type, ResponseType.ERROR_RESPONSE)
        self.assertEqual(response.error_type, ErrorType.INVALID_REQUEST)

    def test_user_info_request(self):
        """Test internal API user info request"""
        res = requests.post(config.HOST + '/auth/token', json=config.token_password_valid)
        access_token = res.json()['access_token']

        with self.subTest("user info request - token, valid"):
            body = UserInfoRequest()
            body.access_token = access_token

            response = self.get_auth_response(RequestType.USER_INFO_REQUEST, body)

            self.assertEqual(response.response_type, ResponseType.USER_INFO_RESPONSE)

            user_info = UserInfo.unpack(response.response_body)
            self.assertEqual(user_info.id, 1)
            self.assertEqual(user_info.name, "admin")
            self.assertEqual(user_info.email, "admin@uce.nl")
            self.assertEqual(user_info.auth_level, 4)
            self.assertEqual(user_info.class_id, None)
            self.assertEqual(user_info.pin_group_id, 100)

        with self.subTest("user info request - user id, valid"):
            body = UserInfoRequest()
            body.user_id = 1

            response = self.get_auth_response(RequestType.USER_INFO_REQUEST, body)

            self.assertEqual(response.response_type, ResponseType.USER_INFO_RESPONSE)

            user_info = UserInfo.unpack(response.response_body)
            self.assertEqual(user_info.id, 1)
            self.assertEqual(user_info.name, "admin")
            self.assertEqual(user_info.email, "admin@uce.nl")
            self.assertEqual(user_info.auth_level, 4)
            self.assertEqual(user_info.class_id, None)
            self.assertEqual(user_info.pin_group_id, 100)

        with self.subTest("user info request - token, invalid"):
            body = UserInfoRequest()
            body.access_token = "invalid"

            response = self.get_auth_response(RequestType.USER_INFO_REQUEST, body)

            self.assertEqual(response.response_type, ResponseType.ERROR_RESPONSE)
            self.assertEqual(response.error_type, ErrorType.INVALID_TOKEN)

        with self.subTest("user info request - user id, invalid"):
            body = UserInfoRequest()
            body.user_id = -1

            response = self.get_auth_response(RequestType.USER_INFO_REQUEST, body)

            self.assertEqual(response.response_type, ResponseType.ERROR_RESPONSE)
            self.assertEqual(response.error_type, ErrorType.INVALID_USER_ID)

    def test_class_member_count_request(self):
        """Test internal API class member count request"""

        with self.subTest("class member count request, valid"):
            body = ClassMemberCountRequest()
            body.class_id = 10000

            response = self.get_auth_response(RequestType.CLASS_MEMBER_COUNT_REQUEST, body)

            self.assertEqual(response.response_type, ResponseType.CLASS_MEMBER_COUNT_RESPONSE)

            class_member_count = ClassMemberCountResponse.unpack(response.response_body)
            self.assertEqual(class_member_count.member_count, 1)

        with self.subTest("class member count request empty, valid"):
            body = ClassMemberCountRequest()
            body.class_id = 20000

            response = self.get_auth_response(RequestType.CLASS_MEMBER_COUNT_REQUEST, body)

            self.assertEqual(response.response_type, ResponseType.CLASS_MEMBER_COUNT_RESPONSE)

            class_member_count = ClassMemberCountResponse.unpack(response.response_body)
            self.assertEqual(class_member_count.member_count, 0)

    def get_auth_response(self, request_type, request_body):
        """Send AuthRequest and return response"""
        request = AuthRequest()
        request.request_type = request_type
        request.request_body = request_body.pack()

        self.MW.publish('auth', request.pack(), 'itest')
        self.MW.consume('itest', False)
        return AuthResponse.unpack(self.MW.MESSAGES.pop()[0])

if __name__ == '__main__':
    unittest.main()

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
