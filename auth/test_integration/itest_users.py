"""/users endpoints integration tests"""

import unittest
import requests
from time import sleep
from . import config

class TestUsers(unittest.TestCase):
    """Integration test class"""

    @classmethod
    def setUpClass(cls):
        """Wait to let db-setup finish"""
        sleep(20)

        # get access token
        res = requests.post(config.HOST + '/auth/token', json=config.token_password_valid).json()
        access_token = res['access_token']
        cls.headers = {'Authorization': f'Bearer {access_token}'}

    def test_get_users(self):
        """Test get users endpoint"""
        url = '/users/'

        with self.subTest("Get users"):
            res = requests.get(config.HOST + url, headers=self.headers)

            self.assertEqual(res.status_code, 200)
            self.assertIn(config.user_1, res.json())
            self.assertIn(config.user_2, res.json())

        with self.subTest("Get users, filter class"):
            res = requests.get(config.HOST + url, params={'class': 10000}, headers=self.headers)

            self.assertEqual(res.status_code, 200)
            self.assertIn(config.user_2, res.json())

        with self.subTest("Get users, filter group"):
            res = requests.get(config.HOST + url, params={'group': 1000}, headers=self.headers)

            self.assertEqual(res.status_code, 200)
            self.assertIn(config.user_1, res.json())

        with self.subTest("Get users, missing token"):
            res = requests.get(config.HOST + url)

            self.assertEqual(res.status_code, 401)

    def test_get_user_info(self):
        """Test get user endpoint"""
        with self.subTest("Get user info with token"):
            res = requests.get(config.HOST + '/users/info', headers=self.headers)

            self.assertEqual(res.status_code, 200)
            self.assertEqual(res.json(), config.expected_user_info)

        with self.subTest("Get user info with id"):
            res = requests.get(config.HOST + '/users/1', headers=self.headers)

            self.assertEqual(res.status_code, 200)
            self.assertEqual(res.json(), config.expected_user_info)

        with self.subTest("Get user info, different user"):
            res = requests.get(config.HOST + '/users/2', headers=self.headers)

            self.assertEqual(res.status_code, 200)
            self.assertEqual(res.json(), config.expected_user_info_2)

        with self.subTest("Get user info, invalid id"):
            res = requests.get(config.HOST + '/users/invalid', headers=self.headers)

            self.assertEqual(res.status_code, 400)

        with self.subTest("Get user info, nonexistent"):
            res = requests.get(config.HOST + '/users/-1', headers=self.headers)

            self.assertEqual(res.status_code, 403)

        with self.subTest("Get user info, missing token"):
            res = requests.get(config.HOST + '/users/1')

            self.assertEqual(res.status_code, 401)

    def test_update_user(self):
        """Test update user endpoint"""

        with self.subTest("Update user, valid"):
            res = requests.put(config.HOST + '/users/2',
                               json=config.update_user_valid,
                               headers=self.headers)

            self.assertEqual(res.status_code, 200)

            # revert
            res = requests.put(config.HOST + '/users/2',
                               json=config.update_user_valid_revert,
                               headers=self.headers)

            self.assertEqual(res.status_code, 200)

        with self.subTest("Update user, invalid class id"):
            res = requests.put(config.HOST + '/users/2',
                               json=config.update_user_invalid_classid,
                               headers=self.headers)

            self.assertEqual(res.status_code, 400)

        with self.subTest("Update user, auth_level too high"):
            res = requests.put(config.HOST + '/users/2',
                               json=config.update_user_invalid_auth_level,
                               headers=self.headers)

            self.assertEqual(res.status_code, 403)

        with self.subTest("Update user, disallow changing own auth_level"):
            res = requests.put(config.HOST + '/users/1',
                               json=config.update_user_invalid_own_auth_level,
                               headers=self.headers)

            self.assertEqual(res.status_code, 403)

    def test_update_password(self):
        """Test password update endpoint"""

        with self.subTest("Update password, valid"):
            res = requests.put(config.HOST + '/users/1/password',
                               json=config.update_password_valid,
                               headers=self.headers)

            self.assertEqual(res.status_code, 200)

            # revert to old password
            res = requests.put(config.HOST + '/users/1/password',
                               json=config.update_password_valid_revert,
                               headers=self.headers)

            self.assertEqual(res.status_code, 200)

        with self.subTest("Update password, verification mismatch"):
            res = requests.put(config.HOST + '/users/1/password',
                               json=config.update_password_invalid,
                               headers=self.headers)

            self.assertEqual(res.status_code, 400)

        with self.subTest("Update password, wrong user"):
            res = requests.put(config.HOST + '/users/2/password',
                               json=config.update_password_valid,
                               headers=self.headers)

            self.assertEqual(res.status_code, 403)

        with self.subTest("Update password, nonexistent user"):
            res = requests.put(config.HOST + '/users/-1/password',
                               json=config.update_password_valid,
                               headers=self.headers)

            self.assertEqual(res.status_code, 403)

        with self.subTest("Update password, missing token"):
            res = requests.put(config.HOST + '/users/1/password',
                               json=config.update_password_valid)

            self.assertEqual(res.status_code, 401)

    def test_delete_user(self):
        """Test user delete endpoint"""
        with self.subTest("Delete user, valid"):
            res = requests.delete(config.HOST + '/users/3', headers=self.headers)

            self.assertEqual(res.status_code, 200)

        with self.subTest("Delete user, invalid user"):
            res = requests.delete(config.HOST + '/users/-1', headers=self.headers)

            self.assertEqual(res.status_code, 403)

    def test_register_user(self):
        """Test user registration endpoint"""
        url = '/users/register'

        with self.subTest("Register user, valid"):
            res = requests.post(config.HOST + url, json=config.register_user_valid)

            self.assertEqual(res.status_code, 201)

        with self.subTest("Register user, email already in use"):
            res = requests.post(config.HOST + url, json=config.register_user_valid)

            self.assertEqual(res.status_code, 400)

        with self.subTest("Register user, invalid class id"):
            res = requests.post(config.HOST + url, json=config.register_user_invalid_classid)

            self.assertEqual(res.status_code, 400)

        with self.subTest("Register user, invalid email"):
            res = requests.post(config.HOST + url, json=config.register_user_invalid_email)

            self.assertEqual(res.status_code, 400)

        with self.subTest("Register user, invalid password"):
            res = requests.post(config.HOST + url, json=config.register_user_invalid_password)

            self.assertEqual(res.status_code, 400)

    def test_create_dummy_user(self):
        """Test dummy user creation"""
        url = '/users/'

        with self.subTest("Create dummy user, valid"):
            res = self.__post_request(url, config.create_dummy_user_valid)

            self.assertEqual(res.status_code, 201)

        with self.subTest("Create dummy user, invalid email"):
            res = requests.post(config.HOST + url,
                                json=config.create_dummy_user_invalid_email,
                                headers=self.headers)

            self.assertEqual(res.status_code, 400)

        with self.subTest("Create dummy user, email already in use"):
            res = self.__post_request(url, config.create_dummy_user_email_in_use)

            self.assertEqual(res.status_code, 400)

        with self.subTest("Create dummy user, invalid auth level"):
            res = self.__post_request(url, config.create_dummy_user_invalid_auth_level)

            self.assertEqual(res.status_code, 400)

        with self.subTest("Create dummy user, invalid class id"):
            res = self.__post_request(url, config.create_dummy_user_invalid_class_id)

            self.assertEqual(res.status_code, 400)

        with self.subTest("Create dummy user, invalid user group id"):
            res = self.__post_request(url, config.create_dummy_user_invalid_user_group_id)

            self.assertEqual(res.status_code, 400)

    def test_verify_dummy_user(self):
        """Test dummy user verification"""
        url=f'/verify?token={config.verify_dummy_user_token}'

        with self.subTest("Verify dummy user, passwords don't match"):
            res = self.__post_request(url, config.verify_dummy_user_invalid_password)

            self.assertEqual(res.status_code, 400)

        with self.subTest("Verify dummy user, missing request body"):
            res = self.__post_request(url, None)

            self.assertEqual(res.status_code, 400)

        with self.subTest("Verify dummy user, valid"):
            res = self.__post_request(url, config.verify_dummy_user_valid)

            self.assertEqual(res.status_code, 200)

        with self.subTest("Verify dummy user, check if deleted after valid request"):
            res = self.__post_request(url, config.verify_dummy_user_valid)

            self.assertEqual(res.status_code, 400)

    def test_request_password_reset(self):
        """Test password reset request"""
        url = '/users/forgot-password'

        with self.subTest("Reset password request, valid"):
            res = self.__post_request(url, config.request_password_reset_valid)

            self.assertEqual(res.status_code, 200)

        with self.subTest("Reset password request, invalid user"):
            res = self.__post_request(url, config.request_password_reset_invalid_user)

            self.assertEqual(res.status_code, 400)

        with self.subTest("Reset password request, missing requst body"):
            res = self.__post_request(url, None)

            self.assertEqual(res.status_code, 400)

    def test_reset_password(self):
        """Test password reset"""
        url = f'/reset-password?token={config.reset_password_token}'
        url_expired = f'/reset-password?token={config.reset_password_token_expired}'

        with self.subTest("Reset password, passwords don't match"):
            res = self.__post_request(url, config.reset_password_invalid_password)

            self.assertEqual(res.status_code, 400)

        with self.subTest("Reset password, missing request body"):
            res = self.__post_request(url, None)

            self.assertEqual(res.status_code, 400)

        with self.subTest("Reset password, valid"):
            res = self.__post_request(url, config.reset_password_valid)

            self.assertEqual(res.status_code, 200)

        with self.subTest("Reset password, expired token"):
            res = self.__post_request(url_expired, config.reset_password_valid)

            self.assertEqual(res.status_code, 400)

    def __post_request(self, url, json):
        """Make a POST request with the required headers and return the response"""
        return requests.post(config.HOST + url, json=json, headers=self.headers)

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
