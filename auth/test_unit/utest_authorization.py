"""
Unit tests for authorization part
"""
import unittest
import os
import time
import jwt
from src.authorization import Authorization

class TestAuthorization(unittest.TestCase):
    """
    Class containing unit tests
    """

    os.environ['JWT_KEY'] = "defaultkey"
    key = os.environ['JWT_KEY']

    def test_generate_jwt(self):
        """
        Test token generation
        """

        (test_token,_) = Authorization.generate_jwt(1, 2)

        decoded_token = jwt.decode(test_token, self.key, algorithms='HS256')

        # check if sub matches user id
        with self.subTest("sub"):
            self.assertEqual(decoded_token['sub'], 1)

        # check if issuer correct
        with self.subTest("iss"):
            #TODO: make configurable
            self.assertEqual(decoded_token['iss'], "https://uce.science.uu.nl")

        # check if issued at time exists
        with self.subTest("iat"):
            self.assertIsInstance(decoded_token['iat'], int)

        # check if expiration time exists and if it's later than the issued at time
        with self.subTest("exp"):
            self.assertIsInstance(decoded_token['exp'], int)
            self.assertGreater(decoded_token['exp'], decoded_token['iat'])

        # check if the auth_time is correct
        with self.subTest("auth_time"):
            self.assertEqual(decoded_token['auth_time'], 2)

    def test_validate_jwt(self):
        """
        Test token validation
        """

        exp_time = time.time() + 600 # expiration time used for tests

        (valid_jwt,_) = Authorization.generate_jwt(1, 2)
        invalid_jwt = "this.is.invalid"

        # TODO: Get issuer from config
        invalid_iss = jwt.encode(
            {
                'iss': "example.com",
                'iat':1,
                'exp':exp_time
            }, self.key, algorithm='HS256')
        invalid_iat = jwt.encode(
            {
                'iss': "https://uce.science.uu.nl",
                'iat': "invalid",
                'exp': exp_time
            }, self.key, algorithm='HS256')
        invalid_exp = jwt.encode(
            {
                'iss': "https://uce.science.uu.nl",
                'iat': 1,
                'exp': 69
            }, self.key, algorithm='HS256')

        missing_iss = jwt.encode(
            {
                'iat':1,
                'exp': exp_time
            }, self.key, algorithm='HS256')
        missing_iat = jwt.encode(
            {
                'iss': "https://uce.science.uu.nl",
                'exp': exp_time
            }, self.key, algorithm='HS256')
        missing_exp = jwt.encode(
            {
                'iss': "https://uce.science.uu.nl",
                'iat': 1
            }, self.key, algorithm='HS256')

        with self.subTest("valid"):
            self.assertIsInstance(Authorization.validate_jwt(valid_jwt), dict)

        with self.subTest("invalid token"):
            self.assertIsNone(Authorization.validate_jwt(invalid_jwt))

        # invalid claims
        with self.subTest("invalid issuer"):
            self.assertIsNone(Authorization.validate_jwt(invalid_iss))
        with self.subTest("invalid issued at"):
            self.assertIsNone(Authorization.validate_jwt(invalid_iat))
        with self.subTest("expired"):
            self.assertIsNone(Authorization.validate_jwt(invalid_exp))

        # missing claims
        with self.subTest("missing issuer claim"):
            self.assertIsNone(Authorization.validate_jwt(missing_iss))
        with self.subTest("missing issued at claim"):
            self.assertIsNone(Authorization.validate_jwt(missing_iat))
        with self.subTest("missing expiration claim"):
            self.assertIsNone(Authorization.validate_jwt(missing_exp))

if __name__ == "__main__":
    unittest.main()

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
