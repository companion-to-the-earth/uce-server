#!/usr/bin/env python3

# Script to add a new user to the database
# DO NOT USE THIS IN PRODUCTION

import subprocess
import getpass
import secrets
import bcrypt
import hashlib

def main():
    print("WARNING: DO NOT RUN THIS IN PRODUCTION")
    print("The intended usage is to add a user in a testing environment")
    print("Are you sure you want to continue? (y/n)")

    if input() != 'y':
        exit()

    email = input("Email: ")

    password = getpass.getpass()
    password = bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt()).decode()

    name = input("Name: ")

    print("Authorization levels:",
          "\t1. student",
          "\t2. teaching assistant",
          "\t3. teacher",
          "\t4. admin",
          sep='\n')
    auth_level = int(input("Authorization level: "))

    if auth_level < 1 or auth_level > 4:
        print("Invalid auth level")
        exit()

    user_id = secrets.randbits(63)

    container_id = subprocess.run(['docker-compose','ps','-q','db-auth'], capture_output=True, text=True).stdout.rstrip()
    print(f"Container ID: {container_id}")

    query = "insert into users(id, email, password, name, pin_group_id, auth_level) "\
           f"values ('{user_id}','{email}', '{password}', '{name}', '-1', '{auth_level}');"
    print(f"Query: {query}")

    print("Run this query? (y/n)")
    if input() != 'y':
        exit()

    subprocess.run(['docker','exec',container_id,'psql','--username=user','--dbname=db-auth','-c',query])

if __name__ == "__main__":
    main()
