#!/usr/bin/env sh

# Simple utility to automatically generate a database migration using alembic
# USAGE:
#   generate_migration.sh [container name] [migration message]

USAGE="generate_migration.sh [container name] [migration message]"

CONTAINER_NAME="$1"
MIGRATION_MESSAGE="$2"

if [ -z $1 -o -z $2 ]; then
    echo "Not all arguments provided"
    echo "USAGE:"
    echo -e "\t${USAGE}"
    exit 1
fi

# rebuild the image so the changed database models are accessible for alembic
# exit if rebuilding fails
echo
echo "----------------------"
echo "   REBUILDING IMAGE   "
echo "----------------------"
docker-compose -f docker-compose.dev.yml build $CONTAINER_NAME || exit 1

# wait for the database to become available, then run alembic in the container
echo
echo "----------------------"
echo " GENERATING MIGRATION "
echo "----------------------"
docker-compose -f docker-compose.dev.yml run $CONTAINER_NAME sh -c "/wait && alembic upgrade head && alembic revision --autogenerate -m \"${MIGRATION_MESSAGE}\""
