FROM python:3.8-alpine3.10

ENV PYTHONUNBUFFERED=1

# add wait script so we can wait for mysql to be available
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.8.0/wait /wait
RUN chmod +x /wait

COPY requirements.txt ./

RUN apk add --virtual build-deps g++ python3-dev musl-dev mariadb-dev \
    && apk add --no-cache py-mysqldb \
    && pip install -r requirements.txt \
    && apk del build-deps

#remove unnecessary dependencies that come with base image
RUN python3 -m pip uninstall -y pip setuptools wheel

# copy alembic config
COPY alembic.ini alembic.ini

# copy db models
COPY models.py models.py

ENV PYTHONPATH=/

# run migrations
# db seeds are only added in testing
CMD if [ "$STAGE" = "$CD_STAGE_TEST" ]; then \
    echo "test stage" \
    && /wait \
    && alembic upgrade head \
    && python /alembic/seed_db.py; \
    else \
    echo "prod stage" \
    && /wait \
    && alembic upgrade head; \
    fi
