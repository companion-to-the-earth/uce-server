"""
Module to seed the database

Loads files from fixtures/ and inserts all records in the database if they do
not exist

Everything happens in one database session so either all seeds are inserted or
the entire operation is rolled back
"""

import os
import glob
import logging
import importlib

import yaml
from sqlalchemy import create_engine
from sqlalchemy.orm import Session


logging.basicConfig(format='%(module)s %(levelname)s | %(message)s',level=logging.INFO)

logging.info("----------------")
logging.info("Loading fixtures")
logging.info("----------------")

# get fixtures file path
current_dir = os.path.dirname(os.path.abspath(__file__))
fixtures_dir = os.path.join(current_dir, 'fixtures')
logging.info("")
logging.info("Loading fixtures from: %s", fixtures_dir)

# get all yml files from fixtures path
fixture_files = sorted(glob.glob(fixtures_dir + '/*.yml'))

engine = create_engine(os.environ['DB_URI'])

logging.info("")
logging.info("Loading fixtures...")
with Session(engine) as session, session.begin():
    for filename in fixture_files:
        logging.info("")
        logging.info("- Loading from %s:", os.path.basename(filename))

        with open(filename, 'r') as file:
            fixture = yaml.safe_load(file)

        # get module and class name, import module and get class
        module_name, class_name = fixture['model'].rsplit('.', 1)
        module = importlib.import_module(module_name)
        model = getattr(module, class_name)

        for record in fixture['records']:
            # build object from yml entry
            obj = model(**record)

            logging.info("  * %s", obj)

            # get primary key
            obj_pk = tuple(record[key] for key in fixture['primary_key'])

            # add object if it doesn't already exist
            if session.query(model).get(obj_pk) is None:
                session.add(obj)

logging.info("")
logging.info("Fixtures loaded successfully")

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
