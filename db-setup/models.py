"""
Database models
"""

import enum
from typing import Text

from sqlalchemy import Column, BigInteger, Integer, Numeric, String, TIMESTAMP, Boolean, ForeignKey, Enum, Text
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class file_type(enum.Enum):
    image = 0
    video = 1
    quiz = 2
    text = 3

class Pin(Base):
    """
    Pin

    pin_id          - 64 bit integer
    title           - title of the pin
    last_edited     - 64 bit timestamp
    latitude        - numeric coordinate
    longitude       - numeric coordinate
    user_id         - 64 bit user id
    """

    __tablename__ = "pins"

    pin_id       = Column(BigInteger,               primary_key=True, nullable=False, autoincrement=False)
    title        = Column(String(length=100),       nullable=False)
    last_edited  = Column(BigInteger,               nullable=False)
    latitude     = Column(Numeric(10, 7),           nullable=False)
    longitude    = Column(Numeric(10, 7),           nullable=False)
    user_id      = Column(BigInteger,               nullable=False)
    text_content = Column(Text)

    def __str__(self):
        """
        String representation of a Pin
        Displayed when seeding the database
        """
        return f"Pin {{ pin_id: {self.pin_id}, title: {self.title}, last_edited: {self.last_edited}, "\
            f"latitude: {self.latitude}, longitude: {self.longitude}, user_id: {self.user_id} }}"

class PinContent(Base):
    """
    PinContent

    pin_content_id          - 64 bit integer
    pin_id                  - 64 bit integer
    content_nr              - integer which indicates the order of files
    file_path               - path to the file in the file system
    content_type            - enum which indicates the content type (image, quiz, video)
    """

    __tablename__ = "pin_content"

    pin_content_id   = Column(BigInteger,           primary_key=True, autoincrement=False, nullable=False)
    pin_id           = Column(BigInteger,           ForeignKey("pins.pin_id", ondelete="CASCADE"), nullable=False)
    content_nr       = Column(Integer,              nullable=False)
    content_type     = Column(Enum(file_type),      nullable=False)

    def __str__(self):
        """
        String representation of a PinContent
        Displayed when seeding the database
        """
        return f"PinContent {{ pin_content_id: {self.pin_content_id}, pin_id: {self.pin_id}, "\
            f"content_nr: {self.content_nr}, "\
            f"content_type: {self.content_type} }}"


class University(Base):
    """
    university

    university_id           - random 64-bit university id
    name                    - name of the university
    """
    __tablename__ = "universities"

    university_id           = Column(BigInteger,                primary_key=True, autoincrement=False)
    name                    = Column(String(50),                nullable=False)

    def __str__(self):
        """
        String representation of a University
        Displayed when seeding the database
        """
        return f"University {{ university_id: {self.university_id}, name: {self.name} }}"

class PinGroup(Base):
    """
    PinGroup

    pin_group_id            - random 64-bit pin group id
    name                    - name of the pin group
    """
    __tablename__ = "pin_groups"

    pin_group_id            = Column(BigInteger,              primary_key=True, autoincrement=False)
    name                    = Column(String(50),              nullable=False)

    def __str__(self):
        """
        String representation of a PinGroup
        Displayed when seeding the database
        """
        return f"PinGroup {{ pin_group_id: {self.pin_group_id}, name: {self.name} }}"

class PinPerPinGroup(Base):
    """
    PinPerPinGroup

    pin_id                - random 64-bit pin id
    pin_group_id          - random 64-bit pin group id
    """

    __tablename__ = "pins_per_pin_group"

    pin_id              = Column(BigInteger,              ForeignKey("pins.pin_id", ondelete="CASCADE"), primary_key=True, autoincrement=False)
    pin_group_id        = Column(BigInteger,              ForeignKey("pin_groups.pin_group_id", ondelete="CASCADE"), primary_key=True, autoincrement=False)

    def __str__(self):
        """
        String representation of a PinPerPinGroup
        Displayed when seeding the database
        """
        return f"PinPerPinGroup {{ pin_id: {self.pin_id}, pin_group_id: {self.pin_group_id} }}"

class Course(Base):
    """
    Course

    course_id               - random 64-bit course id
    name                    - name of the class
    university_id           - 64-bit university_id
    """

    __tablename__ = "courses"

    course_id             = Column(BigInteger,              primary_key=True, autoincrement=False)
    name                  = Column(String(50),              nullable=False)
    university_id         = Column(BigInteger,              ForeignKey("universities.university_id", ondelete="CASCADE"), autoincrement=False)

    def __str__(self):
        """
        String representation of a Course
        Displayed when seeding the database
        """
        return f"Course {{ course_id: {self.course_id}, name: {self.name}, university_id: {self.university_id}}}"

class Class(Base):
    """
    Class

    class_id                - random 64-bit class id
    name                    - name of the class
    course_id               - 64-bit course id
    pin_group_id            - 64-bit pin_group_id
    """

    __tablename__ = "classes"

    class_id              = Column(BigInteger,              primary_key=True, autoincrement=False)
    name                  = Column(String(50),              nullable=False)
    course_id             = Column(BigInteger,              ForeignKey("courses.course_id", ondelete="CASCADE"), autoincrement=False, nullable=False)
    pin_group_id          = Column(BigInteger,              ForeignKey("pin_groups.pin_group_id", ondelete="CASCADE"), autoincrement=False)

    def __str__(self):
        """
        String representation of a Class
        Displayed when seeding the database
        """
        return f"Class {{ class_id: {self.class_id}, name: {self.name}, course_id: {self.course_id}, pin_group_id: {self.pin_group_id} }}"

class UserGroup(Base):
    """
    UserGroup

    user_group_id                - random 64-bit user group id
    name                         - name of the class
    class_id                     - 64-bit class_id
    pin_group_id                 - 64-bit pin_group_id
    """

    __tablename__ = "user_groups"

    user_group_id              = Column(BigInteger,              primary_key=True, autoincrement=False)
    name                       = Column(String(50),              nullable=False)
    class_id                   = Column(BigInteger,              ForeignKey("classes.class_id", ondelete="CASCADE"), autoincrement=False)
    pin_group_id               = Column(BigInteger,              ForeignKey("pin_groups.pin_group_id", ondelete="CASCADE"), autoincrement=False)
    max_user_count             = Column(Integer,                 nullable=False)

    def __str__(self):
        """
        String representation of a UserGroup
        Displayed when seeding the database
        """
        return f"UserGroup {{ user_group_id: {self.user_group_id}, name: {self.name}, class_id: {self.class_id}, pin_group_id: {self.pin_group_id}, max_user_count: {self.max_user_count} }}"

class UserPerUserGroup(Base):
    """
    UserPerUserGroup

    user_id                      - 64-bit user id
    user_group_id                - 64-bit user group id
    """

    __tablename__ = "users_per_user_group"

    user_id              = Column(BigInteger,              primary_key=True, autoincrement=False)
    user_group_id        = Column(BigInteger,              ForeignKey("user_groups.user_group_id", ondelete="CASCADE"), autoincrement=False)


    def __str__(self):
        """
        String representation of a UserGroupPerUserGroup
        Displayed when seeding the database
        """
        return f"UserPerUserGroup {{ user_id: {self.user_id}, user_group_id: {self.user_group_id} }}"

class Verification(Base):
    """
    Verification

    user_id                      - 64-bit user id
    verification_code            - string of 36 chars
    experation_date              - 64-bit int of the time
    """

    __tablename__ = "verification"

    user_id             = Column(BigInteger,                 nullable=False)
    verification_code   = Column(String(36),                 primary_key=True, nullable=False)
    expiration_date     = Column(TIMESTAMP(timezone=True),   nullable=False)

    def __str__(self):
        """
        String representation of a Verification
        Displayed when seeding the database
        """
        return f"Verification {{ verification_code: {self.verification_code}, expiration_date: {self.expiration_date}, user_id: {self.user_id} }}"

class Book(Base):
    """
    Book

    book_id             - 64 bit integer
    title               - title of the book
    last_edited         - 64 bit timestamp
    course_id           - 64 bit course_id
    """

    __tablename__ = "books"

    book_id             = Column(BigInteger,               primary_key=True, nullable=False, autoincrement=False)
    title               = Column(String(length=100),       nullable=False)
    last_edited         = Column(BigInteger,               nullable=False)

    def __str__(self):
        """
        String representation of a Book
        Displayed when seeding the database
        """
        return f"Book {{ book_id: {self.book_id}, title: {self.title}, last_edited: {self.last_edited} }}"

class BookSection(Base):
    """
    BookSection

    book_section_id         - 64 bit integer
    book_id                 - 64 bit integer
    title                   - title of a section
    section_nr              - integer which indicates the order of sections
    """

    __tablename__ = "book_sections"

    book_section_id     = Column(BigInteger,               primary_key=True, autoincrement=False, nullable=False)
    book_id             = Column(BigInteger,               ForeignKey("books.book_id", ondelete="CASCADE"), nullable=False)
    title               = Column(String(length=100),       nullable=False)
    section_nr          = Column(Integer,                  nullable=False)

    def __str__(self):
        """
        String representation of a BookSection
        Displayed when seeding the database
        """
        return f"BookSection {{ book_section_id: {self.book_section_id}, book_id: {self.book_id}, " \
            f"title: {self.title}, section_nr: {self.section_nr}, "

class BookContent(Base):
    """
    BookContent

    book_content_id         - 64 bit integer
    book_id                 - 64 bit integer
    content_nr              - integer which indicates the order of files
    content_type            - enum which indicates the content type (image, quiz, video)
    """

    __tablename__ = "book_content"

    book_content_id     = Column(BigInteger,               primary_key=True, autoincrement=False, nullable=False)
    book_section_id     = Column(BigInteger,               ForeignKey("book_sections.book_section_id", ondelete="CASCADE"), nullable=False)
    content_nr          = Column(Integer,                  nullable=False)
    content_type        = Column(Enum(file_type),          nullable=False)

    def __str__(self):
        """
        String representation of a BookContent
        Displayed when seeding the database
        """
        return f"BookContent {{ book_content_id: {self.book_content_id}, book_id: {self.book_section_id}, " \
            f"content_nr: {self.content_nr}, content_type: {self.content_type} }}"