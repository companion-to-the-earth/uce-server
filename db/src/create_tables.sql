CREATE TABLE Universities (
    UniversityID 	CHAR(36)  	    NOT NULL,
    Title           VARCHAR(30)     NOT NULL,
    PRIMARY KEY(UniversityID)
);

CREATE TABLE Locations (
    LocationID 		CHAR(36)  	    NOT NULL,
    Title           VARCHAR(30)     NOT NULL,
    PRIMARY KEY(LocationID)
);

CREATE TABLE Faculties (
    FacultyID 		CHAR(36)  	    NOT NULL,
    Title           VARCHAR(30)     NOT NULL,
    PRIMARY KEY(FacultyID)
);

CREATE TABLE Classes (
    ClassID	 	    CHAR(36)  	    NOT NULL,
    Title           VARCHAR(30)     NOT NULL,
    PRIMARY KEY(ClassID)
);

CREATE TABLE Groups (
    GroupID 		CHAR(36)  	    NOT NULL,
    Title           VARCHAR(30)     NOT NULL,
    PRIMARY KEY(GroupID)
);

CREATE TABLE Groups_Per_Class (
    ClassID         CHAR(36)  	    NOT NULL,
    GroupID 		CHAR(36)  	    NOT NULL,
    PRIMARY KEY(ClassID, GroupID),
    FOREIGN KEY(ClassID)            REFERENCES Classes(ClassID),
    FOREIGN KEY(GroupID)            REFERENCES Groups(GroupID)
);

CREATE TABLE Classes_Per_Faculty (
    FacultyID 		CHAR(36)  	    NOT NULL,
    ClassID         CHAR(36)  	    NOT NULL,
    PRIMARY KEY(FacultyID, ClassID),
    FOREIGN KEY(FacultyID)          REFERENCES Faculties(FacultyID),
    FOREIGN KEY(ClassID)            REFERENCES Classes(ClassID)
);

CREATE TABLE Faculties_Per_Location (
    LocationID      CHAR(36)  	    NOT NULL,
    FacultyID 		CHAR(36)  	    NOT NULL,
    PRIMARY KEY(LocationID, FacultyID),
    FOREIGN KEY(LocationID)	        REFERENCES Locations(LocationID),
    FOREIGN KEY(FacultyID)          REFERENCES Faculties(FacultyID)
);

CREATE TABLE Locations_Per_University (
    UniversityID    CHAR(36)  	    NOT NULL,
    LocationID 		CHAR(36)  	    NOT NULL,
    PRIMARY KEY(UniversityID, LocationID),
    FOREIGN KEY(UniversityID)	    REFERENCES Universities(UniversityID),
    FOREIGN KEY(LocationID)         REFERENCES Locations(LocationID)
);

CREATE TABLE File_Paths (
    FileID		    CHAR(36)	    NOT NULL,
    Title   	    VARCHAR(30)	    NOT NULL,
    Property_Type   VARCHAR(15)     NOT NULL,
    UniversityID	CHAR(36)        NOT NULL,
    LocationID		CHAR(36),
    FacultyID		CHAR(36),
    ClassID		    CHAR(36),
    GroupID		    CHAR(36),
    PRIMARY KEY(FileID),
    FOREIGN KEY(UniversityID)	    REFERENCES Universities(UniversityID),
    FOREIGN KEY(LocationID)	        REFERENCES Locations(LocationID),
    FOREIGN KEY(FacultyID)	        REFERENCES Faculties(FacultyID),
    FOREIGN KEY(ClassID)	        REFERENCES Classes(ClassID),
    FOREIGN KEY(GroupID)	        REFERENCES Groups(GroupID)
);

CREATE TABLE Pins (
    PinID		    CHAR(36)	    NOT NULL,
    Title           VARCHAR(40)     NOT NULL,
    Pin_Type        VARCHAR(10)     NOT NULL,
    Pin_Status      INT             NOT NULL,
    Difficulty_Lvl	INT UNSIGNED	NOT NULL,
    Location_UTM	VARCHAR(22)     NOT NULL,
    Visible_Range	INT UNSIGNED 	DEFAULT NULL,
    Predecessor_IDs	TEXT            DEFAULT NULL,
    Successor_IDs   TEXT            DEFAULT NULL,
    Content         TEXT            NOT NULL,
    PRIMARY KEY(PinID),
    FOREIGN KEY(PinID)              REFERENCES File_Paths(FileID)
);

CREATE TABLE User_Location_Histories (
    UserID		    CHAR(36)  	    NOT NULL,
    Location_UTM	VARCHAR(22)     NOT NULL,
    Location_time   TIMESTAMP       DEFAULT CURRENT_TIMESTAMP	NOT NULL,
    Last_Updated	TIMESTAMP	    DEFAULT CURRENT_TIMESTAMP	NOT NULL,
    PRIMARY KEY(UserID, Location_UTM)
);

CREATE TABLE Users_Per_Group (
    UserID		    CHAR(36)  	    NOT NULL,
    GroupID		    CHAR(36)  	    NOT NULL,
    PRIMARY KEY(UserID, GroupID),
    FOREIGN KEY(GroupID) 	        REFERENCES Groups(GroupID)
);

CREATE TABLE Verification_Codes (
    Verification_Code	CHAR(36)        NOT NULL,
    Expiration_Date	    TIMESTAMP	    NOT NULL,
    UserID		        CHAR(36)	    NOT NULL,
    PRIMARY KEY(Verification_Code)
);



CREATE TABLE Pin_Groups(
    PinGroupID      BIGINT(16)          NOT NULL,
    Name            VARCHAR(50)         NOT NULL,
    PRIMARY KEY(PinGroupID)
);

CREATE TABLE NewUniversities(
    UniversityID    BIGINT(16)          NOT NULL,
    Name            VARCHAR(50)         NOT NULL,
    PRIMARY KEY(UniversityID)
);

CREATE TABLE NewClasses (
    ClassID	 	    BIGINT(16)  	NOT NULL,
    Name            VARCHAR(30)     NOT NULL,
    UniversityID    BIGINT(16)      NOT NULL,
    PinGroupID      BIGINT(16)      NOT NULL,
    PRIMARY KEY(ClassID),
    FOREIGN KEY(UniversityID)       REFERENCES NewUniversities(UniversityID),
    FOREIGN KEY(PinGroupID)         REFERENCES Pin_Groups(PinGroupID)
);

CREATE TABLE NewPins(
    PinID           BIGINT(16)          NOT NULL,
    Title           VARCHAR(50)         NOT NULL,
    LastEdited      BIGINT(16)          NOT NULL,
    Latitude        DOUBLE PRECISION    NOT NULL,
    Longitude       DOUBLE PRECISION    NOT NULL,
    UserID          BIGINT(16)          NOT NULL,
    PRIMARY KEY(PinID)
);

CREATE TABLE PinContent(
    ContentID       BIGINT(16)                          NOT NULL,
    PinID           BIGINT(16)                          NOT NULL,
    ContentNr       INT UNSIGNED                        NOT NULL,
    Content         VARCHAR(255)                        NOT NULL,
    Type            ENUM('text','image','video','quiz') NOT NULL,
    PRIMARY KEY(ContentID),
    FOREIGN KEY(PinID)          REFERENCES NewPins(PinID) ON DELETE CASCADE
);

CREATE TABLE Pins_Per_Pin_Group(
    PinGroupID      BIGINT(16)          NOT NULL,
    PinID           BIGINT(16)          NOT NULL,
    PRIMARY KEY(PinGroupID, PinID),
    FOREIGN KEY(PinGroupID)             REFERENCES Pin_Groups(PinGroupID) ON DELETE CASCADE,
    FOREIGN KEY(PinID)                  REFERENCES NewPins(PinID) ON DELETE CASCADE
);

CREATE TABLE User_Groups(
    UserGroupID     BIGINT(16)          NOT NULL,
    Name            VARCHAR(50) 	    NOT NULL,
    ClassID         BIGINT(16)          NOT NULL,
    PinGroupID      BIGINT(16)          NOT NULL,
    PRIMARY KEY(UserGroupID),
    FOREIGN KEY(ClassID)                REFERENCES NewClasses(ClassID),
    FOREIGN KEY(PinGroupID)             REFERENCES Pin_Groups(PinGroupID)
);

CREATE TABLE Users_Per_User_Group(
    UserID          BIGINT(16)          NOT NULL,
    UserGroupID     BIGINT(16)          NOT NULL,
    PRIMARY KEY(UserID),
    FOREIGN KEY(UserGroupID)            REFERENCES User_Groups(UserGroupID) ON DELETE CASCADE
);