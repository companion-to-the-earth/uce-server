-- Change delimiter so that the function body doesn't end the function declaration
DELIMITER ;;
CREATE FUNCTION uuid_v4s()
    RETURNS CHAR(36)
BEGIN
    -- 1th and 2nd block are made of 6 random bytes
    SET @h1 = HEX(RANDOM_BYTES(4));
    SET @h2 = HEX(RANDOM_BYTES(2));

    -- 3th block will start with a 4 indicating the version, remaining is random
    SET @h3 = SUBSTR(HEX(RANDOM_BYTES(2)), 2, 3);

    -- 4th block first nibble can only be 8, 9 A or B, remaining is random
    SET @h4 = CONCAT(HEX(FLOOR(ASCII(RANDOM_BYTES(1)) / 64)+8),
                SUBSTR(HEX(RANDOM_BYTES(2)), 2, 3));

    -- 5th block is made of 6 random bytes
    SET @h5 = HEX(RANDOM_BYTES(6));

    -- Build the complete UUID
    RETURN LOWER(CONCAT(
        @h1, '-', @h2, '-4', @h3, '-', @h4, '-', @h5
    ));
END
;;

DELIMITER ;;
CREATE TRIGGER before_insert_Gile_Paths
BEFORE INSERT ON File_Paths
FOR EACH ROW
BEGIN
  IF new.FileID IS NULL THEN
    SET new.FileID = uuid_v4s();
  END IF;
END
;;

DELIMITER ;;
CREATE TRIGGER before_insert_Universities
BEFORE INSERT ON Universities
FOR EACH ROW
BEGIN
  IF new.UniversityID IS NULL THEN
    SET new.UniversityID = uuid_v4s();
  END IF;
END
;;

DELIMITER ;;
CREATE TRIGGER before_insert_Locations
BEFORE INSERT ON Locations
FOR EACH ROW
BEGIN
  IF new.LocationID IS NULL THEN
    SET new.LocationID = uuid_v4s();
  END IF;
END
;;

DELIMITER ;;
CREATE TRIGGER before_insert_Faculties
BEFORE INSERT ON Faculties
FOR EACH ROW
BEGIN
  IF new.FacultyID IS NULL THEN
    SET new.FacultyID = uuid_v4s();
  END IF;
END
;;

DELIMITER ;;
CREATE TRIGGER before_insert_Classes
BEFORE INSERT ON Classes
FOR EACH ROW
BEGIN
  IF new.ClassID IS NULL THEN
    SET new.ClassID = uuid_v4s();
  END IF;
END
;;

DELIMITER ;;
CREATE TRIGGER before_insert_Groups
BEFORE INSERT ON Groups
FOR EACH ROW
BEGIN
  IF new.GroupID IS NULL THEN
    SET new.GroupID = uuid_v4s();
  END IF;
END
;;

INSERT INTO Groups (GroupID, Title) VALUES
  ("12170a4a-6ae1-4610-8e9b-5719ce4d8576", "Group 1"),
  ("291a1981-a35f-45a6-9c12-94476f9bc072", "Group 2"),
  ("afcc0d24-2bd4-4701-b1e9-ee07a7d73bdf", "Group 3"),
  ("fe3430a6-94f2-49a1-be92-39d4633d1693", "Group 4");

INSERT INTO Classes (ClassID, Title) VALUES 
  ("2e2da709-a273-4427-a2be-75213d16c81a", "Class 1"),
  ("6088d815-0ece-4607-aef1-713f71ab89f9", "Class 2"),
  ("41ba208b-ba08-4afa-864d-0043c4203595", "Class 3");

INSERT INTO Faculties (FacultyID, Title) VALUES 
  ("61d7c7f3-6ddf-428e-9cc0-b886dcd17242", "Faculty 1"),
  ("1a9de169-d3b5-4333-b87b-7b98ac4b6317", "Faculty 2");

INSERT INTO Locations (LocationID, Title) VALUES 
  ("a1b002b9-6736-4b4c-9e2b-c99a1c7fc1b7", "Pyrenees"),
  ("da4e89ba-0fd0-47d6-a35c-13f621889362", "Grand Canyon"),
  ("90875463-1e88-4156-af59-56d1712d9d42", "Alps");

INSERT INTO Universities (UniversityID, Title) VALUES 
  ("1a37f1e8-60ec-40fd-b56c-646b4c38c8c5", "University Utrecht"),
  ("62fcb1f4-f8cf-4be4-85a4-31973b2de263", "University Rotterdam");

INSERT INTO File_Paths (FileID, Title, Property_Type, UniversityID, LocationID) VALUES
  ('8fddb646-a9c1-4d57-af09-3dbd912af826', 'test_1a', 'map',"1a37f1e8-60ec-40fd-b56c-646b4c38c8c5", "a1b002b9-6736-4b4c-9e2b-c99a1c7fc1b7"),
  ('d888fc6e-fcea-4bd9-b295-2a76426c0e70', 'test_1b', 'map',"1a37f1e8-60ec-40fd-b56c-646b4c38c8c5", "a1b002b9-6736-4b4c-9e2b-c99a1c7fc1b7"),
  ('777b4a79-659c-4b19-b2b1-eee34e3dbe1f', 'test_1c', 'map',"1a37f1e8-60ec-40fd-b56c-646b4c38c8c5", "a1b002b9-6736-4b4c-9e2b-c99a1c7fc1b7");

INSERT INTO File_Paths (FileID, Title, Property_Type, UniversityID, LocationID, FacultyID, ClassID, GroupID) VALUES
  ('7b54f09a-2689-4ea0-8b86-b0b486d7854e', 'test_2a', 'content', "1a37f1e8-60ec-40fd-b56c-646b4c38c8c5", "a1b002b9-6736-4b4c-9e2b-c99a1c7fc1b7",
  "61d7c7f3-6ddf-428e-9cc0-b886dcd17242", "2e2da709-a273-4427-a2be-75213d16c81a", "12170a4a-6ae1-4610-8e9b-5719ce4d8576"),
  ('9cd32111-c9df-4e4d-9f48-b7c2abaf6da3', 'test_2b', 'content', "1a37f1e8-60ec-40fd-b56c-646b4c38c8c5", "a1b002b9-6736-4b4c-9e2b-c99a1c7fc1b7",
  "61d7c7f3-6ddf-428e-9cc0-b886dcd17242", "2e2da709-a273-4427-a2be-75213d16c81a", "12170a4a-6ae1-4610-8e9b-5719ce4d8576");
  
INSERT INTO Users_Per_Group (UserID, GroupID) VALUES
  (1, '12170a4a-6ae1-4610-8e9b-5719ce4d8576');

INSERT INTO Groups_Per_Class (GroupID, ClassID) VALUES 
  ('12170a4a-6ae1-4610-8e9b-5719ce4d8576', '2e2da709-a273-4427-a2be-75213d16c81a');

INSERT INTO Classes_Per_Faculty (ClassID, FacultyID) VALUES 
  ('2e2da709-a273-4427-a2be-75213d16c81a', '61d7c7f3-6ddf-428e-9cc0-b886dcd17242');

INSERT INTO Faculties_Per_Location (FacultyID, LocationID) VALUES 
  ('61d7c7f3-6ddf-428e-9cc0-b886dcd17242', 'a1b002b9-6736-4b4c-9e2b-c99a1c7fc1b7');

INSERT INTO Locations_Per_University (LocationID, UniversityID) VALUES 
  ('a1b002b9-6736-4b4c-9e2b-c99a1c7fc1b7', '1a37f1e8-60ec-40fd-b56c-646b4c38c8c5');



INSERT INTO NewPins VALUES
  (10, "testpin_1", 1618218320, 345.23234, 256.3345, 1),
  (20, "testpin_2", 1618218321, 202.32323, 222.123123, 2),
  (30, "testpin_3", 1618218322, 203.32323, 333.123123, 1),
  (40, "testpin_4", 1618218323, 204.32323, 444.123123, 2);

INSERT INTO PinContent VALUES
  (10, 10, 0, 'path/to/content', 'text'),
  (20, 10, 1, 'path/to/morecontent', 'image'),
  (30, 20, 0, 'path/to/content', 'text'),
  (40, 30, 0, 'path/to/morecontent', 'image'),
  (50, 40, 0, 'path/to/content', 'text'),
  (60, 40, 1, 'path/to/morecontent', 'image');

INSERT INTO Pin_Groups VALUES
  (100, 'testgroup_1'),
  (200, 'testgroup_2'),
  (300, 'testgroup_3'),
  (400, 'testgroup_4'),
  (500, 'testgroup_5'),
  (600, 'testgroup_6'),
  (700, 'testgroup_7'),
  (800, 'testgroup_8');

INSERT INTO NewUniversities (UniversityID, Name) VALUES
  (100000, "testUniversity_1"),
  (200000, "testUniversity_2");

INSERT INTO NewClasses (ClassID, Name, UniversityID, PinGroupID) VALUES
  (10000, "testClass_1", 100000, 300),
  (20000, "testClass_2", 100000, 400),
  (30000, "testClass_3", 100000, 800);

INSERT INTO User_Groups (UserGroupID, Name, ClassID, PinGroupID) VALUES
  (1000, "testGroup_1", 10000, 500),
  (2000, "testGroup_2", 10000, 600),
  (3000, "testGroup_3", 20000, 700),
  (4000, "testGroup_4", 20000, 800);

INSERT INTO Pins_Per_Pin_Group VALUES
  (100, 10),
  (100, 20),
  (200, 30),
  (300, 10),
  (400, 10),
  (400, 40),
  (500, 10),
  (500, 20),
  (600, 10),
  (600, 20),
  (600, 30),
  (600, 40),
  (700, 10);

INSERT INTO Users_Per_User_Group VALUES
  (1, 1000),
  (2, 2000);