version: '3'

services:
  library:
    image: registry.gitlab.com/companion-to-the-earth/uce-server/library:$CI_COMMIT_REF_NAME-$STAGE
    depends_on:
      - middleware-db
      - auth
    environment:
      STAGE: ${STAGE}
      CD_STAGE_TEST: ${CD_STAGE_TEST}
    volumes:
      - ./library/content:/var/www/library/content
    expose:
      - "5000"
    restart: on-failure

  pins:
    image: registry.gitlab.com/companion-to-the-earth/uce-server/pins:$CI_COMMIT_REF_NAME-$STAGE
    depends_on:
      - middleware-db
      - auth
    environment:
      STAGE: ${STAGE}
      CD_STAGE_TEST: ${CD_STAGE_TEST}
    volumes:
      - ./pins/content:/var/www/content
    expose:
      - "5000"
    restart: on-failure

  group_management:
    image: registry.gitlab.com/companion-to-the-earth/uce-server/group_management:$CI_COMMIT_REF_NAME-$STAGE
    depends_on:
      - middleware-db
      - auth
    environment:
      STAGE: ${STAGE}
      CD_STAGE_TEST: ${CD_STAGE_TEST}
    expose:
      - "5000"
    restart: on-failure

  mail:
    image: registry.gitlab.com/companion-to-the-earth/uce-server/mail:$CI_COMMIT_REF_NAME-$STAGE
    environment:
      STAGE: ${STAGE}
      CD_STAGE_TEST: ${CD_STAGE_TEST}
      DOMAIN_NAME: ${DOMAIN_NAME}
      MAIL_SERVER: ${MAIL_SERVER}
      MAIL_PORT: ${MAIL_PORT}
      MAIL_USERNAME: ${MAIL_USERNAME}
      MAIL_PASSWORD: ${MAIL_PASSWORD}
      MAIL_USE_TLS: ${MAIL_USE_TLS}
      MAIL_USE_SSL: ${MAIL_USE_SSL}
      MAIL_DEFAULT_SENDER: ${MAIL_DEFAULT_SENDER}
    restart: on-failure

  db:
    image: registry.gitlab.com/companion-to-the-earth/uce-server/db:$CI_COMMIT_REF_NAME-$STAGE
    command: mysqld  #default-authentication-plugin=mysql_native_password
    environment:
      MYSQL_DATABASE: ${UCE_DATABASE}
      MYSQL_ROOT_PASSWORD: ${UCE_DB_ROOT_PASSWORD}
      MYSQL_USER: ${UCE_DB_USER}
      MYSQL_PASSWORD: ${UCE_DB_PASSWORD}
    volumes:
      - ./db/data/:/var/lib/mysql
    restart: on-failure

  db-setup:
    image: registry.gitlab.com/companion-to-the-earth/uce-server/db-setup:$CI_COMMIT_REF_NAME-$STAGE
    environment:
      DB_URI: "mysql://${UCE_DB_USER}:${UCE_DB_PASSWORD}@db/${UCE_DATABASE}"
      STAGE: ${STAGE}
      CD_STAGE_TEST: ${CD_STAGE_TEST}
      WAIT_HOSTS: "db:3306"
    volumes:
      - ./db-setup/alembic:/alembic
    depends_on:
      - db

  middleware:
    image: registry.gitlab.com/companion-to-the-earth/lib-middleware-connector/middleware:2.0.0-test
    restart: on-failure

  middleware-db:
    image: registry.gitlab.com/companion-to-the-earth/uce-server/middleware-db:$CI_COMMIT_REF_NAME-$STAGE
    environment:
      STAGE: ${STAGE}
      CD_STAGE_TEST: ${CD_STAGE_TEST}
      MYSQL_DATABASE: ${UCE_DATABASE}
      MYSQL_USER: ${UCE_DB_USER}
      MYSQL_PASSWORD: ${UCE_DB_PASSWORD}
    depends_on:
      - middleware
      - db
    restart: on-failure

  db-auth:
    image: postgres:13.2-alpine
    environment:
      POSTGRES_DB: ${AUTH_DATABASE}
      POSTGRES_USER: ${AUTH_DB_USER}
      POSTGRES_PASSWORD: ${AUTH_DB_PASSWORD}
    volumes:
      - ./db-auth/data/:/var/lib/postgresql/data
    restart: on-failure

  auth:
    image: registry.gitlab.com/companion-to-the-earth/uce-server/auth:$CI_COMMIT_REF_NAME-$STAGE
    environment:
      STAGE: ${STAGE}
      CD_STAGE_TEST: ${CD_STAGE_TEST}
      DB_URI: "postgresql+psycopg2://${AUTH_DB_USER}:${AUTH_DB_PASSWORD}@db-auth/${AUTH_DATABASE}"
      JWT_KEY: ${JWT_KEY}
      WAIT_HOSTS: "db-auth:5432"
    volumes:
      - ./auth/alembic/:/server/framework/alembic
    expose:
      - "5000"
    depends_on:
      - middleware
      - db-auth
    restart: on-failure

  proxy:
    image: registry.gitlab.com/companion-to-the-earth/uce-server/proxy:$CI_COMMIT_REF_NAME-$STAGE
    environment:
      USE_CERTBOT: ${USE_CERTBOT}
      DOMAIN_NAME: ${DOMAIN_NAME}
      CERTBOT_EMAIL: ${CERTBOT_EMAIL}
      VERSION: ${CI_COMMIT_REF_NAME}
    ports:
      - "${PROXY_PORT}:80"
      - "${PROXY_PORT_SSL}:443"
    volumes:
      - ./proxy/letsencrypt:/etc/letsencrypt
    depends_on:
      - auth
      - pins
      - group_management
      - library
    restart: on-failure
