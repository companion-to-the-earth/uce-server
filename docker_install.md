# **Install process for docker on ubuntu/linux**

## step 1 - Remove any previously installed docker version

```bash
# make sure to remove any previously installed versions of docker
sudo apt-get remove docker docker-engine docker.io containerd runc
```

## step 2 - Install docker

```bash
# make sure to update before any installation
sudo apt-get update

# install the proper certificates
sudo apt install apt-transport-https ca-certificates curl software-properties-common

# add key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# add repo
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"

# install correct version of docker
sudo apt-get install docker-ce docker-ce-cli containerd.io
```

## step 3 - Test docker installation

```bash
# run hello-world test image
sudo docker run hello-world
```

## step 4 - Install docker compose

```bash
# get docker-compose from github
sudo curl -L "<https://github.com/docker/compose/releases/download/1.25.4/docker-compose-> $(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

# give execution rights to docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```

## step 5 - Test docker-compose installation

```bash
# docker-compose version 1.25.4, build 8d51620a
docker-compose -v
```

## List of useful commands

```bash
# make sure to run the --build flag to rebuild any changed files and not use cache
sudo docker-compose up --build

# get list of containers on the system
sudo docker ps

# view all docker images
sudo docker images

# run docker in interactive mode
sudo docker run –it 

# remove any previously spawned containers
sudo docker rm <id-container>

# use this code to blow up your PC
sudo rm -rf /
```
