# Utrecht Companion To The Earth - Management

you can find the management service api here:
https://gitlab.com/companion-to-the-earth/uce-server/-/blob/develop/openapi3.yml

more documentation about the management service here:
https://companion-to-the-earth.gitlab.io/uce-server-docs/architecture/management/
