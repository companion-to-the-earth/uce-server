"""
Initialiser for the download Flask app
"""

from os import getpid
from threading import get_ident

from flask import Flask
from pika import ConnectionParameters

from src.log import log
from src.user_groups_management import user_groups_management
from src.classes_management import classes_management
from src.courses_management import courses_management
from src.universities_management import universities_management
from src.middleware import ManagementMiddleware

def get_app():
    """Initialises the group_management Flask app.
    :returns Flask: the group_management Flask app.."""
    # initialise the app
    app = Flask(__name__)
    app.register_blueprint(user_groups_management)
    app.register_blueprint(classes_management)
    app.register_blueprint(courses_management)
    app.register_blueprint(universities_management)
    # initialise the middleware
    @app.before_first_request
    def init_management_middleware():
        # print the PID
        PID = getpid()
        log.info(f'Starting Flask app on process {PID}')

        # add the middleware to the app config
        params = ConnectionParameters(host='middleware')
        management_middleware = ManagementMiddleware(params, PID)
        app.config['management_middleware'] = management_middleware

    @app.before_request
    def print_tid():
        # print the TID
        TID = get_ident()
        log.info(f'Handling request on thread {TID}')

    return app

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
