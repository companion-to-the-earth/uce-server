"""
Authorisation for server content
"""
from flask import current_app as app
from src.middleware import MIDDLEWARE_AUTH
from src.schemas.auth import *

def retrieve_jwt_information(jwt):
    """Get the user information from the jwt"""
    body = UserInfoRequest()
    body.access_token = jwt
    request = AuthRequest()
    request.request_type = RequestType.USER_INFO_REQUEST
    request.request_body = body.pack()

    management_middleware = app.config['management_middleware']
    response = management_middleware.send_message_bare(MIDDLEWARE_AUTH, request.pack())
    #unpack the response
    response = AuthResponse.unpack(response)
    if response.response_type == ResponseType.ERROR_RESPONSE:
        raise Exception(f'{response.error_message}')
    response = UserInfo.unpack(response.response_body)
    return response

def build_authorisation_filter(jwt):
    """builds an sql filter based on the jwt"""
    user_info = retrieve_jwt_information(jwt)
    if user_info.auth_level <= 2:
        auth_sql_filter = f"ClassID='{user_info.class_id}'"
    else:
        auth_sql_filter = ""

    return auth_sql_filter

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
