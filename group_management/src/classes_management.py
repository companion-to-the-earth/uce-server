# TODO exceptions worden hierdoor niet in de codestyle meegenomen
# dit zorgt nu voor een slechte score => exceptions moeten specifieker
#pylint: disable=W0703

from flask import Blueprint, request, jsonify

from src.log import log
from src import utils
from src import auth
from src import query_classes
from src.middleware import DatabaseException

classes_management = Blueprint('classes_management', __name__)

@classes_management.route('/classes/', methods=['GET'], provide_automatic_options=False)
def get_classes():
    """handles get class requests.
    returns all classes with that belong to the id defined in the request"""
    # validate the jwt and get the user data
    try:
        user_data = get_user_data_from_header(request.headers)
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(401, 'Token missing or invalid')
    # check if values are valid
    try:
        key = list(request.args.keys())[0]
        value = list(request.args.values())[0]
        if key not in ['class_id', 'university_id', 'course_id']:
            raise Exception('key not allowed')
        value = int(value)
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(400,
            'Bad request, missing required parameters or otherwise malformed')
    # get all files in the database
    try:
        files = query_classes.get_classes_query(key, value, user_data)
    except DatabaseException as exception:
        log.error(exception)
        return utils.simple_json_response(500, 'Database query failed')
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(403, 
            'Not authorized to perform this action or access this resource')
    return jsonify(files), 200

@classes_management.route('/classes/', methods=['POST'], provide_automatic_options=False)
def create_class():
    """handles POST requests. Creates a class"""
    # validate the jwt and get the user data
    try:
        user_data = get_user_data_from_header(request.headers)
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(401, 'Token missing or invalid')
    #validate values
    try:
        request_data = request.get_json(force=True, silent=True)
        if request_data is None:
            raise Exception('no request data found for creation class')
        request_data['name'] = utils.validate_user_text_input(request_data['name'])
        request_data['course_id'] = int(request_data['course_id'])
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(400,
            'Bad request, missing required parameters or otherwise malformed')
    # generate a random UserGroupID
    random_class_id = utils.generate_random_ID()
    while not query_classes.check_key_validity('classes', 'class_id', random_class_id):
        random_class_id = utils.generate_random_ID()
    # generate a random PinGroupID
    random_pin_group_id = utils.generate_random_ID()
    while not query_classes.check_key_validity('pin_groups', 'pin_group_id', random_pin_group_id):
        random_pin_group_id = utils.generate_random_ID()
    # build Pin Group
    try:
        query_classes.pin_group_create(user_data, 
            {'pin_group_id': random_pin_group_id, 'name': 'pin_group'})
    except DatabaseException as exception:
        log.error(exception)
        return utils.simple_json_response(500, 'Database Pin Group create failed')
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(403, 
            'Not authorized to perform this action or access this resource')
    #set generated id's
    request_data["class_id"] = random_class_id
    request_data["pin_group_id"] = random_pin_group_id
    # create class entry in the database
    try:
        query_classes.class_create(user_data, request_data)
    except DatabaseException as exception:
        log.error(exception)
        return utils.simple_json_response(500, 'Database insert failed')
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(403, 
            'Not authorized to perform this action or access this resource')
    return utils.send_id_return(201,random_class_id)

@classes_management.route('/classes/<class_id>', methods=['GET'], provide_automatic_options=False)
def get_class(class_id):
    """handles get classes requests returns all the classes that belong to the requested class_id"""
    # validate the jwt and get the user data
    try:
        user_data = get_user_data_from_header(request.headers)
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(401, 'Token missing or invalid')
    # get all files in the database
    try:
        class_id = int(class_id)
        files = query_classes.get_classes_query('class_id', class_id, user_data)
        if files != []:
            files = files[0]
    except DatabaseException as exception:
        log.error(exception)
        return utils.simple_json_response(500, 'Database query failed')
    except Exception as exception:
        # TODO Exception handling is too broad,
        # no distinction between auth error and bad request error
        log.error(exception)
        return utils.simple_json_response(400,
            'Bad request, missing required parameters or otherwise malformed')
    return jsonify(files), 200

@classes_management.route('/classes/<class_id>', methods=['PUT'], provide_automatic_options=False)
def update_class(class_id):
    """Handles PUT requests. updates the class with the id and values defined in the request"""
    # validate the jwt and get the user data
    try:
        user_data = get_user_data_from_header(request.headers)
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(401, 'Token missing or invalid')
    #check if values are valid
    try:
        request_data = request.get_json(force=True, silent=True)
        if request_data is None:
            raise Exception('No request data')
        params = {}
        keys = list(request_data.keys())
        for key in keys:
            if key in ['course_id', 'name']:
                params[key] = request_data[key]
        for key in params.keys():
            if key == 'course_id':
                params['course_id'] = int(request_data['course_id'])
            if key == 'name':
                params['name'] = utils.validate_user_text_input(request_data['name'])
        class_id = int(class_id)
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(400,
            'Bad request, missing required parameters or otherwise malformed')
    # update the group information
    try:
        query_classes.class_update(class_id, params, user_data)
    except DatabaseException as exception:
        log.error(exception)
        return utils.simple_json_response(500, 'Database query failed')
    except Exception as exception:
        # TODO Exception handling is too broad,
        # no distinction between auth error and bad request error
        log.error(exception)
        return utils.simple_json_response(400,
            'Bad request, missing required parameters or otherwise malformed')
    return utils.simple_json_response(200, 'Class updated')

@classes_management.route('/classes/<class_id>', 
    methods=['DELETE'], provide_automatic_options=False)
def delete_class(class_id):
    """Handles DELETE requests. Deletes the class with the id defined in the request"""
    # validate the jwt and get the user data
    try:
        user_data = get_user_data_from_header(request.headers)
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(401, 'Token missing or invalid')
    # get all files in the database
    try:
        class_id = int(class_id)
        query_classes.class_delete(class_id, user_data)
    except DatabaseException as exception:
        log.error(exception)
        return utils.simple_json_response(500, 'Database query failed')
    except Exception as exception:
        # TODO Exception handling is too broad,
        # no distinction between auth error and bad request error
        log.error(exception)
        return utils.simple_json_response(400,
            'Bad request, missing required parameters or otherwise malformed')
    return utils.simple_json_response(200, 'Class deleted')

def get_user_data_from_header(headers):
    """ gets the user data from the header """
    jwt = utils.get_JWT(headers)
    user_data = query_classes.get_user_data(jwt)
    return user_data

#
# Internal API endpoints
#

@classes_management.route('/internal/classes', methods=['GET'])
def get_classes_internal():
    """Internal API endpoint to get all available class ids"""
    try:
        classes = query_classes.get_all_class_ids()
    except DatabaseException as exception:
        log.error(exception)
        return utils.simple_json_response(500, 'Database query failed')
    return jsonify(classes), 200

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
