# TODO exceptions worden hierdoor niet in de codestyle meegenomen
# dit zorgt nu voor een slechte score => exceptions moeten specifieker
#pylint: disable=W0703

from flask import Blueprint, request, jsonify

from src.log import log
from src import utils
from src import auth
from src import query_courses
from src.middleware import DatabaseException

courses_management = Blueprint('courses_management', __name__)

@courses_management.route('/courses/', methods=['GET'], provide_automatic_options=False)
def get_courses():
    """handles get courses requests.
    returns all courses with the defined university id"""
    # validate the jwt and get the user data
    try:
        user_data = get_user_data_from_header(request.headers)
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(401, 'Token missing or invalid')
    # validate user input
    try:
        key = list(request.args.keys())[0]
        value = list(request.args.values())[0]
        if key != 'university_id':
            return utils.simple_json_response(400,
                'Bad request, missing required parameters or otherwise malformed')
        value = int(value)
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(400,
            'Bad request, missing required parameters or otherwise malformed')
    # get all files in the database
    try:
        files = query_courses.get_courses_query(key, value, user_data)
    except DatabaseException as exception:
        log.error(exception)
        return utils.simple_json_response(500, 'Database query failed')
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(403,
            "Not authorized to perform this action or access this resource")
    return jsonify(files), 200

@courses_management.route('/courses/', methods=['POST'], provide_automatic_options=False)
def create_course():
    """handles POST requests. Creates a course"""
    # validate the jwt and get the user data
    try:
        user_data = get_user_data_from_header(request.headers)
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(401, 'Token missing or invalid')
    #validate values
    try:
        request_data = request.get_json(force=True, silent=True)
        if request_data is None:
            raise Exception('no data found for course creation')
        request_data['name'] = utils.validate_user_text_input(request_data['name'])
        request_data['university_id'] = int(request_data['university_id'])
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(400,
            'Bad request, missing required parameters or otherwise malformed')
    # generate a random course id
    random_course_id = utils.generate_random_ID()
    while not query_courses.check_key_validity('courses', 'course_id', random_course_id):
        random_course_id = utils.generate_random_ID()

    request_data["course_id"] = random_course_id
    # get all files in the database
    try:
        query_courses.course_create(user_data, request_data)
    except DatabaseException as exception:
        log.error(exception)
        return utils.simple_json_response(500, 'Database insert failed')
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(403,
            'Not authorized to perform this action or access this resource')

    return utils.send_id_return(201,random_course_id)

@courses_management.route('/courses/<course_id>', 
    methods=['GET'], provide_automatic_options=False)
def get_course(course_id):
    """handles get course requests.
    returns a specific course with the defined course id"""
    # validate the jwt and get the user data
    try:
        user_data = get_user_data_from_header(request.headers)
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(401, 'Token missing or invalid')
    # get all files in the database
    try:
        course_id = int(course_id)
        files = query_courses.get_courses_query('course_id', course_id, user_data)
        if files != []:
            files = files[0]
    except DatabaseException as exception:
        log.error(exception)
        return utils.simple_json_response(500, 'Database query failed')
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(401,
            'Token missing or invalid')
    return jsonify(files), 200

@courses_management.route('/courses/<course_id>', methods=['DELETE'], provide_automatic_options=False)
def delete_course(course_id):
    """Handles DELETE requests. Deletes the course with the id defined in the request"""
    # validate the jwt and get the user data
    try:
        user_data = get_user_data_from_header(request.headers)
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(401, 'Token missing or invalid')
    # get all files in the database
    try:
        course_id = int(course_id)
        query_courses.course_delete(course_id, user_data)
    except DatabaseException as exception:
        log.error(exception)
        return utils.simple_json_response(400,
                    'Bad request, missing required parameters or otherwise malformed')
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(400, exception.args[0])
    return utils.simple_json_response(200, 'Course deleted')

@courses_management.route('/courses/<course_id>', methods=['PUT'], provide_automatic_options=False)
def update_course(course_id):
    """Handles PUT requests. updates the course with the id and values defined in the request"""
    # validate the jwt and get the user data
    try:
        user_data = get_user_data_from_header(request.headers)
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(401, 'Token missing or invalid')
    try:
        # validate input
        request_data = request.get_json(force=True, silent=True)
        if request_data is None:
            raise Exception('no data found')
        params = {}
        keys = list(request_data.keys())
        for key in keys:
            if key in ['university_id', 'name']:
                params[key] = request_data[key]
        for key in params.keys():
            if(key == 'university_id'):
                params['university_id'] = int(request_data['university_id'])
            if(key == 'name'):
                params['name'] = utils.validate_user_text_input(request_data['name'])
        course_id = int(course_id)
         # update the course information
        query_courses.course_update(course_id, params, user_data)
    except DatabaseException as exception:
        log.error(exception)
        return utils.simple_json_response(500, 'Database query failed')
    except Exception as exception:
        log.error(exception)
        # TODO no difference between key/request error
        return utils.simple_json_response(400,
                    'Bad request, missing required parameters or otherwise malformed')
    return utils.simple_json_response(200, 'Course updated')

def get_user_data_from_header(headers):
    """ gets the user data from the header """
    jwt = utils.get_JWT(headers)
    user_data = query_courses.get_user_data(jwt)
    return user_data

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
