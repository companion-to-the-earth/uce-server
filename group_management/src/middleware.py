"""
Interaction with query-able services
"""

from threading import get_ident
from ast import literal_eval
from src.schemas.auth import *
from lib.middleware import Middleware

MIDDLEWARE_DB = 'middleware-db'
MIDDLEWARE_AUTH = 'auth'

class DatabaseException(Exception):
    """
    Database exception.
    """

class ManagementMiddleware(Middleware):
    """
    Implementation of Middleware for querying other services.
    Handles the sending, recieving and parsing of messages.
    """
    __responses = {}

    def __init__(self, connection_params, pid):
        self.__queue_name = f'middleware-group_management-{pid}'
        super().__init__(connection_params)

    def exec_query(self, recipient, message):
        """
        Sends a query to a service.

        :param str recipient: the service
        :param str message: the query
        :return obj: the result
        """

        thread_id = get_ident()
        thread_queue = f'{self.__queue_name}-{thread_id}'
        self.publish(recipient, message, thread_queue)
        self.consume(thread_queue, False)

        # Get result and remove it immediately
        result = self.__responses[thread_id]
        self.__responses[thread_id] = None

        body_decoded = result.decode()
        # try to parse the response
        try:
            body_parsed = literal_eval(body_decoded)
        except ValueError:
            raise DatabaseException(f'Could not parse database response: {body_decoded}')
        except Exception:
            raise DatabaseException(f'Unhandled database exception: {body_decoded}')

        return body_parsed

    def send_message_bare(self, recipient, message):
        """
        Sends a query to a service.

        :param str recipient: the service
        :param str message: the query
        :return obj: the result
        """

        thread_id = get_ident()
        thread_queue = f'{self.__queue_name}-{thread_id}'
        self.publish(recipient, message, thread_queue)
        self.consume(thread_queue, False)

        # Get result and remove it immediately
        result = self.__responses[thread_id]
        self.__responses[thread_id] = None
        
        return result
    def on_message_received(self, callback, method, properties, body):
        """ValueError
        Parses and saves incoming messages.

        :raises DatabaseException: unable to parse message
        """
        # save the response
        self.__responses[get_ident()] = body

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
