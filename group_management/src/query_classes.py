"""
Queries for other services
"""

from flask import current_app as app
from src.middleware import MIDDLEWARE_DB, MIDDLEWARE_AUTH
from src.middleware import DatabaseException
from src import query_user_groups
from src.schemas.auth import *

def get_all_class_ids():
    """Queries the database for all available classes and returns their ids"""
    query = "SELECT class_id FROM classes;|()"

    management_middleware = app.config['management_middleware']
    resp = management_middleware.exec_query(MIDDLEWARE_DB, query)

    if resp is None:
        return []

    classes = []
    for entry in resp:
        classes.append(entry[0])

    return classes

def get_classes_query(key, value, user_data):
    """Queries groups from the database with the given key and value"""

    if user_data.auth_level < 2:
        raise Exception("Not authorized to perform this action or access this resource")

    if key == 'course_id':
        query = f"SELECT * " \
                f"FROM classes " \
                f"WHERE course_id=%s;|('{value}',)"
    elif key == 'university_id':
        query = f"SELECT classes.class_id, classes.name, " \
                f"classes.pin_group_id, classes.course_id " \
                f"FROM (classes " \
                f"INNER JOIN courses " \
                f"ON classes.course_id = courses.course_id) " \
                f"WHERE university_id=%s;|('{value}',)"
    elif key == 'class_id':
        query = f"SELECT *" \
                f"FROM classes " \
                f"WHERE class_id=%s;|('{value}',)"

    # get the response from the database
    management_middleware = app.config['management_middleware']
    resp = management_middleware.exec_query(MIDDLEWARE_DB, query)
    # parse the response
    files = []
    for file in resp:
        file_data = {}
        file_data['class_id'] = file[0]
        file_data['class_id_str'] = f'{file[0]}'
        file_data['name'] = file[1]
        file_data['course_id'] = file[3]
        file_data['course_id_str'] = f'{file[3]}'
        files.append(file_data)
    return files

def class_create(user_data, new_class_values = None):
    """create class"""
    if user_data.auth_level < 2:
        raise Exception("Not authorized to perform this action or access this resource")

    if new_class_values is None:
        return
    query = f"INSERT INTO classes (class_id, name, pin_group_id, course_id)" \
            f" VALUES (%s, %s, %s, %s);|" \
            f"('{new_class_values['class_id']}', '{new_class_values['name']}'," \
            f" '{new_class_values['pin_group_id']}', '{new_class_values['course_id']}')"

    # get the response from the database
    management_middleware = app.config['management_middleware']
    management_middleware.exec_query(MIDDLEWARE_DB, query)
    # parse the response
    return

def pin_group_create(user_data, new_pin_values = None):
    """create pin group"""
    if user_data.auth_level < 2:
        raise Exception("Not authorized to perform this action or access this resource")
    if new_pin_values is None:
        return
    query = f"INSERT INTO pin_groups (pin_group_id, name) VALUES (%s, %s);|" \
            f"('{new_pin_values['pin_group_id']}', '{new_pin_values['name']}')"
    # get the response from the database
    management_middleware = app.config['management_middleware']
    #hier loopt hij vast
    management_middleware.exec_query(MIDDLEWARE_DB, query)
    # parse the response
    return

def class_delete(class_id, user_data):
    """Deletes a specific group with id class_id """
    if user_data.auth_level < 2:
        raise Exception("Not authorized to perform this action or access this resource")

    #check if the class is empty
    management_middleware = app.config['management_middleware']
    member_count = class_member_count_request(int(class_id))
    #check if class doesn't contain user group's
    query = f"SELECT user_group_id " \
            f"FROM user_groups " \
            f"WHERE class_id=%s;|({class_id},)"
    resp = management_middleware.exec_query(MIDDLEWARE_DB, query)
    if len(resp) > 0 or member_count > 0:
        raise Exception("Class not empty")
    #get pin_group_id of the class
    query = f"SELECT pin_group_id " \
            f"FROM classes " \
            f"WHERE class_id=%s;|({class_id},)"
    resp = management_middleware.exec_query(MIDDLEWARE_DB, query)
    if not resp:
        raise Exception("class id doesn't exist")
    pin_group_id = resp[0][0]
    #delete pin_group's
    query = f"DELETE FROM pin_groups " \
            f"WHERE pin_group_id=%s;|({pin_group_id},)"
    management_middleware.exec_query(MIDDLEWARE_DB, query)
    #delete class
    query = f"DELETE FROM classes " \
            f"WHERE class_id=%s;|({class_id},)"
    management_middleware.exec_query(MIDDLEWARE_DB, query)

def class_member_count_request(class_id):
    """Get the amount of members in a class"""
    body = ClassMemberCountRequest()
    body.class_id = class_id
    request = AuthRequest()
    request.request_type = RequestType.CLASS_MEMBER_COUNT_REQUEST
    request.request_body = body.pack()

    management_middleware = app.config['management_middleware']
    response = management_middleware.send_message_bare(MIDDLEWARE_AUTH, request.pack())
    #unpack the response
    response = AuthResponse.unpack(response)
    if response.response_type == ResponseType.ERROR_RESPONSE:
        print("FAILED___________________________________")
    response = ClassMemberCountResponse.unpack(response.response_body)
    return response.member_count

def class_update(class_id, class_info, user_data):
    """Update class information in the Classes table"""
    if user_data.auth_level < 2:
        raise Exception("Not authorized to perform this action or access this resource")

    management_middleware = app.config['management_middleware']
    if class_id is None:
        raise ValueError('one of the variables is empty')
    #check if id exists
    query = f"SELECT * " \
            f"FROM classes " \
            f"WHERE class_id=%s;|({class_id},)"
    resp = management_middleware.exec_query(MIDDLEWARE_DB, query)
    if len(resp) == 0:
        raise Exception("Class is non existing")
    #build query
    query = "UPDATE classes SET "
    for key in class_info.keys():
        query += f"{key}=%s, "
    query = query[:-2]
    query += " WHERE class_id=%s;|("
    for key in class_info.keys():
        query += f"'{class_info[key]}',"
    query += f"'{class_id}',)"
    #send query to db
    management_middleware.exec_query(MIDDLEWARE_DB, query)

def check_key_validity(table_name, variable_name, primary_key):
    """ checks if pk exists in the pin database, usefull for generating unique ids """
    #TODO: write test to check if this works
    query = f"SELECT %s FROM %s WHERE %s = %s;|" \
            f"('{variable_name}','{table_name}','{variable_name}','{primary_key}')"

    management_middleware = app.config['management_middleware']
    resp = management_middleware.exec_query(MIDDLEWARE_DB, query)
    # if the response is not null it means the primary key does exist in the database
    if resp is None:
        return True
    return False

def get_user_data(access_token):
    """ gets user data associated with the token """
    management_middleware = app.config['management_middleware']

    if access_token is None:
        raise Exception("Token missing or invalid")

    body = UserInfoRequest()
    body.access_token = access_token
    request = AuthRequest()
    request.request_type = RequestType.USER_INFO_REQUEST
    request.request_body = body.pack()
    response = management_middleware.send_message_bare(MIDDLEWARE_AUTH, request.pack())
    response = AuthResponse.unpack(response)

    if response.response_type == ResponseType.ERROR_RESPONSE:
        raise Exception("Token missing or invalid")

    user_info = UserInfo.unpack(response.response_body)
    return user_info

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
