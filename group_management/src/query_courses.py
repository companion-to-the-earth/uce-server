"""
Queries for other services
"""

from flask import current_app as app
from src.middleware import MIDDLEWARE_DB, MIDDLEWARE_AUTH
from src.schemas.auth import *

def get_courses_query(key, value, user_data):
    """Queries a course from the database with the given key value pair"""

    if user_data.auth_level < 1:
        raise Exception("Not authorized to perform this action or access this resource")

    if key == 'university_id':
        query = f"SELECT *" \
                f"FROM courses " \
                f"WHERE university_id=%s;|('{value}',)"
    elif key == 'course_id':
        query = f"SELECT *" \
                f"FROM courses " \
                f"WHERE course_id=%s;|('{value}',)"

    # get the response from the database
    management_middleware = app.config['management_middleware']
    resp = management_middleware.exec_query(MIDDLEWARE_DB, query)
    # parse the response
    files = []
    for file in resp:
        file_data = {}
        file_data['course_id'] = file[0]
        file_data['course_id_str'] = f'{file[0]}'
        file_data['name'] = file[1]
        file_data['university_id'] = file[2]
        file_data['university_id_str'] = f'{file[2]}'
        files.append(file_data)
    return files

def course_create(user_data, new_class_values = None):
    """create course query"""

    if user_data.auth_level < 2:
        raise Exception("Not authorized to perform this action or access this resource")

    query = f"INSERT INTO courses (course_id, name, university_id)" \
            f" VALUES (%s, %s, %s);|" \
            f"('{new_class_values['course_id']}', '{new_class_values['name']}'," \
            f" '{new_class_values['university_id']}')"

    # get the response from the database
    management_middleware = app.config['management_middleware']
    management_middleware.exec_query(MIDDLEWARE_DB, query)
    # parse the response
    return

def check_key_validity(table_name, variable_name, primary_key):
    """checks if pk exists in the database, usefull for generating unique ids """
    #TODO: write test to check if this works
    query = f"SELECT %s FROM %s WHERE %s = %s;|" \
            f"('{variable_name}','{table_name}','{variable_name}','{primary_key}')"

    management_middleware = app.config['management_middleware']
    resp = management_middleware.exec_query(MIDDLEWARE_DB, query)
    # if the response is not null it means the primary key does exist in the database
    if resp is None:
        return True
    return False

def course_delete(course_id, user_data):
    """Deletes a specific course with given course_id """
    if user_data.auth_level < 2:
        raise Exception("Not authorized to perform this action or access this resource")

    management_middleware = app.config['management_middleware']
    #check if course doesn't contain classes
    query = f"SELECT class_id " \
            f"FROM classes " \
            f"WHERE course_id=%s;|({course_id},)"
    resp = management_middleware.exec_query(MIDDLEWARE_DB, query)
    if len(resp) > 0:
        raise Exception("Course not empty")
    #check if course exists
    query = f"SELECT course_id " \
            f"FROM courses " \
            f"WHERE course_id=%s;|({course_id},)"
    resp = management_middleware.exec_query(MIDDLEWARE_DB, query)
    if len(resp) == 0:
        raise Exception("Course does not exist")
    #delete class
    query = f"DELETE FROM courses " \
            f"WHERE course_id=%s;|({course_id},)"
    management_middleware.exec_query(MIDDLEWARE_DB, query)

def course_update(course_id, course_info, user_data):
    """Update course information in the Courses table"""
    if user_data.auth_level < 2:
        raise Exception("Not authorized to perform this action or access this resource")

    management_middleware = app.config['management_middleware']
    if course_id is None:
        raise ValueError('one of the variables is empty')
    #check if id exists
    query = f"SELECT * " \
            f"FROM courses " \
            f"WHERE course_id=%s;|({course_id},)"
    resp = management_middleware.exec_query(MIDDLEWARE_DB, query)
    if len(resp) == 0:
        raise Exception("Course doe not exist")

    #build query
    query = "UPDATE courses SET "
    for key in course_info.keys():
        query += f"{key}=%s, "
    query = query[:-2]
    query += " WHERE course_id=%s;|("
    for key in course_info.keys():
        query += f"'{course_info[key]}',"
    query += f"'{course_id}',)"
    #send query to db
    management_middleware.exec_query(MIDDLEWARE_DB, query)

def get_user_data(access_token):
    """ gets user data associated with the token """
    management_middleware = app.config['management_middleware']

    if access_token is None:
        raise Exception("Token missing or invalid")

    body = UserInfoRequest()
    body.access_token = access_token
    request = AuthRequest()
    request.request_type = RequestType.USER_INFO_REQUEST
    request.request_body = body.pack()
    response = management_middleware.send_message_bare(MIDDLEWARE_AUTH, request.pack())
    response = AuthResponse.unpack(response)

    if response.response_type == ResponseType.ERROR_RESPONSE:
        raise Exception("Token missing or invalid")

    user_info = UserInfo.unpack(response.response_body)
    return user_info

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
