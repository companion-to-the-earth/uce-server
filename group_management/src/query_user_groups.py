"""
Queries for other services
"""

from flask import current_app as app
from src.middleware import MIDDLEWARE_DB, MIDDLEWARE_AUTH
from src.middleware import DatabaseException
from src.schemas.auth import *

def get_all_group_ids(class_id):
    """ Queries the database for all available user groupts within
        the specified class and returns their ids                   """
    query = f"SELECT user_group_id FROM user_groups WHERE class_id=%s;|('{class_id}',)"

    management_middleware = app.config['management_middleware']
    resp = management_middleware.exec_query(MIDDLEWARE_DB, query)

    if resp is None:
        return []

    groups = []
    for entry in resp:
        groups.append(entry[0])

    return groups

def group_query(key, value, user_data):
    """Queries groups from the database with the given key, value pair """

    management_middleware = app.config['management_middleware']
    query_allowed = False

    if key == 'user_id':
        # first check if the user has access
        if value == user_data.id or user_data.auth_level >= 2:
            query_allowed = True
            query = f"SELECT user_groups.user_group_id, user_groups.name, " \
                    f"user_groups.class_id, " \
                    f"user_groups.pin_group_id, " \
                    f"user_groups.max_user_count " \
                    f"FROM (user_groups " \
                    f"INNER JOIN users_per_user_group " \
                    f"ON user_groups.user_group_id = users_per_user_group.user_group_id) " \
                    f"WHERE user_id=%s AND %s;|('{value}',)"
    elif key == 'course_id':
        # first check if the user has access
        if user_data.auth_level >= 2:
            query_allowed = True
            query = f"SELECT user_groups.user_group_id, user_groups.name, " \
                    f"user_groups.class_id, " \
                    f"user_groups.pin_group_id, " \
                    f"user_groups.max_user_count " \
                    f"FROM (user_groups " \
                    f"INNER JOIN classes " \
                    f"ON user_groups.class_id = classes.class_id) " \
                    f"WHERE classes.course_id=%s;|('{value}',)"
    elif key == 'university_id':
        # first check if the user has access
        if user_data.auth_level >= 2:
            query_allowed = True
            query = f"SELECT user_groups.user_group_id, user_groups.name, " \
                    f"user_groups.class_id, " \
                    f"user_groups.pin_group_id, " \
                    f"user_groups.max_user_count " \
                    f"FROM (user_groups " \
                    f"INNER JOIN classes " \
                    f"ON user_groups.class_id = classes.class_id " \
                    f"INNER JOIN courses " \
                    f"ON classes.course_id = courses.course_id) " \
                    f"WHERE courses.university_id=%s;|('{value}',)"
    elif key == 'class_id':
        # first check if the user has access
        if user_data.class_id == value or user_data.auth_level >= 2:
            query_allowed = True
            query = f"SELECT * " \
                    f"FROM user_groups " \
                    f"WHERE class_id=%s;|('{value}',)"
    elif key == 'user_group_id':
        # get the class id from the group to check for authorization levels
        class_id = management_middleware.exec_query(MIDDLEWARE_DB,
                "SELECT class_id " \
                f"FROM user_groups WHERE user_group_id = %s;|('{id}',)")
        if len(class_id) > 0:
            class_id = class_id[0][0]
        if user_data.class_id == class_id or user_data.auth_level >= 2:
                query_allowed = True
                query = f"SELECT * " \
                        f"FROM user_groups " \
                        f"WHERE user_group_id=%s;|('{value}',)"

    # get the pindata from the response
    if not query_allowed:
        raise Exception("Not authorized to perform this action or access this resource")

    # get the response from the database
    resp = management_middleware.exec_query(MIDDLEWARE_DB, query)
    # parse the response
    files = []
    for file in resp:
        file_data = {}
        file_data['user_group_id'] = file[0]
        file_data['user_group_id_str'] = f'{file[0]}'
        file_data['name'] = file[1]
        file_data['class_id'] = file[2]
        file_data['class_id_str'] = f'{file[2]}'
        file_data['max_user_count'] = file[4]
        file_data['members'] = get_users_per_group(file[0])
        files.append(file_data)
    return files

def get_users_per_group(group_id):
    """returns all members in a user group"""
    query = f"SELECT *" \
            f"FROM users_per_user_group " \
            f"WHERE user_group_id=%s;|('{group_id}',)"
    # get the response from the database
    management_middleware = app.config['management_middleware']
    resp = management_middleware.exec_query(MIDDLEWARE_DB, query)
    files = []
    for file in resp:
        file_data = {}
        file_data['user_id'] = file[0]
        file_data['user_id_str'] = f'{file[0]}'
        files.append(file_data)
    return files

def group_create(new_group_values = None):
    """create user group"""
    #TODO: make an pinGroup when creating an group.
    if new_group_values is None:
        return
    query = f"INSERT INTO user_groups (user_group_id, name, class_id, " \
            f"pin_group_id, max_user_count)" \
            f" VALUES (%s, %s, %s, %s, %s);|" \
            f"('{new_group_values['user_group_id']}', '{new_group_values['name']}'," \
            f" '{new_group_values['class_id']}', '{new_group_values['pin_group_id']}', " \
            f"'{new_group_values['max_user_count']}')"

    # get the response from the database
    management_middleware = app.config['management_middleware']
    management_middleware.exec_query(MIDDLEWARE_DB, query)
    # parse the response
    return
def pin_group_create(new_pin_values = None):
    """create pin group"""
    #TODO: make an pinGroup when creating an group.
    if new_pin_values is None:
        return
    query = f"INSERT INTO pin_groups (pin_group_id, name) VALUES (%s, %s);|" \
            f"('{new_pin_values['pin_group_id']}', '{new_pin_values['name']}')"
    # get the response from the database
    management_middleware = app.config['management_middleware']
    #hier loopt hij vast
    management_middleware.exec_query(MIDDLEWARE_DB, query)
    # parse the response
    return

def group_delete(group_id, user_data):
    """Deletes a specific group with the given group_id """

    if user_data.auth_level < 2:
        raise Exception("Not authorized to perform this action or access this resource")

    #TODO: remove the user group and the entries in Users_Per_User_Group
    # bij group verwijderen wordt pingroup verwijderd en de entries in de Users_Per_User_Group
    #drie stappen:
    #get pinGroupID
    query = f"SELECT pin_group_id " \
            f"FROM user_groups " \
            f"WHERE user_group_id=%s;|({group_id},)"

    #get the PinGroupID from the database
    management_middleware = app.config['management_middleware']
    resp = management_middleware.exec_query(MIDDLEWARE_DB, query)
    if not resp:
        raise DatabaseException("wrong id")
    pin_group_id = resp[0][0]
    #delete user_Groups
    query = f"DELETE FROM user_groups " \
            f"WHERE user_group_id=%s;|({group_id},)"
    management_middleware.exec_query(MIDDLEWARE_DB, query)
    #delete Pin_Group
    query = f"DELETE FROM pin_groups " \
            f"WHERE pin_group_id=%s;|({pin_group_id},)"
    management_middleware.exec_query(MIDDLEWARE_DB, query)

def get_max_user_count(group_id):
    """Gets the max users of a user group"""
    query = f"SELECT max_user_count FROM user_groups WHERE user_group_id = %s;|" \
            f"('{group_id}',)"
    management_middleware = app.config['management_middleware']
    resp = management_middleware.exec_query(MIDDLEWARE_DB, query)
    if not resp:
        raise Exception('user group id does not exist')
    return resp[0][0]

def get_user_count(group_id):
    """Gets the amount of users in a user group"""
    query = f"SELECT COUNT(*) FROM users_per_user_group WHERE user_group_id = %s;|" \
        f"('{group_id}',)"

    management_middleware = app.config['management_middleware']
    resp = management_middleware.exec_query(MIDDLEWARE_DB, query)
    return resp[0][0]

def group_join(group_id, user_data):
    """Adds user to a group in the Users_Per_Group table"""
    user_id = user_data.id
    management_middleware = app.config['management_middleware']

    # if the auth level is < 2, check if the user is in the same class as the user group
    if user_data.auth_level < 2:

        class_id = management_middleware.exec_query(MIDDLEWARE_DB,
                    f"SELECT class_id FROM user_groups WHERE user_group_id = %s;|" \
                    f"('{group_id}',)")
        if len(class_id) > 0:
            class_id = class_id[0][0]
            # if the student is not in the class of the user group, reject
            if class_id != user_data.class_id:
                raise Exception("Not authorized to perform this action or access this resource")
        else:
            raise DatabaseException('User is not part of a class')

    if user_id is None or group_id is None:
        raise DatabaseException('one of the variables is empty')

    query = f"INSERT INTO users_per_user_group (user_id, user_group_id) VALUES (%s, %s);|" \
            f"('{user_id}', '{group_id}')"

    management_middleware.exec_query(MIDDLEWARE_DB, query)

def internal_group_join(group_id, user_id):
    """ Adds user to a group in the Users_Per_Group table
        NO AUTHENTICATION REQUIRED internal use only      """

    management_middleware = app.config['management_middleware']

    if user_id is None or group_id is None:
        raise DatabaseException('one of the variables is empty')

    query = f"INSERT INTO users_per_user_group (user_id, user_group_id) VALUES (%s, %s);|" \
            f"('{user_id}', '{group_id}')"

    management_middleware.exec_query(MIDDLEWARE_DB, query)

def group_leave(user_id):
    """removes user from a group in the users_per_group table"""
    if user_id is None:
        raise DatabaseException('one of the variables is empty')

    query = f"DELETE FROM users_per_user_group WHERE user_id=%s;|" \
            f"('{user_id}',)"

    management_middleware = app.config['management_middleware']
    management_middleware.exec_query(MIDDLEWARE_DB, query)

def kick_user(user_id):
    """removes user from a group in the users_per_group table"""
    query = f"DELETE FROM users_per_user_group WHERE user_id=%s;|" \
            f"('{user_id}',)"

    management_middleware = app.config['management_middleware']
    management_middleware.exec_query(MIDDLEWARE_DB, query)

def group_update(group_id, group_info):
    """Update user group information in the User_Groups table"""
    if group_id is None:
        raise ValueError('one of the variables is empty')
    #build query
    query = "UPDATE user_groups SET "
    for key in group_info.keys():
        query += f"{key}=%s, "
    query = query[:-2]
    query += " WHERE user_group_id=%s;|("
    for key in group_info.keys():
        query += f"'{group_info[key]}',"
    query += f"'{group_id}',)"
    #send query to db
    management_middleware = app.config['management_middleware']
    management_middleware.exec_query(MIDDLEWARE_DB, query)

def check_key_validity(table_name, variable_name, primary_key):
    """ checks if pk exists in the pin database, usefull for generating unique ids """
    #TODO: write test to check if this works
    query = f"SELECT %s FROM %s WHERE %s = %s;|" \
            f"('{variable_name}','{table_name}','{variable_name}','{primary_key}')"

    management_middleware = app.config['management_middleware']
    resp = management_middleware.exec_query(MIDDLEWARE_DB, query)
    # if the response is not null it means the primary key does exist in the database
    if resp is None:
        return True
    return False

def get_user_data(access_token):
    """ gets user data associated with the token """
    management_middleware = app.config['management_middleware']

    if access_token is None:
        raise Exception("Token missing or invalid")

    body = UserInfoRequest()
    body.access_token = access_token
    request = AuthRequest()
    request.request_type = RequestType.USER_INFO_REQUEST
    request.request_body = body.pack()
    response = management_middleware.send_message_bare(MIDDLEWARE_AUTH, request.pack())
    response = AuthResponse.unpack(response)

    if response.response_type == ResponseType.ERROR_RESPONSE:
        raise Exception("Token missing or invalid")

    user_info = UserInfo.unpack(response.response_body)
    return user_info

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
