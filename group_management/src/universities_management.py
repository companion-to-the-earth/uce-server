from flask import Blueprint, request, jsonify

from src.log import log
from src import utils
from src import auth
from src import query_courses
from src.middleware import DatabaseException

universities_management = Blueprint('universities_management', __name__)

@universities_management.route('/universities/', methods=['GET'], provide_automatic_options=False)
def get_universities():
    return utils.simple_json_response(501, 'This request is not implemented yet.')

@universities_management.route('/universities/', methods=['POST'], provide_automatic_options=False)
def create_university():
    return utils.simple_json_response(501, 'This request is not implemented yet.')

@universities_management.route('/universities/<id>', methods=['GET'], provide_automatic_options=False)
def get_university(id):
    return utils.simple_json_response(501, 'This request is not implemented yet.')

@universities_management.route('/universities/<id>', methods=['PUT'], provide_automatic_options=False)
def update_university(id):
    return utils.simple_json_response(501, 'This request is not implemented yet.')

@universities_management.route('/universities/<id>', methods=['DELETE'], provide_automatic_options=False)
def delete_university(id):
    return utils.simple_json_response(501, 'This request is not implemented yet.')