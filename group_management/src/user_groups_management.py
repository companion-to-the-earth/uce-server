"""
user_groups_management blueprint; handles user group requests
"""

# TODO exceptions worden hierdoor niet in de codestyle meegenomen
# dit zorgt nu voor een slechte score => exceptions moeten specifieker
#pylint: disable=W0703

from flask import Blueprint, request, jsonify

from src.log import log
from src import utils
from src import auth
from src import query_user_groups
from src.middleware import DatabaseException

user_groups_management = Blueprint('user_groups_management', __name__)

@user_groups_management.route('/user-groups/', methods=['GET'], provide_automatic_options=False)
def get_groups():
    """handles get user_group requests.
    returns all groups when no id is defined"""
    # validate the jwt and get the user data
    try:
        user_data = get_user_data_from_header(request.headers)
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(401, 'Token missing or invalid')

    # check if the id is a valid integer value
    try:
        key = list(request.args.keys())[0]
        value = list(request.args.values())[0]
        if key not in ['user_id', 'class_id', 'university_id', 'course_id']:
            raise Exception("key not allowed")
        value = int(value)
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(400,
            'Bad request, missing required parameters or otherwise malformed')
    # get all files in the database
    try:
        files = query_user_groups.group_query(key, value, user_data)
    except DatabaseException as exception:
        log.error(exception)
        return utils.simple_json_response(500, 'Database query failed')
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(403,
            "Not authorized to perform this action or access this resource")
    return jsonify(files), 200

@user_groups_management.route('/user-groups/<group_id>', 
    methods=['GET'], provide_automatic_options=False)
def get_group(group_id):
    """handles get user_group requests.
    returns the info group with the defined user group id"""
    # validate the jwt and get the user data
    try:
        user_data = get_user_data_from_header(request.headers)
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(401, 'Token missing or invalid')

    #check if id is valid integer
    try:
        group_id = int(group_id)
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(400,
            'Bad request, missing required parameters or otherwise malformed')
    # get all files in the database
    try:
        files = query_user_groups.group_query('user_group_id', group_id, user_data)
        if files != []:
            files = files[0]
    except DatabaseException as exception:
        log.error(exception)
        return utils.simple_json_response(500, 'Database query failed')
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(403,
            "Not authorized to perform this action or access this resource")

    return jsonify(files), 200

@user_groups_management.route('/user-groups/', 
    methods=['POST'], provide_automatic_options=False)
def create_group():
    """handles POST requests. Creates a group"""
    # validate the jwt and get the user data
    try:
        user_data = get_user_data_from_header(request.headers)
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(401, 'Token missing or invalid')

    # handle the auth levels at the start of the request
    if user_data.auth_level < 2:
        log.error("Not authorized to perform this action or access this resource")
        return utils.simple_json_response(403,
            "Not authorized to perform this action or access this resource")

    #check if given values are valid
    try:
        request_data = request.get_json(force=True, silent=True)
        if request_data is None:
            raise Exception('no request data found')
        request_data['name'] = utils.validate_user_text_input(request_data['name'])
        request_data['class_id'] = int(request_data['class_id'])
        request_data['max_user_count'] = int(request_data['max_user_count'])
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(400,
            'Bad request, missing required parameters or otherwise malformed')
    # generate a random UserGroupID
    random_group_id = utils.generate_random_ID()
    while not query_user_groups.check_key_validity('user_groups', \
            'user_group_id', random_group_id):
        random_group_id = utils.generate_random_ID()
    # generate a random PinGroupID
    random_pin_group_id = utils.generate_random_ID()
    while not query_user_groups.check_key_validity('pin_groups', \
            'pin_group_id', random_pin_group_id):
        random_pin_group_id = utils.generate_random_ID()
    # build Pin Group
    try:
        query_user_groups.pin_group_create({'pin_group_id': random_pin_group_id, 
            'name': 'pin_group'})
    except DatabaseException as exception:
        log.error(exception)
        return utils.simple_json_response(500, 'Database Pin Group create failed')

    #set UserGroupID
    request_data["user_group_id"] = random_group_id
    request_data["pin_group_id"] = random_pin_group_id
    # get all files in the database
    try:
        query_user_groups.group_create(request_data)
    except DatabaseException as exception:
        log.error(exception)
        return utils.simple_json_response(500, 'Database insert failed')
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(403,
            "Not authorized to perform this action or access this resource")
    return utils.send_id_return(201,random_group_id)

@user_groups_management.route('/user-groups/<group_id>', 
    methods=['DELETE'], provide_automatic_options=False)
def delete_group(group_id):
    """Handles DELETE requests. Deletes the group with the id defined in the request"""
    # validate the jwt and get the user data
    try:
        user_data = get_user_data_from_header(request.headers)
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(401, 'Token missing or invalid')

    #check if id is valid integer
    try:
        group_id = int(group_id)
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(400,
            'Bad request, missing required parameters or otherwise malformed')
    # get all files in the database
    try:
        query_user_groups.group_delete(group_id, user_data)
    except DatabaseException as exception:
        log.error(exception)
        return utils.simple_json_response(500,
                    'Bad request, missing required parameters or otherwise malformed')
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(403,
            "Not authorized to perform this action or access this resource")

    return utils.simple_json_response(200, 'User group deleted')

@user_groups_management.route('/user-groups/<group_id>/join', 
    methods=['POST'], provide_automatic_options=False)
def join_group(group_id):
    """Handles GET requests. joins the group with the id defined in the request"""
    # validate the jwt and get the user data
    try:
        user_data = get_user_data_from_header(request.headers)
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(401, 'Token missing or invalid')

    # check if user group is not full yet
    try:
        group_id = int(group_id)
        max_user_count = query_user_groups.get_max_user_count(group_id)
        user_count = query_user_groups.get_user_count(group_id)
        if user_count >= max_user_count:
            raise Exception('User group full')
    except DatabaseException as exception:
        log.error(exception)
        return utils.simple_json_response(500,'Database query failed')
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(400,
                    'Bad request, missing required parameters or otherwise malformed')
    # get all files in the database
    try:
        query_user_groups.group_join(group_id, user_data)
    except DatabaseException as exception:
        log.error(exception)
        return utils.simple_json_response(500, 'Database query failed')
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(403,
            "Not authorized to perform this action or access this resource")

    return utils.simple_json_response(200, 'User group joined')

@user_groups_management.route('/user-groups/leave', 
    methods=['POST'], provide_automatic_options=False)
def leave_group():
    """Handles GET requests. leave the group with the id defined in the request"""
    # validate the jwt and get the user data
    try:
        user_data = get_user_data_from_header(request.headers)
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(401, 'Token missing or invalid')

    # get all files in the database
    try:
        query_user_groups.group_leave(user_data.id)
    except DatabaseException as exception:
        log.error(exception)
        return utils.simple_json_response(500, 'Database query failed')
    return utils.simple_json_response(200, 'User group left')

@user_groups_management.route('/user-groups/<group_id>', 
    methods=['PUT'], provide_automatic_options=False)
def update_group(group_id):
    """ Handles PUT requests. updates the user group with 
        the id and values defined in the request                """
    # validate the jwt and get the user data
    try:
        user_data = get_user_data_from_header(request.headers)
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(401, 'Token missing or invalid')

    # handle the auth levels at the start of the request
    if user_data.auth_level < 2:
        log.error("Not authorized to perform this action or access this resource")
        return utils.simple_json_response(403,
            "Not authorized to perform this action or access this resource")

    #check if values are valid
    try:
        request_data = request.get_json(force=True, silent=True)
        if request_data is None:
            raise Exception('No request data')
        params = {}
        keys = list(request_data.keys())
        for key in keys:
            if key in ['class_id', 'name', 'max_user_count']:
                params[key] = request_data[key]
        for key in params.keys():
            if key == 'max_user_count':
                params['max_user_count'] = int(request_data['max_user_count'])
            elif key == 'class_id':
                params['class_id'] = int(request_data['class_id'])
            elif key == 'name':
                params['name'] = utils.validate_user_text_input(request_data['name'])
        group_id = int(group_id)
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(400,
                    'Bad request, missing required parameters or otherwise malformed')
    # update the group information
    try:
        query_user_groups.group_update(group_id, params)
    except DatabaseException as exception:
        log.error(exception)
        return utils.simple_json_response(500, 'Database query failed')
    return utils.simple_json_response(200, 'User group updated')

@user_groups_management.route('/user-groups/kick', 
    methods=['POST'], provide_automatic_options=False)
def kick_from_group():
    """Handles GET requests. leave the group with the id defined in the request"""
    
    # validate the jwt and get the user data
    try:
        user_data = get_user_data_from_header(request.headers)
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(401, 'Token missing or invalid')

    # handle the auth levels at the start of the request
    if user_data.auth_level < 2:
        log.error("Not authorized to perform this action or access this resource")
        return utils.simple_json_response(403,
            "Not authorized to perform this action or access this resource")

    # get all files in the database and check if value are valid
    try:
        key = list(request.args.keys())[0]
        value = list(request.args.values())[0]
        if key not in ['user_id']:
            raise Exception("key not allowed")
        value = int(value)
        query_user_groups.kick_user(value)
    except DatabaseException as exception:
        log.error(exception)
        return utils.simple_json_response(500, 'Database query failed')
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(400,
            'Bad request, missing required parameters or otherwise malformed')
    return utils.simple_json_response(200, 'User kicked from user-group')

def get_user_data_from_header(headers):
    """ gets the user data from the header """
    jwt = utils.get_JWT(headers)
    user_data = query_user_groups.get_user_data(jwt)
    return user_data

#
# Internal API endpoints
#

@user_groups_management.route('/internal/user-groups', methods=['GET'])
def internal_get_user_group_ids():
    """Internal API endpoint to get all user group ids from a specific class"""
    class_id = request.args.get('classID')
    if class_id is None:
        return "Bad request", 400

    try:
        groups = query_user_groups.get_all_group_ids(class_id)
    except DatabaseException as exception:
        log.error(exception)
        return "Database query failed", 500
    return jsonify(groups), 200

@user_groups_management.route('/internal/user-groups/<group_id>', methods=['GET'])
def get_group_members(group_id):
    """Internal API endpoint to get all members from a user group"""

    try:
        group_id = int(group_id)
    except ValueError:
        return "Bad request", 400

    members = query_user_groups.get_users_per_group(group_id)

    # only return int ids
    response = []
    for member in members:
        response.append(member['user_id'])

    return jsonify(response), 200

@user_groups_management.route('/internal/user-groups/<group_id>/join', methods=['POST'])
def internal_join_group(group_id):
    """Internal API endpoint to join a user group"""
    try:
        max_user_count = query_user_groups.get_max_user_count(group_id)
        user_count = query_user_groups.get_user_count(group_id)
    except Exception as exception:
        log.error(exception)
        return "Internal server error", 500
    if user_count >= max_user_count:
        return "User group full", 400

    try:
        request_data = request.get_json(force=True, silent=True)
        user_id = request_data['user_id']
    except (KeyError, TypeError):
        return "Bad request", 400

    try:
        query_user_groups.internal_group_join(group_id, user_id)
    except DatabaseException as exception:
        log.error(exception)
        return "Database query failed", 500

    return '', 200

@user_groups_management.route('/internal/user-groups/leave', methods=['POST'])
def internal_leave_group():
    """Internal API endpoint to leave a user group"""
    try:
        user_id = request.json['user_id']
    except (KeyError, TypeError):
        return "Bad request", 400

    try:
        query_user_groups.group_leave(user_id)
    except DatabaseException as exception:
        log.error(exception)
        return "Database query failed", 500

    return '', 200

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
