"""Config for itests"""

# host
host = 'http://group_management:5000'

#get jwt
token_password_valid = {
    'grant_type': 'password',
    'email': 'admin@uce.nl',
    'password': '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8'
}
jwt = ""

#standard error msg
no_token = 'Token missing or invalid'
not_authorized = 'Not authorized to perform this action or access this resource'
invalid_request = 'Bad request, missing required parameters or otherwise malformed'

#test courses
test_course_id = 1000000
test_course_object_good = {"name": "iteration_test_class", "university_id": 200000}
test_course_object_update = {"name": "changed_class_name", "university_id": 100000}
#test class_id
test_class_id = 20000
test_class_object_good = {"name": "iteration_test_class", "course_id": 2000000}
test_class_object_update = {"name": "changed_class_name", "course_id": 1000000}
# test ID_User_Group
test_id_user_group = 2000
# test UniversityID
test_university_id = 100000
# all UserGroupID's
allUserGroupIDs = [1000,2000,3000,4000]
#test object for creating user
test_object_good = {"name": "iteration_test_group", "class_id": 20000, "max_user_count": 6}
test_object_wrong = {"name": "iteration_test_group", "class_id": "1232asd", "max_user_count": 6}
test_Object_Non_Existing_ClassID = {"name": "testgroup", "class_id": 1, "max_user_count": 6}
test_update_object = {"name": "testgroeppie2.0"}
new_id = ""

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
