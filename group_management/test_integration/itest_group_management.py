""""Test module that tests connections to the download service"""

import unittest
import requests
from time import sleep

from . import config
from src import query_user_groups

class TestGroup_management(unittest.TestCase):
    """Test dynamic content"""
    @classmethod
    def setUpClass(cls):
        """Wait 60 seconds to allow for setup"""
        print("sleeping for 60 sec...")
        sleep(60)
        res = requests.post('http://auth:5000/auth/token', json=config.token_password_valid)
        config.jwt = "Bearer " + res.json()['access_token']
        print(f"access token: {config.jwt}")

    def test_1_auth_no_JWT_GET(self):
        """Test whether a GET request with no JWT yields a 401"""
        r = requests.get(f'{config.host}/user-groups/')
        self.assertIn(config.no_token, r.json().values())
        self.assertEqual(r.status_code, 401)

    def test_2_wrong_id_GET(self):
        """Test whether a GET request with wrong user_group_id yields a 400"""
        headers = {'authorization': config.jwt}
        r = requests.get(f'{config.host}/user-groups/', params={'university_id':'wrongid'}, headers=headers)
        #self.assertIn('Bad request, missing required parameters or otherwise malformed', r.json().values())
        self.assertEqual(r.status_code, 400)

    def test_3_good_id_GET(self):
        """Test whether a GET request with good user_group_id yields a 200"""
        headers = {'authorization': config.jwt}
        r = requests.get(f'{config.host}/user-groups/{config.test_id_user_group}', headers=headers)
        self.assertEqual(r.status_code, 200)

    def test_4_no_id_GET(self):
        """Test whether a GET request with no user_group_id yields a 200 and returns all group ids"""
        headers = {'authorization': config.jwt}
        r = requests.get(f'{config.host}/user-groups/', params={'university_id': config.test_university_id}, headers=headers)
        for item in r.json():
            self.assertIn(item['user_group_id'], config.allUserGroupIDs)
        self.assertEqual(r.status_code, 200)

    def test_5_auth_no_JWT_POST(self):
        """Test whether a POST request with no JWT yields a 400"""
        r = requests.post(f'{config.host}/user-groups/', json=config.test_object_good)

        self.assertIn(config.no_token, r.json().values())
        self.assertEqual(r.status_code, 401)

    def test_6_nonexisting_class_id_POST(self):
        """Test whether a POST request with a non existing class id gets rejected with code 500"""
        headers = {'authorization': config.jwt}
        r = requests.post(f'{config.host}/user-groups/', headers=headers,json=config.test_Object_Non_Existing_ClassID)
        #self.assertIn('Database insert failed', r.json().values())
        self.assertEqual(r.status_code, 500)

    def test_7_add_join_leave_update_delete_user_group_POST(self):
        """Test whether a POST request with new values gets added as new user group and yields code 200"""
        headers = {'authorization': config.jwt}
        r_create = requests.post(f'http://group_management:5000/user-groups/', headers=headers, json=config.test_object_good)
        json = r_create.json()
        new_id = json['id']

        headers = {'authorization': config.jwt}
        #iform some reason the user is already in a group so i need to remove that first before joining a new one.
        r_leave = requests.post(f'{config.host}/user-groups/leave', headers=headers)

        r_join = requests.post(f'{config.host}/user-groups/{new_id}/join', headers=headers)

        r_leave = requests.post(f'{config.host}/user-groups/leave', headers=headers)
        """Test whether a DELETE request with the new user group id we build in the test before this yields code 200"""
        headers = {'authorization': config.jwt}

        r_update = requests.put(f'{config.host}/user-groups/{new_id}', headers=headers, json=config.test_update_object)

        r_delete = requests.delete(f'{config.host}/user-groups/{new_id}', headers=headers)

        self.assertEqual(r_create.status_code, 201)
        self.assertEqual(r_join.status_code, 200)
        self.assertEqual(r_leave.status_code, 200)
        self.assertEqual(r_delete.status_code, 200)
        self.assertEqual(r_update.status_code, 200)

    def test_8_invalid_leave_user_group_GET(self):
        """Test whether a LEAVE request with the new user group id yields code 200"""
        headers = {'authorization': config.jwt}
        r = requests.post(f'{config.host}/user-groups/leave', headers=headers)
        self.assertEqual(r.status_code, 200)

    def test_9_invalid_join_user_group_GET(self):
        """Test whether a JOIN request with the new user group id yields code 400"""
        headers = {'authorization': config.jwt}
        r = requests.post(f'{config.host}/user-groups/000000000/join', headers=headers)
        self.assertEqual(r.status_code, 400)

    def test_10_invalid_update_user_group_PUT(self):
        """Test whether a PUT request with the new user group information but wrong classID yields code 200"""
        headers = {'authorization': config.jwt}
        r = requests.put(f'{config.host}/user-groups/000000000', headers=headers, json=config.test_update_object)
        self.assertEqual(r.status_code, 200)

    def test_11_invalid_update_user_group_PUT(self):
        """Test whether a PUT request with no user group information yields code 400"""
        headers = {'authorization': config.jwt}
        r = requests.put(f'{config.host}/user-groups/000000000', headers=headers)
        self.assertEqual(r.status_code, 400)

    def test_12_delete_non_existing_user_group_DELETE(self):
        """Test whether a DELETE request with non existing user group id yields code 500"""
        headers = {'authorization': config.jwt}
        r = requests.delete(f'{config.host}/user-groups/0000000', headers=headers)
        self.assertEqual(r.status_code, 500)

    def test_13_get_class_wrong_id_GET(self):
        """Test whether a GET request with wrong id yields a 400"""
        headers = {'authorization': config.jwt}
        r = requests.get(f'{config.host}/classes/', params={'course_id':'wrongid'}, headers=headers)
        self.assertIn(config.invalid_request, r.json().values())
        self.assertEqual(r.status_code, 400)

    def test_14_get_class_correct_course_id_GET(self):
        """Test whether a GET request with correct course_id yields a 200"""
        headers = {'authorization': config.jwt}
        r = requests.get(f'{config.host}/classes/', params={'university_id':config.test_university_id}, headers=headers)
        self.assertEqual(r.status_code, 200)

    def test_15_get_class_wrong_key_GET(self):
        """Test whether a GET request with wrong key yields a 400"""
        headers = {'authorization': config.jwt}
        r = requests.get(f'{config.host}/classes/', params={'wrong_key':config.test_university_id}, headers=headers)
        self.assertEqual(r.status_code, 400)

    def test_16_get_classes_correct_class_id_GET(self):
        """Test whether a GET request with good class_id yields a 400"""
        headers = {'authorization': config.jwt}
        r = requests.get(f'{config.host}/classes/{config.test_class_id}', headers=headers)
        self.assertEqual(r.status_code, 200)

    def test_17_get_classes_wrong_class_id_GET(self):
        """Test whether a GET request with good class_id yields a 200"""
        headers = {'authorization': config.jwt}
        r = requests.get(f'{config.host}/classes/00000000', headers=headers)
        self.assertEqual(r.status_code, 200)

    def test_18_create_update_delete_class_POST_UPDATE_DELETE(self):
        """Test whether POST, UPDATE and DELETE requests yield code 200"""
        headers = {'authorization': config.jwt}
        r_create = requests.post(f'{config.host}/classes/', headers=headers, json=config.test_class_object_good)

        json = r_create.json()
        new_id = json['id']

        headers = {'authorization': config.jwt}
        #iform some reason the user is already in a group so i need to remove that first before joining a new one.

        r_update = requests.put(f'{config.host}/classes/{new_id}', headers=headers, json=config.test_class_object_update)

        r_delete = requests.delete(f'{config.host}/classes/{new_id}', headers=headers)

        self.assertEqual(r_create.status_code, 201)
        self.assertEqual(r_delete.status_code, 200)
        self.assertEqual(r_update.status_code, 200)

    def test_19_delete_wrong_class_id_DELETE(self):
        """Test whether a DELETE request with wrong class_id yields a 400"""
        headers = {'authorization': config.jwt}
        r = requests.delete(f'{config.host}/classes/00000000', headers=headers)
        self.assertIn(config.invalid_request, r.json().values())
        self.assertEqual(r.status_code, 400)

    def test_20_delete_not_empty_class_DELETE(self):
        """Test whether a DELETE request with wrong class_id yields a 400"""
        headers = {'authorization': config.jwt}
        r = requests.delete(f'{config.host}/classes/{config.test_class_id}', headers=headers)
        self.assertIn(config.invalid_request, r.json().values())
        self.assertEqual(r.status_code, 400)

    def test_21_update_wrong_class_id_PUT(self):
        """Test whether a PUT request with wrong class_id yields a 400"""
        headers = {'authorization': config.jwt}
        r = requests.delete(f'{config.host}/classes/00000000', headers=headers, json=config.test_class_object_update)
        self.assertEqual(r.status_code, 400)

    def test_22_update_no_body_PUT(self):
        """Test whether a PUT request with no update information yields a 400"""
        headers = {'authorization': config.jwt}
        r = requests.delete(f'{config.host}/classes/00000000', headers=headers)
        self.assertIn(config.invalid_request, r.json().values())
        self.assertEqual(r.status_code, 400)

    def test_23_get_courses_correct_GET(self):
        """Test whether a GET request with correct unviversity_id yields a 200"""
        headers = {'authorization': config.jwt}
        r = requests.get(f'{config.host}/courses/', params={'university_id':config.test_university_id}, headers=headers)
        self.assertEqual(r.status_code, 200)

    def test_24_get_courses_wrong_id_GET(self):
        """Test whether a GET request with wrong unviversity_id yields a 200"""
        headers = {'authorization': config.jwt}
        r = requests.get(f'{config.host}/courses/', params={'university_id':000000000}, headers=headers)
        self.assertEqual(r.status_code, 200)

    def test_25_get_courses_wrong_key_GET(self):
        """Test whether a GET request with wrong key yields a 400"""
        headers = {'authorization': config.jwt}
        r = requests.get(f'{config.host}/courses/', params={'wrong_key':config.test_university_id}, headers=headers)
        self.assertIn(config.invalid_request, r.json().values())
        self.assertEqual(r.status_code, 400)

    def test_26_get_specific_course_correct_GET(self):
        """Test whether a GET request with correct course_id yields a 200"""
        headers = {'authorization': config.jwt}
        r = requests.get(f'{config.host}/courses/{config.test_course_id}', headers=headers)
        self.assertEqual(r.status_code, 200)

    def test_27_get_specific_course_wrong_id_GET(self):
        """Test whether a GET request with wrong course_id yields a 200"""
        headers = {'authorization': config.jwt}
        r = requests.get(f'{config.host}/courses/0000000', headers=headers)
        self.assertEqual(r.status_code, 200)

    def test_28_delete_not_empty_course_DELETE(self):
        """Test whether a DELETE request with course_id that contains classes yields a 400"""
        headers = {'authorization': config.jwt}
        r = requests.delete(f'{config.host}/courses/{config.test_course_id}', headers=headers)
        self.assertIn('Course not empty', r.json().values())
        self.assertEqual(r.status_code, 400)

    def test_28_delete_non_existing_course_DELETE(self):
        """Test whether a DELETE request with non existing course_id yields a 400"""
        headers = {'authorization': config.jwt}
        r = requests.delete(f'{config.host}/courses/0000000000', headers=headers)
        self.assertIn('Course does not exist', r.json().values())
        self.assertEqual(r.status_code, 400)

    def test_29_create_update_delete_course_POST_UPDATE_DELETE(self):
        """Test whether POST, UPDATE and DELETE requests yield code 200"""
        headers = {'authorization': config.jwt}
        #create course
        r_create = requests.post(f'{config.host}/courses/', headers=headers, json=config.test_course_object_good)
        json = r_create.json()
        print(json)
        new_id = json['id']
        #update course
        r_update = requests.put(f'{config.host}/courses/{new_id}', headers=headers, json=config.test_course_object_update)
        #delete course
        r_delete = requests.delete(f'{config.host}/courses/{new_id}', headers=headers)

        self.assertEqual(r_create.status_code, 201)
        self.assertEqual(r_delete.status_code, 200)
        self.assertEqual(r_update.status_code, 200)

    def test_30_update_non_existing_course_UPDATE(self):
        """Test whether a UPDATE request with non existing course_id yields a 400"""
        headers = {'authorization': config.jwt}
        r = requests.delete(f'{config.host}/courses/0000000000', headers=headers)
        self.assertIn('Course does not exist', r.json().values())
        self.assertEqual(r.status_code, 400)

    def test_31_update_with_out_body_course_UPDATE(self):
        """Test whether a UPDATE request with no body yields a 400"""
        headers = {'authorization': config.jwt}
        r = requests.delete(f'{config.host}/courses/{config.test_course_id}', headers=headers)
        self.assertEqual(r.status_code, 400)

    def test_32_auth_no_JWT_classes_GET(self):
        """Test whether a GET request with no JWT yields a 401"""
        r = requests.get(f'{config.host}/classes/')
        self.assertIn(config.no_token, r.json().values())
        self.assertEqual(r.status_code, 401)

    def test_33_auth_no_JWT_classes_POST(self):
        """Test whether a POST request with no JWT yields a 401"""
        r = requests.post(f'{config.host}/classes/')
        self.assertIn(config.no_token, r.json().values())
        self.assertEqual(r.status_code, 401)

    def test_34_auth_no_JWT_classes_GET(self):
        """Test whether a GET request with no JWT yields a 401"""
        r = requests.get(f'{config.host}/classes/0000000')
        self.assertIn(config.no_token, r.json().values())
        self.assertEqual(r.status_code, 401)

    def test_35_auth_no_JWT_classes_PUT(self):
        """Test whether a PUT request with no JWT yields a 401"""
        r = requests.put(f'{config.host}/classes/00000')
        self.assertIn('Token missing or invalid', r.json().values())
        self.assertEqual(r.status_code, 401)

    def test_36_auth_no_JWT_classes_DELETE(self):
        """Test whether a DELETE request with no JWT yields a 401"""
        r = requests.delete(f'{config.host}/classes/00000')
        self.assertIn(config.no_token, r.json().values())
        self.assertEqual(r.status_code, 401)

    def test_37_auth_no_JWT_courses_GET(self):
        """Test whether a GET request with no JWT yields a 401"""
        r = requests.get(f'{config.host}/courses/')
        self.assertIn(config.no_token, r.json().values())
        self.assertEqual(r.status_code, 401)

    def test_38_auth_no_JWT_courses_POST(self):
        """Test whether a POST request with no JWT yields a 401"""
        r = requests.post(f'{config.host}/courses/')
        self.assertIn(config.no_token, r.json().values())
        self.assertEqual(r.status_code, 401)

    def test_39_auth_no_JWT_courses_GET(self):
        """Test whether a GET request with no JWT yields a 401"""
        r = requests.get(f'{config.host}/courses/00000000')
        self.assertIn(config.no_token, r.json().values())
        self.assertEqual(r.status_code, 401)

    def test_39_auth_no_JWT_courses_DELETE(self):
        """Test whether a DELETE request with no JWT yields a 401"""
        r = requests.delete(f'{config.host}/courses/00000000')
        self.assertIn(config.no_token, r.json().values())
        self.assertEqual(r.status_code, 401)

    def test_40_auth_no_JWT_courses_PUT(self):
        """Test whether a PUT request with no JWT yields a 401"""
        r = requests.put(f'{config.host}/courses/00000000')
        self.assertIn(config.no_token, r.json().values())
        self.assertEqual(r.status_code, 401)

    def test_41_universities_GET(self):
        """Test whether a GET request with no JWT yields a 501"""
        r = requests.get(f'{config.host}/universities/')
        self.assertIn('This request is not implemented yet.', r.json().values())
        self.assertEqual(r.status_code, 501)

    def test_42_universities_POST(self):
        """Test whether a POST request with no JWT yields a 501"""
        r = requests.post(f'{config.host}/universities/')
        self.assertIn('This request is not implemented yet.', r.json().values())
        self.assertEqual(r.status_code, 501)

    def test_43_universities_GET(self):
        """Test whether a GET request with no JWT yields a 501"""
        r = requests.get(f'{config.host}/universities/0000000')
        self.assertIn('This request is not implemented yet.', r.json().values())
        self.assertEqual(r.status_code, 501)

    def test_44_universities_PUT(self):
        """Test whether a PUT request with no JWT yields a 501"""
        r = requests.put(f'{config.host}/universities/000000')
        self.assertIn('This request is not implemented yet.', r.json().values())
        self.assertEqual(r.status_code, 501)

    def test_45_universities_DELETE(self):
        """Test whether a DELETE request with no JWT yields a 501"""
        r = requests.delete(f'{config.host}/universities/000000')
        self.assertIn('This request is not implemented yet.', r.json().values())
        self.assertEqual(r.status_code, 501)

    def test_46_wrong_key_user_groups_GET(self):
        """Test whether a GET request with wrong parameter key yields a 400"""
        headers = {'authorization': config.jwt}
        r = requests.get(f'{config.host}/user-groups/', params={'wrong_key':'000000'}, headers=headers)
        self.assertEqual(r.status_code, 400)

    def test_47_auth_no_JWT_user_groups_GET(self):
        """Test whether a GET request with no JWT yields a 401"""
        r = requests.get(f'{config.host}/user-groups/00000000')
        self.assertIn(config.no_token, r.json().values())
        self.assertEqual(r.status_code, 401)

    def test_48_non_valid_id_user_groups_GET(self):
        """Test whether a GET request with wrong parameter key yields a 400"""
        headers = {'authorization': config.jwt}
        r = requests.get(f'{config.host}/user-groups/123wrong12', headers=headers)
        self.assertEqual(r.status_code, 400)

    def test_49_wrong_id_user_groups_GET(self):
        """Test whether a GET request with good user_group_id yields a 200"""
        headers = {'authorization': config.jwt}
        r = requests.get(f'{config.host}/user-groups/000000', headers=headers)
        self.assertEqual(r.status_code, 200)

    def test_50_no_req_body_user_groups_POST(self):
        """Test whether a POST request with no request body yields a 200"""
        headers = {'authorization': config.jwt}
        r = requests.post(f'{config.host}/user-groups/', headers=headers)
        self.assertEqual(r.status_code, 400)

    def test_51_wrong_req_body_user_groups_POST(self):
        """Test whether a POST request with no request body yields a 200"""
        headers = {'authorization': config.jwt}
        r = requests.post(f'{config.host}/user-groups/', headers=headers, json=config.test_object_wrong)
        self.assertEqual(r.status_code, 400)

    def test_52_auth_no_JWT_user_groups_DELETE(self):
        """Test whether a DELETE request with no JWT yields a 401"""
        r = requests.delete(f'{config.host}/user-groups/00000000')
        self.assertIn(config.no_token, r.json().values())
        self.assertEqual(r.status_code, 401)

    def test_53_non_valid_id_user_groups_DELETE(self):
        """Test whether a DELETE request with a non valid id yields a 401"""
        headers = {'authorization': config.jwt}
        r = requests.delete(f'{config.host}/user-groups/0000asdasd0000', headers=headers)
        self.assertEqual(r.status_code, 400)

    def test_54_auth_no_JWT_user_groups_join_POST(self):
        """Test whether a POST request with no JWT yields a 401"""
        r = requests.post(f'{config.host}/user-groups/00000000/join')
        self.assertIn(config.no_token, r.json().values())
        self.assertEqual(r.status_code, 401)

    def test_55_auth_no_JWT_user_groups_leave_GET(self):
        """Test whether a GET request with no JWT yields a 401"""
        r = requests.delete(f'{config.host}/user-groups/leave')
        self.assertIn(config.no_token, r.json().values())
        self.assertEqual(r.status_code, 401)

    def test_56_auth_no_JWT_user_groups_PUT(self):
        """Test whether a PUT request with no JWT yields a 401"""
        r = requests.put(f'{config.host}/user-groups/00000000')
        self.assertIn(config.no_token, r.json().values())
        self.assertEqual(r.status_code, 401)

    def test_57_internal_get_classes(self):
        """Test internal get classes endpoint"""
        r = requests.get(f'{config.host}/internal/classes')
        self.assertEqual(r.status_code, 200)
        self.assertEqual(r.json(), [10000, 20000])

    def test_58_internal_get_user_groups(self):
        """Test internal get user groups endpoint"""
        r = requests.get(f'{config.host}/internal/user-groups', params={'classID': 10000})
        self.assertEqual(r.status_code, 200)
        self.assertEqual(r.json(), [1000, 2000])

    def test_59_internal_get_user_group_members(self):
        """Test internal get user group members endpoint"""
        r = requests.get(f'{config.host}/internal/user-groups/1000')
        self.assertEqual(r.status_code, 200)
        self.assertIn(1, r.json())

    def test_60_internal_join_user_group(self):
        """Test internal join user group endpoint"""
        r = requests.post(f'{config.host}/internal/user-groups/1000/join', json={'user_id':0})
        
        self.assertEqual(r.status_code, 200)

        r = requests.get(f'{config.host}/internal/user-groups/1000')
        self.assertIn(0, r.json())

    def test_61_internal_leave_user_group(self):
        """Test internal leave user group endpoint"""
        r = requests.post(f'{config.host}/internal/user-groups/leave', json={'user_id':0})
        self.assertEqual(r.status_code, 200)

        r = requests.get(f'{config.host}/internal/user-groups/1000')
        self.assertNotIn(0, r.json())

    def test_62_get_courses_invalid_id_GET(self):
        """Test whether a GET request with invalid id yields a 400"""
        headers = {'authorization': config.jwt}
        r = requests.get(f'{config.host}/courses/', params={'university_id':"0000wrong0000"}, headers=headers)
        self.assertEqual(r.status_code, 400)

    def test_63_kick_user_group_POST(self):
        """Test whether a POST correct kick request yields a 200"""
        headers = {'authorization': config.jwt}
        r = requests.post(f'{config.host}/user-groups/kick', params={'user_id': 2}, headers=headers)
        self.assertEqual(r.status_code, 200)
    
    def test_64_kick_user_group_POST(self):
        """Test whether a POST request with invalid id yields a 400"""
        headers = {'authorization': config.jwt}
        r = requests.post(f'{config.host}/user-groups/kick', params={'user_id': "12312wrong3"}, headers=headers)
        self.assertEqual(r.status_code, 400)
    
    def test_65_kick_user_group_POST(self):
        """Test whether a POST request with no user id yields a 400"""
        headers = {'authorization': config.jwt}
        r = requests.post(f'{config.host}/user-groups/kick', headers=headers)
        self.assertEqual(r.status_code, 400)

if __name__ == '__main__':
    unittest.main()

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
