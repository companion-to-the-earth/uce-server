FROM registry.gitlab.com/companion-to-the-earth/wsgi-server/wsgi-server:1.0.3-prod

#WORKDIR=/server/framework/app (set in wsgi-server image)

ARG STAGE
ARG CD_STAGE_TEST
COPY requirements.prod.txt requirements.test.txt ./
ENV PYTHONUNBUFFERED=1

# add wait script so we can wait for dependencies to be available
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.8.0/wait /wait
RUN chmod +x /wait

#install test dependencies if necessary
RUN [ "$STAGE" = "$CD_STAGE_TEST" ] && pip install -r requirements.test.txt || true

RUN apk add --virtual build-deps bash git openssh \
    && pip install -r requirements.prod.txt \
    && apk del build-deps

#remove unnecessary dependencies that come with base image
RUN python3 -m pip uninstall -y pip setuptools wheel

# add app source
COPY __init__.py __init__.py
COPY src src
COPY test_unit test_unit

# set the workdir to app to help ci/cd
WORKDIR /server/framework

# uncomment to use debug server
#prepend coverage with exec, since the python scripts do not listen to STOPSIGNAL=SIGTERM
#https://stackoverflow.com/questions/48692363/coverage-run-app-py-inside-a-docker-container-not-creating-coverage-file
CMD if [ "$STAGE" = "$CD_STAGE_TEST" ]; then \
    echo "test stage" \
    && /wait \
    && export COVERAGE_FILE=/.coverage \
    && exec coverage run --branch /server/wsgi/app/src/server.py; \
    else \
    echo "prod stage" \
    && /wait \
    && exec python /server/wsgi/app/src/server.py; \
    fi
