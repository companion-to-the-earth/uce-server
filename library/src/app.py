"""
Initialiser for the library Flask app
"""

from os import getpid
from threading import get_ident

from flask import Flask
from pika import ConnectionParameters

from src.log import log
from src.books import books
from src.book_sections import book_sections
from src.book_contents import book_contents
from src.middleware import LibraryMiddleware

def get_app():
    """
    Initialises the library Flask app.

    :returns Flask: the library Flask app
    """

    # initialise the app
    app = Flask(__name__)
    app.register_blueprint(books)
    app.register_blueprint(book_sections)
    app.register_blueprint(book_contents)
    # initialise the database
    @app.before_first_request
    def init_middleware():
        # print the PID
        pid = getpid()
        log.info(f'Starting Flask app on process {pid}')

        # add the middleware to the app config
        params = ConnectionParameters(host='middleware')
        middleware = LibraryMiddleware(params, pid)
        app.config['MW'] = middleware

    @app.before_request
    def print_tid():
        # print the TID
        tid = get_ident()
        log.info(f'Handling request on thread {tid}')

    return app

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
