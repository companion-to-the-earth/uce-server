"""
Book content blueprint;
"""
import json
import glob
import os

from flask import Blueprint, request, send_from_directory
from src.log import log
from src import query
from src import utils
from src.middleware import DatabaseException

book_contents = Blueprint('book_contents', __name__)

FILE_FOLDER = "/var/www/library/content/"
ALLOWED_EXTENSIONS = ['image/png', 'image/jpg', 'image/jpeg', 'video/mp4','text/plain']

@book_contents.route('/books/sections/<book_section_id>/contents', methods=['POST'], provide_automatic_options=False)
def create_contents(book_section_id):
    """handles POST requests. Creates content"""
    # validate the jwt and get the user data
    try:
        user_data = get_user_data_from_header(request.headers)
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(401, 'Token missing or invalid')

    queryhandler = query.Query()
    #check file
    try:
        file = request.files['file']
        if not file or not utils.allowed_file(file):
            raise Exception('file is missing or file type not allowed')

        request_data_raw = request.form['book_content']
        request_data = json.loads(request_data_raw)
        if request_data['content_type'] not in ['image', 'video', 'quiz', 'text']:
            raise Exception('content type not allowed')
        #if not a number it will break
        request_data['content_nr'] = int(request_data['content_nr'])
        book_section_id = int(book_section_id)
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(400,
                    'Bad request, missing required parameters or otherwise malformed')
    #get a new random id
    random_book_content_id = utils.generate_random_ID()
    while not queryhandler.check_key_validity('book_content', 'book_content_id', random_book_content_id):
        random_book_content_id = utils.generate_random_ID()

    request_data['book_content_id'] = random_book_content_id
    request_data['book_section_id'] = book_section_id

    try:
        queryhandler.create_book_content(request_data, user_data)
    except DatabaseException as exception:
        log.error(exception)
        return utils.simple_json_response(500, 'Database insert failed')
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(400,
                    'Bad request, missing required parameters or otherwise malformed')
    #save file with id as name
    filename = str(random_book_content_id) + "." + utils.file_extension(file.content_type)
    file.save(os.path.join(FILE_FOLDER, filename))

    return utils.send_id_return(201, random_book_content_id)

@book_contents.route('/books/sections/contents/<book_content_id>', methods=['GET'], provide_automatic_options=False)
def get_content(book_content_id):
    """handles GET requests. Get content and download it"""
    # validate the jwt and get the user data
    try:
        user_data = get_user_data_from_header(request.headers)
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(401, 'Token missing or invalid')

    queryhandler = query.Query()
    try:
        book_content_id = int(book_content_id)
        queryhandler.get_book_content(book_content_id)
    except DatabaseException as exception:
        log.error(exception)
        return utils.simple_json_response(500, 'Database insert failed')
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(400,
            'Bad request, missing required parameters or otherwise malformed')
    #search file name
    os.chdir(FILE_FOLDER)
    temp = str(book_content_id) + "." + "*"
    filename = ''
    for file in glob.glob(temp):
        filename = file
    return send_from_directory(FILE_FOLDER, filename)

@book_contents.route('/books/sections/contents/<book_content_id>', methods=['PUT'], provide_automatic_options=False)
def update_content(book_content_id):
    """handles PUT requests. Update content in db and uploads the new file"""
    # validate the jwt and get the user data
    try:
        user_data = get_user_data_from_header(request.headers)
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(401, 'Token missing or invalid')

    queryhandler = query.Query()
    # check if the requested id is valid
    try:
        book_content_id = int(book_content_id)
    except (ValueError, KeyError, TypeError):
        return utils.simple_json_response(400,
            'Bad request, missing required parameters or otherwise malformed')
    params = {}
    file = ''
    try:
        # check the parameters if supplied
        if 'book_content' in request.form.keys():
            request_data_raw = request.form['book_content']
            request_data = json.loads(request_data_raw)
            # get the parameters that are supplied
            keys = list(request_data.keys())
            for key in keys:
                if key in ['content_type', 'content_nr', 'book_section_id']:
                    params[key] = request_data[key]
            # check if the request params have the correct type
            # maybe an inefficient way to check the types but it works for now
            for key in params.keys():
                if(key == 'content_type'):
                    params['content_type'] = request_data['content_type']
                    if params['content_type'] not in ['image', 'video', 'quiz', 'text']:
                        raise Exception('Content type not a valid value')
                if(key == 'content_nr'):
                    params['content_nr'] = int(request_data['content_nr'])
                if(key == 'book_section_id'):
                    params['book_section_id'] = int(request_data['book_section_id'])
        # check if a file is supplied
        if 'file' in request.files.keys():
            file = request.files['file']
        #check if the request contains any of the valid parameters or a file
        if not params and not file:
            raise Exception('no request data supplied')
        if file:
            if not utils.allowed_file(file):
                raise Exception('file name not allowed')
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(400,
                    'Bad request, missing required parameters or otherwise malformed')
    #if form is defined in request
    if params:
        try:
            queryhandler.update_book_content(params, book_content_id, user_data)
        except DatabaseException as exception:
            log.error(exception)
            return utils.simple_json_response(500, 'Database insert failed')
        except Exception as exception:
            log.error(exception)
            return utils.simple_json_response(400,
                        'Bad request, missing required parameters or otherwise malformed')

    # only save the file if it is supplied
    if file:
        # first remove the old file
        os.chdir(FILE_FOLDER)
        temp = str(book_content_id) + '.' + '*'
        filename = ''
        for f in glob.glob(temp):
            filename = f
        os.remove(filename)

        file_extension = utils.file_extension(file.content_type)
        file.filename = str(book_content_id)
        file.save(os.path.join(FILE_FOLDER, file.filename + '.' + file_extension))

    return utils.simple_json_response(200, 'Book content updated!')

@book_contents.route('/books/sections/contents/<book_content_id>', methods=['DELETE'], provide_automatic_options=False)
def delete_content(book_content_id):
    """handles DELETE requests. Deletes content"""
    # validate the jwt and get the user data
    try:
        user_data = get_user_data_from_header(request.headers)
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(401, 'Token missing or invalid')

    # content id from string to int.
    try:
        book_content_id = int(book_content_id)
    except (ValueError, KeyError, TypeError):
        return utils.simple_json_response(400,
                'Bad request, missing required parameters or otherwise malformed')
    #delete database entry
    try:
        queryhandler = query.Query()
        queryhandler.delete_book_content(book_content_id, user_data)
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(400,
                'Bad request, missing required parameters or otherwise malformed')
    #delete file
    os.chdir(FILE_FOLDER)
    temp = str(book_content_id) + '.' + '*'
    filename = ''
    for f in glob.glob(temp):
        filename = f
    os.remove(filename)
    return utils.simple_json_response(200, 'Book content DELETE')

def get_user_data_from_header(headers):
    """ gets the user data from the header """

    jwt = utils.get_JWT(headers)
    queryhandler = query.Query()
    user_data = queryhandler.get_user_data(jwt)
    return user_data

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
