"""
Book sections blueprint;
"""

from flask import Blueprint, request
from src.log import log
from src import query
from src import utils
from src.middleware import DatabaseException

book_sections = Blueprint('book_sections', __name__)

@book_sections.route('/books/<book_id>/sections', methods=['POST'], provide_automatic_options=False)
def create_sections(book_id):
    """handles POST requests. Creates a section"""
    # validate the jwt and get the user data
    try:
        user_data = get_user_data_from_header(request.headers)
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(401, 'Token missing or invalid')

    request_data = request.get_json(force=True, silent=True)
    if request_data is None:
        return utils.simple_json_response(400,
            'Bad request, missing required parameters or otherwise malformed')

    queryhandler = query.Query()
    #check if no weird values are inputted
    try:
        request_data['book_id'] = int(book_id)
        request_data['section_nr'] = int(request_data['section_nr'])
        request_data['title'] = utils.validate_user_text_input(request_data['title'])
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(400,
            'Bad request, missing required parameters or otherwise malformed')
    # generate a random BookID
    random_book_sections_id = utils.generate_random_ID()
    while not queryhandler.check_key_validity('book_sections', 'book_section_id', random_book_sections_id):
        random_book_sections_id = utils.generate_random_ID()
    #set UserGroupID
    request_data["book_section_id"] = random_book_sections_id

    # get all files in the database
    try:
        queryhandler.book_section_create(request_data, user_data)
    except DatabaseException as exception:
        log.error(exception)
        return utils.simple_json_response(500, 'Database insert failed')
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(400,
            'Bad request, missing required parameters or otherwise malformed')
    return utils.send_id_return(201,random_book_sections_id)

@book_sections.route('/books/sections/<book_section_id>', methods=['PUT'], provide_automatic_options=False)
def update_sections(book_section_id):
    """handles PUT requests. Updates a section"""
    queryhandler = query.Query()
    # validate the jwt and get the user data
    try:
        user_data = get_user_data_from_header(request.headers)
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(401, 'Token missing or invalid')

    request_data = request.get_json(force=True, silent=True)
    if request_data is None:
        return utils.simple_json_response(400,
            'Bad request, missing required parameters or otherwise malformed')
    #validate user input
    try:
        params = {}
        keys = list(request_data.keys())
        book_section_id = int(book_section_id)
        for key in keys:
            if key in ['book_id', 'title', 'section_nr']:
                params[key] = request_data[key]
        for key in params.keys():
            if(key == 'book_id'):
                params['book_id'] = int(request_data['book_id'])
            if(key == 'section_nr'):
                params['section_nr'] = int(request_data['section_nr'])
            if(key == 'title'):
                params['title'] = utils.validate_user_text_input(request_data['title'])
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(400,
                    'Bad request, missing required parameters or otherwise malformed')
    # get all files in the database
    try:
        queryhandler.book_sections_update(book_section_id, params, user_data)
    except DatabaseException as exception:
        log.error(exception)
        return utils.simple_json_response(500, 'Database query failed')
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(400,
            'Bad request, missing required parameters or otherwise malformed')
    return utils.simple_json_response(200, 'Book section updated')

@book_sections.route('/books/sections/<book_section_id>', methods=['DELETE'], provide_automatic_options=False)
def delete_book_section(book_section_id):
    """
    handles DELETE book section requests,
    deletes the book section with the given book_section_id
    """
    # validate the jwt and get the user data
    try:
        user_data = get_user_data_from_header(request.headers)
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(401, 'Token missing or invalid')

    queryhandler = query.Query()
    # get all files in the database
    try:
        book_section_id = int(book_section_id)
        queryhandler.book_section_delete(book_section_id, user_data)
    except DatabaseException as exception:
        log.error(exception)
        return utils.simple_json_response(500, 'Database query failed')
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(400,
            'Bad request, missing required parameters or otherwise malformed')
    return utils.simple_json_response(200, 'Book section deleted')

def get_user_data_from_header(headers):
    """ gets the user data from the header """

    jwt = utils.get_JWT(headers)
    queryhandler = query.Query()
    user_data = queryhandler.get_user_data(jwt)
    return user_data

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
