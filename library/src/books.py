"""
Books blueprint;
"""

from flask import Blueprint, request, jsonify
from src.errors import APIError, BadRequestError, ValueNotFoundError, TokenError, UnauthorizedError
from src.log import log
from src import query
from src import utils
from src.middleware import DatabaseException

books = Blueprint('books', __name__)

@books.route('/books/', methods=['GET'], provide_automatic_options=False)
def query_all_books():
    """
    Handles GET requests.
    returns all books and their information.
    That includes section and content information
    """
    # validate the jwt and get the user data
    try:
        user_data = get_user_data_from_header(request.headers)
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(401, 'Token missing or invalid')
    # get all files in the database
    try:
        queryhandler = query.Query()
        files = queryhandler.get_all_books_query(user_data)
    except DatabaseException as exception:
        log.error(exception)
        return utils.simple_json_response(500, 'Database query failed')

    return jsonify(files), 200

@books.route('/books/', methods=['POST'], provide_automatic_options=False)
def create_book():
    """handles POST requests. Creates a book"""
    # validate the jwt and get the user data
    try:
        user_data = get_user_data_from_header(request.headers)
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(401, 'Token missing or invalid')

    request_data = request.get_json(force=True, silent=True)
    if request_data is None:
        return utils.simple_json_response(400, 'Bad request, missing required parameters or otherwise malformed')
    #validate user input
    try:
        request_data['title'] = utils.validate_user_text_input(request_data['title'])
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(400,
            'Bad request, missing required parameters or otherwise malformed')

    queryhandler = query.Query()
    # generate a random BookID
    random_book_id = utils.generate_random_ID()
    while not queryhandler.check_key_validity('books', 'book_id', random_book_id):
        random_book_id = utils.generate_random_ID()
    #set UserGroupID
    request_data["book_id"] = random_book_id
    request_data["last_edited"] = utils.get_timestamp()
    # get all files in the database
    try:
        queryhandler.book_create(user_data, request_data)
    except DatabaseException as exception:
        log.error(exception)
        return utils.simple_json_response(500, 'Database insert failed')
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(401, 'Token missing or invalid')
    return utils.send_id_return(201,random_book_id)

@books.route('/books/<book_id>', methods=['GET'], provide_automatic_options=False)
def query_single_books(book_id):
    """handles Get book requests. returns all the information about a single book with book_id"""

    # validate the jwt and get the user data
    try:
        user_data = get_user_data_from_header(request.headers)
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(401, 'Token missing or invalid')

    queryhandler = query.Query()
    # get all files in the database
    try:
        book_id = int(book_id)
        files = queryhandler.get_books_query(book_id)
        if files == []:
            return utils.simple_json_response(400,
                'Bad request, missing required parameters or otherwise malformed')
    except DatabaseException as exception:
        log.error(exception)
        return utils.simple_json_response(500, 'Database query failed')
    return jsonify(files[0]), 200

@books.route('/books/<book_id>', methods=['PUT'], provide_automatic_options=False)
def update_book(book_id):
    """handles PUT book requests updates the book id with the given data"""
    queryhandler = query.Query()
    # validate the jwt and get the user data
    try:
        user_data = get_user_data_from_header(request.headers)
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(401, 'Token missing or invalid')

    request_data = request.get_json(force=True, silent=True)
    if request_data is None:
        return utils.simple_json_response(400,
                    'Bad request, missing required parameters or otherwise malformed')
    #validate user input
    try:
        params = {}
        book_id = int(book_id)
        params['title'] = utils.validate_user_text_input(request_data['title'])
        params['last_edited'] = utils.get_timestamp()
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(400,
                    'Bad request, missing required parameters or otherwise malformed')
    # get all files in the database
    try:
        queryhandler.book_update(book_id, params, user_data)
    except DatabaseException as exception:
        log.error(exception)
        return utils.simple_json_response(500, 'Database query failed')
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(400,
                        'Bad request, missing required parameters or otherwise malformed')
    return utils.simple_json_response(200, 'Book updated')

@books.route('/books/<book_id>', methods=['DELETE'], provide_automatic_options=False)
def delete_book(book_id):
    """handles DELETE book requests deletes the book with the given book_id"""
    queryhandler = query.Query()
    # validate the jwt and get the user data
    try:
        user_data = get_user_data_from_header(request.headers)
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(401, 'Token missing or invalid')
    # delete all files in the database
    try:
        book_id = int(book_id)
        queryhandler.book_delete(book_id, user_data)
    except DatabaseException as exception:
        log.error(exception)
        return utils.simple_json_response(500, 'Database query failed')
    except Exception as exception:
        log.error(exception)
        return utils.simple_json_response(400,
                        'Bad request, missing required parameters or otherwise malformed')
    return utils.simple_json_response(200, 'Book deleted')

def get_user_data_from_header(headers):
    """ gets the user data from the header """
    jwt = utils.get_JWT(headers)
    queryhandler = query.Query()
    user_data = queryhandler.get_user_data(jwt)
    return user_data

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
