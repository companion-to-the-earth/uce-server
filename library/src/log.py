"""
Library service logging
"""

import sys
import logging
import colorlog

# Setup logging
log = logging.getLogger('library')
stream_handler = logging.StreamHandler(sys.stdout)
stream_handler.setLevel(logging.DEBUG)
stream_handler.setFormatter(colorlog.ColoredFormatter(
	'%(log_color)s%(levelname)-8s%(reset)s | %(log_color)s%(message)s%(reset)s'))
log.addHandler(stream_handler)
log.setLevel(logging.DEBUG)

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
