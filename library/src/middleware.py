"""Module containing middleware implementation for library service"""
import re
from threading import get_ident
from ast import literal_eval
from middleware_connector import middleware
from src.log import log

class LibraryMiddleware(middleware.Middleware):
    """Middleware extender"""

    MESSAGES = {}

    def __init__(self, params, pid):
        self.__queue_name = f'library-{pid}'
        super().__init__(params)

    def publish_message(self, receiver, message):
        """Library implementation of publish_message method"""
        thread_id = get_ident()
        thread_queue = f'{self.__queue_name}-{thread_id}'
        self.publish(receiver, message, thread_queue)
        self.consume(thread_queue, False)

    def on_message_received(self, __ch, __method, __properties, body):
        """Consume method overrider"""
        thread_id = get_ident()
        log.info(f'Received {body} on {thread_id}')
        self.MESSAGES[thread_id] = body

    def get_thread_response(self):
        """
        Library implementation of get_thread_response

        :return str:the response for this thread
        """
        thread_id = get_ident()
        response = self.MESSAGES[thread_id]

        body_decoded = str(response, 'utf-8')

        # TODO lieve help
        match = re.findall(r"Decimal\('(.*?)'\)", body_decoded)
        for dec in match:
            body_decoded = body_decoded.replace(f"Decimal('{dec}')", dec)

        # try to parse the response
        try:
            body_parsed = literal_eval(body_decoded)
        except ValueError:
            raise DatabaseException(f'Could not parse database response: {body_decoded}')
        except Exception:
            raise DatabaseException(f'Unhandled database exception: {body_decoded}')

        self.MESSAGES[thread_id] = None
        return body_parsed

    def get_thread_binary_response(self):
        """
        Library implementation of get_thread_response

        :return str:the binary response for this thread
        """
        thread_id = get_ident()
        response = self.MESSAGES[thread_id]
        self.MESSAGES[thread_id] = None
        return response

class DatabaseException(Exception):
    """
    Database exception.
    """

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
