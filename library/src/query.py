"""
Queries for other services
"""

from flask import current_app as app
from src import utils
from .schemas.auth import *

class Query:
    """Class containing book query procedures"""

    def __init__(self):
        self.MW = app.config['MW']

    def check_key_validity(self, table_name, variable_name, primary_key):
        """ checks if pk exists in the database, usefull for generating unique ids """
        #TODO: write test to check if this works
        query = f"SELECT %s FROM %s WHERE %s = %s;|" \
                f"('{variable_name}','{table_name}','{variable_name}','{1000000}')"
        self.MW.publish_message('middleware-db',query)
        resp = self.MW.get_thread_response()
        # if the response is not null it means the primary key does exist in the database
        if resp is None:
            return True
        return False

    def get_all_books_query(self, user_data):
        """Queries all books from the database """

        query = f"SELECT * " \
                f"FROM books " \
                f";|()"
        # get the response from the database
        self.MW.publish_message('middleware-db',query)
        resp = self.MW.get_thread_response()
        # parse the response
        files = []
        if resp:
            for file in resp:
                file_data = {}
                file_data['book_id'] = file[0]
                file_data['book_id_str'] = f'{file[0]}'
                file_data['title'] = file[1]
                file_data['last_edited'] = file[2]
                files.append(file_data)

        return files

    def get_books_query(self, id):
        """Queries books from the database with the given value"""
        query = f"SELECT * " \
                f"FROM books " \
                f"WHERE book_id=%s;|('{id}',)"
        # get the response from the database
        self.MW.publish_message('middleware-db',query)
        resp = self.MW.get_thread_response()
        # parse the response
        files = []
        for file in resp:
            file_data = {}
            file_data['book_id'] = file[0]
            file_data['book_id_str'] = f'{file[0]}'
            file_data['title'] = file[1]
            file_data['last_edited'] = file[2]
            file_data['sections'] = self.get_sections_query(file[0])
            files.append(file_data)
        return files

    def get_sections_query(self, book_id):
        """Queries sections from the database with the given book id"""
        query = f"SELECT * " \
                f"FROM book_sections " \
                f"WHERE book_id=%s;|('{book_id}',)"
        # get the response from the database
        self.MW.publish_message('middleware-db',query)
        resp = self.MW.get_thread_response()
        # parse the response
        files = []
        for file in resp:
            file_data = {}
            file_data['book_section_id'] = file[0]
            file_data['book_section_id_str'] = f'{file[0]}'
            file_data['book_id'] = file[1]
            file_data['book_id_str'] = str(file[1])
            file_data['title'] = file[2]
            file_data['section_nr'] = file[3]
            file_data['contents'] = self.get_contents_query(file[0])
            files.append(file_data)
        return files

    def get_contents_query(self, section_id):
        """Queries content from the database with the given section id"""
        query = f"SELECT * " \
                f"FROM book_content " \
                f"WHERE book_section_id=%s;|('{section_id}',)"
        # get the response from the database
        self.MW.publish_message('middleware-db',query)
        resp = self.MW.get_thread_response()
        # parse the response
        files = []
        for file in resp:
            file_data = {}
            file_data['book_content_id'] = file[0]
            file_data['book_content_id_str'] = str(file[0])
            file_data['book_section_id'] = file[1]
            file_data['book_section_id_str'] = str(file[1])
            file_data['content_nr'] = file[2]
            file_data['content_type'] = file[3]
            files.append(file_data)
        return files

    def book_create(self, user_data, new_book_values = None):
        """create book"""

        # check if the user is authorized to perform this action
        if user_data.auth_level < 3:
            raise Exception("Token missing or invalid")

        if new_book_values is None:
            return
        query = f"INSERT INTO books (book_id, title, last_edited)" \
                f" VALUES (%s, %s, %s);|" \
                f"('{new_book_values['book_id']}', '{new_book_values['title']}'," \
                f" '{new_book_values['last_edited']}')"
        # get the response from the database
        self.MW.publish_message('middleware-db',query)
        resp = self.MW.get_thread_response()
        return resp

    def book_update(self, book_id, book_info, user_data):
        """Update book information in the Books table"""
        # check if the user is authorized to perform this action
        if user_data.auth_level < 3:
            raise Exception("Token missing or invalid")

        if book_id is None:
            raise ValueError('one of the variables is empty')
        #check if id exists
        query = f"SELECT * " \
                f"FROM books " \
                f"WHERE book_id=%s;|({book_id},)"
        self.MW.publish_message('middleware-db',query)
        resp = self.MW.get_thread_response()
        if len(resp) == 0:
            raise Exception("Book does not exist")
        #build query
        query = "UPDATE books SET "
        for key in book_info.keys():
            query += f"{key}=%s, "
        query = query[:-2]
        query += " WHERE book_id=%s;|("
        for key in book_info.keys():
            query += f"'{book_info[key]}',"
        query += f"'{book_id}',)"
        #send query to db
        self.MW.publish_message('middleware-db',query)
        resp = self.MW.get_thread_response()

    def book_delete(self, book_id, user_data):
        """Deletes a specific book with book_id """
        # check if the user is authorized to perform this action
        if user_data.auth_level < 3:
            raise Exception("Token missing or invalid")
        #check if book exists
        query = f"SELECT * " \
                f"FROM books " \
                f"WHERE book_id=%s;|({book_id},)"
        self.MW.publish_message('middleware-db',query)
        resp = self.MW.get_thread_response()
        if len(resp) == 0:
            raise Exception("Book does not exist")
        #delete book
        query = f"DELETE FROM books " \
                f"WHERE book_id=%s;|({book_id},)"
        self.MW.publish_message('middleware-db',query)
        resp = self.MW.get_thread_response()

    def book_section_create(self, new_book_section_values, user_data):
        """create book query"""
        # check if the user is authorized to perform this action
        if user_data.auth_level < 3:
            raise Exception("Token missing or invalid")

        query = f"INSERT INTO book_sections (book_section_id, book_id, title, section_nr)" \
                f" VALUES (%s, %s, %s, %s);|" \
                f"('{new_book_section_values['book_section_id']}', " \
                f"'{new_book_section_values['book_id']}'," \
                f" '{new_book_section_values['title']}', '{new_book_section_values['section_nr']}')"
        # get the response from the database
        self.MW.publish_message('middleware-db',query)
        resp = self.MW.get_thread_response()
        return resp

    def book_sections_update(self, book_section_id, new_book_section_values, user_data):
        """Update book sections information in the book_sections table"""
        # check if the user is authorized to perform this action
        if user_data.auth_level < 3:
            raise Exception("Token missing or invalid")
        #check if id exists
        query = f"SELECT * " \
                f"FROM book_sections " \
                f"WHERE book_section_id=%s;|({book_section_id},)"
        self.MW.publish_message('middleware-db',query)
        resp = self.MW.get_thread_response()
        if len(resp) == 0:
            raise Exception("Book Section does not exist")
        query = f"SELECT book_id " \
                f"FROM book_sections " \
                f"WHERE book_section_id=%s;|({book_section_id},)"
        self.MW.publish_message('middleware-db',query)
        resp = self.MW.get_thread_response()
        book_id = resp[0][0]
        #build query
        query = "UPDATE book_sections SET "
        for key in new_book_section_values.keys():
            query += f"{key}=%s, "
        query = query[:-2]
        query += " WHERE book_section_id=%s;|("
        for key in new_book_section_values.keys():
            query += f"'{new_book_section_values[key]}',"
        query += f"'{book_section_id}',)"
        #send query to db
        self.MW.publish_message('middleware-db',query)
        resp = self.MW.get_thread_response()

        query = f"UPDATE books SET last_edited=%s WHERE book_id=%s;|" \
                f"({utils.get_timestamp()},{book_id})"
        self.MW.publish_message('middleware-db',query)
        resp = self.MW.get_thread_response()

    def book_section_delete(self, book_section_id, user_data):
        """Deletes a specific book section with book_section_id """
        # check if the user is authorized to perform this action
        if user_data.auth_level < 3:
            raise Exception("Token missing or invalid")
        #check if book section exists
        query = f"SELECT * " \
                f"FROM book_sections " \
                f"WHERE book_section_id=%s;|({book_section_id},)"
        self.MW.publish_message('middleware-db',query)
        resp = self.MW.get_thread_response()
        if len(resp) == 0:
            raise Exception("Book section does not exist")
        #delete book
        query = f"DELETE FROM book_sections " \
                f"WHERE book_section_id=%s;|({book_section_id},)"
        self.MW.publish_message('middleware-db',query)
        resp = self.MW.get_thread_response()

    def create_book_content(self, new_data, user_data):
        """ creates new entry in db """
        # check if the user is authorized to perform this action
        if user_data.auth_level < 3:
            raise Exception("Token missing or invalid")

        #check if book section exists
        query = f"SELECT * " \
                f"FROM book_sections " \
                f"WHERE book_section_id=%s;|({new_data['book_section_id']},)"
        self.MW.publish_message('middleware-db',query)
        resp = self.MW.get_thread_response()
        if len(resp) == 0:
            raise Exception("Book section does not exist")

        query = f"INSERT INTO book_content (book_content_id, book_section_id, content_nr, content_type)" \
                f" VALUES (%s, %s, %s, %s);|" \
                f"('{new_data['book_content_id']}', '{new_data['book_section_id']}'," \
                f" '{new_data['content_nr']}', '{new_data['content_type']}')"
        # get the response from the database
        self.MW.publish_message('middleware-db',query)
        resp = self.MW.get_thread_response()
        return resp

    def get_book_content(self, content_id):
        """ creates new entry in db """
        #check if book section exists
        query = f"SELECT * " \
                f"FROM book_content " \
                f"WHERE book_content_id=%s;|({content_id},)"
        self.MW.publish_message('middleware-db',query)
        resp = self.MW.get_thread_response()
        if len(resp) == 0:
            raise Exception("Book content does not exist")
        return resp

    def update_book_content(self, book_content_data, content_id, user_data):
        """ updates book content """
        # check if the user is authorized to perform this action
        if user_data.auth_level < 3:
            raise Exception("Token missing or invalid")

        #check if id exists
        query = f"SELECT * " \
                f"FROM book_content " \
                f"WHERE book_content_id=%s;|({content_id},)"
        self.MW.publish_message('middleware-db',query)
        resp = self.MW.get_thread_response()
        if len(resp) == 0:
            raise Exception("Book_content does not exist")
        #build query
        query = "UPDATE book_content SET "
        for key in book_content_data.keys():
            query += f"{key}=%s, "
        query = query[:-2]
        query += " WHERE book_content_id=%s;|("
        for key in book_content_data.keys():
            query += f"'{book_content_data[key]}',"
        query += f"'{content_id}',)"
        self.MW.publish_message('middleware-db',query)
        resp = self.MW.get_thread_response()

    def delete_book_content(self, content_id, user_data):

        # check if the user is authorized to perform this action
        if user_data.auth_level < 3:
            raise Exception("Token missing or invalid")
        #check if id exists
        query = f"SELECT * " \
                f"FROM book_content " \
                f"WHERE book_content_id=%s;|({content_id},)"
        self.MW.publish_message('middleware-db',query)
        resp = self.MW.get_thread_response()
        if len(resp) == 0:
            raise Exception("Book_content_id does not exist")
        #delete book content
        query = f"DELETE FROM book_content " \
            f"WHERE book_content_id=%s;|({content_id},)"
        self.MW.publish_message('middleware-db',query)
        resp = self.MW.get_thread_response()

    def get_user_data(self, access_token):
        """ gets user data associated with the token """

        if access_token is None:
            raise Exception("Token missing or invalid")

        body = UserInfoRequest()
        body.access_token = access_token
        request = AuthRequest()
        request.request_type = RequestType.USER_INFO_REQUEST
        request.request_body = body.pack()
        self.MW.publish_message('auth', request.pack())
        response = self.MW.get_thread_binary_response()
        response = AuthResponse.unpack(response)

        if response.response_type == ResponseType.ERROR_RESPONSE:
            raise Exception("Token missing or invalid")

        user_info = UserInfo.unpack(response.response_body)
        return user_info

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
