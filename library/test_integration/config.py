"""Config for itests"""

# host
host = 'http://library:5000'
jwt = ''
token_password_valid = {
    'grant_type': 'password',
    'email': 'admin@uce.nl',
    'password': '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8'
}
#test course data
test_course_id = 1000000
#test books
test_book_id = 1000010
test_book_object_update = {"title": "changed_book_itests"}
test_book_object_create = {"title": "book_itests"}
#test sections
test_section_id = 11
test_section_object_update = {"title": "section_text_updated", "section_nr": 0}
test_section_object_update_wrong = {"wrong_key": "section_text_updated", "section_nr": 0}
test_section_object_create = {"title": "section_text_create", "section_nr": 0}
#test content
test_book_content = { "content_type": "text", "content_nr": 6 }
test_book_content_update = { "content_type": "text", "content_nr": 7 }
test_book_content_update_wrong = { "content_type": "wrong", "content_nr": 7 }
# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
