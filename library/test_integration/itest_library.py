""""Test module that tests connections to the library service"""

import unittest
import requests
from random import randint, random
from json import loads, dumps
from time import sleep

from . import config

class Library(unittest.TestCase):
    """Test dynamic content"""

    @classmethod
    def setUpClass(cls):
        """Wait 20 seconds to allow for setup"""
        sleep(20)
        res = requests.post('http://auth:5000/auth/token', json=config.token_password_valid)
        config.jwt = "Bearer " + res.json()['access_token']

    def test_1_no_jwt_GET_books(self):
        """Test whether a GET request with no JWT yields a 400"""
        r = requests.get(f'{config.host}/books/')
        self.assertIn('Token missing or invalid', r.json().values())
        self.assertEqual(r.status_code, 401)

    def test_2_correct_request_GET_books(self):
        """Test whether a GET request correct values yields a 200"""
        header = {'authorization': config.jwt}
        r = requests.get(f'{config.host}/books/', headers = header)
        self.assertEqual(r.status_code, 200)

    def test_3_no_jwt_POST_books(self):
        """Test whether a POST request with no jwt yields a 400"""
        r = requests.post(f'{config.host}/books/')
        self.assertIn('Token missing or invalid', r.json().values())
        self.assertEqual(r.status_code, 401)

    def test_4_no_request_data_POST_books(self):
        """Test whether a POST request with no request data yields a 400"""
        header = {'authorization': config.jwt}
        r = requests.post(f'{config.host}/books/', headers = header)
        self.assertIn('Bad request, missing required parameters or otherwise malformed', r.json().values())
        self.assertEqual(r.status_code, 400)

    def test_5_no_auth_GET_books(self):
        """Test whether a GET request with no JWT yields a 400"""
        r = requests.get(f'{config.host}/books/000000000')
        self.assertIn('Token missing or invalid', r.json().values())
        self.assertEqual(r.status_code, 401)

    def test_6_wrong_id_GET_books(self):
        """Test whether a GET request with wrong id yields a 400"""
        header = {'authorization': config.jwt}
        r = requests.get(f'{config.host}/books/000000000', headers=header)
        self.assertIn('Bad request, missing required parameters or otherwise malformed', r.json().values())
        self.assertEqual(r.status_code, 400)

    def test_7_correct_id_GET_books(self):
        """Test whether a GET request with correct id yields a 200"""
        header = {'authorization': config.jwt}
        r = requests.get(f'{config.host}/books/{config.test_book_id}', headers=header)
        self.assertEqual(r.status_code, 200)

    def test_8_no_jwt_PUT_books(self):
        """Test whether a PUT request with no jwt yields a 401"""
        r = requests.put(f'{config.host}/books/{config.test_book_id}')
        self.assertIn('Token missing or invalid', r.json().values())
        self.assertEqual(r.status_code, 401)

    def test_9_no_request_data_PUT_books(self):
        """Test whether a PUT request with no request data yields a 400"""
        header = {'authorization': config.jwt}
        r = requests.put(f'{config.host}/books/0000000', headers = header)
        self.assertIn('Bad request, missing required parameters or otherwise malformed', r.json().values())
        self.assertEqual(r.status_code, 400)

    def test_10_wrong_id_PUT(self):
        """Test whether a PUT request with wrong id yields a 400"""
        header = {'authorization': config.jwt}
        r = requests.put(f'{config.host}/books/0000000', headers = header, json=config.test_book_object_update)
        self.assertIn('Bad request, missing required parameters or otherwise malformed', r.json().values())
        self.assertEqual(r.status_code, 400)

    def test_11_wrong_id_DELETE_books(self):
        """Test whether a DELETE request with wrong id yields a 400"""
        header = {'authorization': config.jwt}
        r = requests.delete(f'{config.host}/books/0000000', headers = header, json=config.test_book_object_update)
        self.assertIn('Bad request, missing required parameters or otherwise malformed', r.json().values())
        self.assertEqual(r.status_code, 400)

    def test_12_correct_create_update_delete_requests_POST_PUT_DELETE_books(self):
        """Test whether a POST, PUT an DELETE request with correct values yields a 201 and 200"""
        header = {'authorization': config.jwt}
        r_create = requests.post(f'{config.host}/books/', headers=header, json=config.test_book_object_create)
        self.assertEqual(r_create.status_code, 201)
        json = r_create.json()
        new_book_id = json['id']

        r_update = requests.put(f'{config.host}/books/{new_book_id}', headers = header, json=config.test_book_object_update)
        self.assertEqual(r_update.status_code, 200)

        r_delete = requests.delete(f'{config.host}/books/{new_book_id}', headers = header)
        self.assertEqual(r_delete.status_code, 200)

    def test_13_no_jwt_POST_sections(self):
        """Test whether a POST request with no jwt yields a 401"""
        r = requests.post(f'{config.host}/books/{config.test_book_id}/sections', json=config.test_section_object_create)
        self.assertIn('Token missing or invalid', r.json().values())
        self.assertEqual(r.status_code, 401)

    def test_14_no_request_data_POST_sections(self):
        """Test whether a POST request with no request data yields a 400"""
        header = {'authorization': config.jwt}
        r = requests.post(f'{config.host}/books/{config.test_book_id}/sections', headers = header)
        self.assertIn('Bad request, missing required parameters or otherwise malformed', r.json().values())
        self.assertEqual(r.status_code, 400)

    def test_15_no_jwt_PUT_sections(self):
        """Test whether a PUT request with no jwt yields a 401"""
        r = requests.put(f'{config.host}/books/sections/{config.test_section_id}', json=config.test_section_object_update)
        self.assertIn('Token missing or invalid', r.json().values())
        self.assertEqual(r.status_code, 401)

    def test_16_no_request_data_PUT_sections(self):
        """Test whether a PUT request with no request data yields a 400"""
        header = {'authorization': config.jwt}
        r = requests.put(f'{config.host}/books/sections/{config.test_section_id}', headers=header)
        self.assertIn('Bad request, missing required parameters or otherwise malformed', r.json().values())
        self.assertEqual(r.status_code, 400)

    def test_17_wrong_key_PUT_sections(self):
        """Test whether a PUT request with no request data yields a 500"""
        header = {'authorization': config.jwt}
        r = requests.put(f'{config.host}/books/sections/{config.test_section_id}', headers=header, json=config.test_section_object_update_wrong)
        self.assertEqual(r.status_code, 200)

    def test_18_wrong_id_PUT_sections(self):
        """Test whether a PUT request with wrong id yields a 400"""
        header = {'authorization': config.jwt}
        r = requests.put(f'{config.host}/books/sections/00000', headers=header, json=config.test_section_object_update)
        self.assertIn('Bad request, missing required parameters or otherwise malformed', r.json().values())
        self.assertEqual(r.status_code, 400)

    def test_19_correct_requests_POST_PUT_DELETE_sections(self):
        """Test whether a POST, PUT and DELETE request with correct values yields a 200"""
        header = {'authorization': config.jwt}
        r_create_section = requests.post(f'{config.host}/books/{config.test_book_id}/sections', headers=header, json=config.test_section_object_create)
        self.assertEqual(r_create_section.status_code, 201)
        json = r_create_section.json()
        new_book_section_id = json['id']

        r_update_section = requests.put(f'{config.host}/books/sections/{new_book_section_id}', headers=header, json=config.test_section_object_update)
        self.assertEqual(r_update_section.status_code, 200)

        r_delete_section = requests.delete(f'{config.host}/books/sections/{new_book_section_id}', headers=header)
        self.assertEqual(r_delete_section.status_code, 200)

    def test_20_wrong_id_DELETE_sections(self):
        """Test whether a DELETE request with no request data yields a 400"""
        header = {'authorization': config.jwt}
        r = requests.delete(f'{config.host}/books/sections/0000000', headers=header)
        self.assertIn('Bad request, missing required parameters or otherwise malformed', r.json().values())
        self.assertEqual(r.status_code, 400)

    def test_21_no_jwt_DELETE_sections(self):
        """Test whether a DELETE request with no request data yields a 401"""
        r = requests.delete(f'{config.host}/books/sections/{config.test_section_id}')
        self.assertIn('Token missing or invalid', r.json().values())
        self.assertEqual(r.status_code, 401)

    def test_22_create_book_content_POST(self):
        """Test whether a POST request with correct value uploads a file """
        testfile = open("test.txt","w+")
        testfile.write("Mooie test.\n")
        testfile.close()

        uploadform = {"book_content": dumps(config.test_book_content)}
        testfile = open("test.txt", "r")
        uploadfile = {"file": ("test.txt", testfile, 'text/plain')}


        with self.subTest("correct upload"):
            # test a correct upload of a file
            r = requests.post(f'{config.host}/books/sections/{config.test_section_id}/contents',
                data=uploadform,
                files=uploadfile,
                headers={"authorization":config.jwt})

            self.assertEqual(r.status_code, 201)

        with self.subTest("no file attached"):
            # test an upload with no file attached
            r = requests.post(f'{config.host}/books/sections/{config.test_section_id}/contents',
            data=uploadform,
            headers={"authorization":config.jwt})

            self.assertEqual(r.status_code, 400)

        with self.subTest("wrong section id"):
            # test an upload where the given section does not exist
            r = requests.post(f'{config.host}/books/sections/0000000/contents',
                data=uploadform,
                files=uploadfile,
                headers={"authorization":config.jwt})
            self.assertEqual(r.status_code, 400)

        with self.subTest("illegal file type"):
            # test if a file of a non-whitelisted type gets denied
            # TODO wat is de echte filename?????
            illegalfile = open("test.blub","w+")
            illegalfile.write("Mooie quiz.\n")

            upload_wrong_file = {"file": ("test.blub", illegalfile, 'test/blub')}

            r = requests.post(f'{config.host}/books/sections/{config.test_section_id}/contents',
                data=uploadform,
                files=upload_wrong_file,
                headers={"authorization":config.jwt})
            illegalfile.close()
            self.assertEqual(r.status_code, 400)

        with self.subTest("illegal content type"):
            # test if an illegal content type gets denied
            illegal_book_content = { "content_type": "wrong_type", "content_nr": 6 }
            illegal_uploadform = {"book_content": dumps(illegal_book_content)}

            r = requests.post(f'{config.host}/books/sections/{config.test_section_id}/contents',
                data=illegal_uploadform,
                files=uploadfile,
                headers={"authorization":config.jwt})

            self.assertEqual(r.status_code, 400)
        testfile.close()

    def test_23_download_book_content_GET(self):
        """ Test downloading of book content """

        with self.subTest("correct download"):
            # test a correct download of a file
            r = requests.get(f'{config.host}/books/sections/contents/101',
                headers={"authorization":config.jwt})

            self.assertEqual(r.status_code, 200)

        with self.subTest("download content does not exist"):
            # test non existing file id
            r = requests.get(f'{config.host}/books/sections/contents/0000',
                headers={"authorization":config.jwt})

            self.assertEqual(r.status_code, 400)

    def test_24_update_book_content(self):
        """ Test updating of book content """

        testfile = open("test.txt","w+")
        testfile.write("update tekst.\n")

        uploadform = {"book_content": dumps(config.test_book_content_update)}
        uploadfile = {"file": ("update_text.txt", testfile, 'text/plain')}

        with self.subTest("correct update"):
            # test a correct update of book content
            r = requests.put(f'{config.host}/books/sections/contents/101',
                headers={"authorization":config.jwt},
                data=uploadform,
                files=uploadfile)

            self.assertEqual(r.status_code, 200)

        with self.subTest("correct update only file"):
            # test a correct update of pincontent
            r = requests.put(f'{config.host}/books/sections/contents/101',
                headers={"authorization":config.jwt},
                files=uploadfile)

            self.assertEqual(r.status_code, 200)

        with self.subTest("correct update only form"):
            # test a correct update of pincontent
            r = requests.put(f'{config.host}/books/sections/contents/101',
                headers={"authorization":config.jwt},
                data=uploadform)

            self.assertEqual(r.status_code, 200)

        with self.subTest("update book content no parameters or file supplied"):
            # test a correct update of pincontent
            r = requests.put(f'{config.host}/books/sections/contents/101',
                headers={"authorization":config.jwt})

            self.assertEqual(r.status_code, 400)

        with self.subTest("incorrect content id"):
            # test a correct update of pincontent
            r = requests.put(f'{config.host}/books/sections/contents/1021wrong_id12',
                headers={"authorization":config.jwt})

            self.assertEqual(r.status_code, 400)

        with self.subTest("illegal file type"):
            # test if a file of a non-whitelisted type gets denied
            illegalfile = open("test.blub","w+")
            illegalfile.write("Mooie quiz.\n")

            upload_wrong_file = {"file": ("test.blub", illegalfile, 'test/blub')}

            r = requests.put(f'{config.host}/books/sections/contents/101',
                data=uploadform,
                files=upload_wrong_file,
                headers={"authorization":config.jwt})
            illegalfile.close()
            self.assertEqual(r.status_code, 400)

        with self.subTest("illegal content type"):
            # test if a file of a non-whitelisted content type gets denied
            uploadform_illegal = {"book_content": dumps(config.test_book_content_update_wrong)}
            r = requests.put(f'{config.host}/books/sections/contents/101',
                data=uploadform_illegal,
                files=uploadfile,
                headers={"authorization":config.jwt})
            self.assertEqual(r.status_code, 400)

        testfile.close()

    def test_25_delete_book_content(self):
        """ Test deleting of book content """

        with self.subTest("delete book content correctly"):
            r = requests.delete(f'{config.host}/books/sections/contents/104',
                headers={"authorization":config.jwt})

            self.assertEqual(r.status_code, 200)

        with self.subTest("delete pincontent id does not exist"):
            r = requests.delete(f'{config.host}/books/sections/contents/0000',
                headers={"authorization":config.jwt})

            self.assertEqual(r.status_code, 400)

        with self.subTest("delete book content invalid id"):
            r = requests.delete(f'{config.host}/books/sections/contents/00dfs00',
                headers={"authorization":config.jwt})

            self.assertEqual(r.status_code, 400)
if __name__ == '__main__':
    unittest.main()

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
