"""Test module that tests the library helper functions"""

import unittest
import random
import string

def random_string(length=None):
    """Generate a random string with length n"""

    if length is None:
        length = random.randint(1, 32)

    # join n random characters
    return ''.join([random.choice(string.ascii_letters + string.digits) for _ in range(length)])

if __name__ == '__main__':
    unittest.main()

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
