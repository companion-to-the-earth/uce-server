"""
Flask app initializer
"""

from os import getpid, environ
from threading import get_ident
from flask import Flask
from flask_mailman import Mail
from src.mail import mail
from src.log import log

def get_app():
    """
    Flask app initializer

    :return Flask: the mail flask app
    """
    app = Flask(__name__)
    app.register_blueprint(mail)

    app.config['DOMAIN_NAME'] = environ['DOMAIN_NAME']

    # Mail config
    app.config['MAIL_SERVER'] = environ['MAIL_SERVER']
    app.config['MAIL_PORT'] = environ['MAIL_PORT']
    app.config['MAIL_USERNAME'] = environ['MAIL_USERNAME']
    app.config['MAIL_PASSWORD'] = environ['MAIL_PASSWORD']
    app.config['MAIL_USE_TLS'] = environ['MAIL_USE_TLS']
    app.config['MAIL_USE_SSL'] = environ['MAIL_USE_SSL']
    app.config['MAIL_DEFAULT_SENDER'] = environ['MAIL_DEFAULT_SENDER']

    try:
        app.config['MAIL_BACKEND'] = environ['MAIL_BACKEND']
        log.info("Using %s backend", environ['MAIL_BACKEND'])
    except KeyError:
        log.info("Using default backend")

    Mail(app)

    pid = getpid()
    log.info("Starting Flask app on process %s", pid)

    @app.before_request
    def print_tid():
        tid = get_ident()
        log.debug("Handling request on thread %s", tid)

    return app

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
