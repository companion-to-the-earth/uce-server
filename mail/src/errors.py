"""
Module containing API errors
"""

class APIError(Exception):
    """
    Generic API error class
    """
    status_code = 500
    message="Internal server error"

    def __init__(self, message=None):
        if message:
            self.message = message
        super().__init__(message)

class BadRequestError(APIError):
    """
    BadRequestError
    Raised if parameters are missing or invalid
    """
    status_code=400
    message="Invalid request"

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
