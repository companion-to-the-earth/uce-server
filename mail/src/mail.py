"""
Mail blueprint, contains internal API to send mails
"""

from flask import Blueprint, request, render_template
from flask import current_app as app
from flask_mailman import EmailMessage
from src import errors
from src.log import log

mail = Blueprint('mail', __name__, template_folder='templates/')

@mail.route('/', methods=['POST'])
def send_mail():
    """
    Send mail endpoint

    Sends the requested type of mail based on mail_type
    """
    try:
        mail_type = request.json['mail_type']
        to = request.json['to']
    except (TypeError, KeyError):
        raise errors.BadRequestError()

    if mail_type == 'password_reset':
        try:
            name = request.json['name']
            token = request.json['token']
        except KeyError:
            raise errors.BadRequestError()

        send_password_reset_mail(name, to, token)

    elif mail_type == 'user_created':
        try:
            token = request.json['token']
        except KeyError:
            raise errors.BadRequestError()

        send_user_created_email(to, token)

    else:
        raise errors.BadRequestError()

    return '',200

def send_password_reset_mail(name, to, reset_token):
    """
    Send password reset email

    :param str name: Name of the user
    :param str to: Recipient email address
    :param str reset_token: Reset password token
    """
    subject = 'UCE - Password Reset'
    reset_url = f"https://{app.config['DOMAIN_NAME']}/reset-password?token={reset_token}"
    body = render_template('password_reset.txt',
                            name=name,
                            reset_url=reset_url)

    msg = EmailMessage(
        subject,
        body,
        to=[to]
    )
    msg.send(fail_silently=False)

def send_user_created_email(to, verification_token):
    """
    Send user created email

    Sent after a user is created with the user creation endpoint,
    provides a link to a form to enter username and password

    :param str to: Recipient email address
    :param str verification_token: Token required to create account
    """
    subject = 'UCE - New Account'
    verification_url=f"https://{app.config['DOMAIN_NAME']}/verify?token={verification_token}"
    body = render_template('user_created.txt',
                           verification_url=verification_url)

    msg = EmailMessage(
        subject,
        body,
        to=[to]
    )
    msg.send(fail_silently=False)

@mail.errorhandler(Exception)
def handle_error(error):
    """
    Flask error handler

    APIErrors are returned to the client, any other exception is logged
    and returned as an internal server error so no internal details are leaked
    """
    if isinstance(error, errors.APIError):
        return (error.message, error.status_code)

    log.error("%s: %s", type(error).__name__, error)

    return ("Internal server error", 500)

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
