"""Integration test config"""

HOST = 'http://mail:5000'

password_reset_valid = {
    'mail_type': 'password_reset',
    'to': 'user@uce.nl',
    'name': 'user',
    'token': 'token'
}

password_reset_missing_type = {
    'to': 'user@uce.nl',
    'name': 'user',
    'token': 'token'
}

password_reset_invalid_type = {
    'mail_type': 'invalid',
    'to': 'user@uce.nl',
    'name': 'user',
    'token': 'token'
}

password_reset_missing_recipient = {
    'mail_type': 'password_reset',
    'name': 'user',
    'token': 'token'
}

password_reset_missing_name = {
    'mail_type': 'password_reset',
    'to': 'user@uce.nl',
    'token': 'token'
}

password_reset_missing_token = {
    'mail_type': 'password_reset',
    'to': 'user@uce.nl',
    'name': 'user'
}

user_created_valid = {
    'mail_type': 'user_created',
    'to': 'user@uce.nl',
    'token': 'token'
}

user_created_missing_type = {
    'to': 'user@uce.nl',
    'token': 'token'
}

user_created_invalid_type = {
    'mail_type': 'invalid',
    'to': 'user@uce.nl',
    'token': 'token'
}

user_created_missing_recipient = {
    'mail_type': 'user_created',
    'token': 'token'
}

user_created_missing_token = {
    'mail_type': 'user_created',
    'to': 'user@uce.nl',
}

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
