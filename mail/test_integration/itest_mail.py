"""
Mail service integration tests
"""

import unittest
import requests
from . import config

class TestMail(unittest.TestCase):
    """Integration test class"""

    def test_password_reset_mail(self):
        """
        Test reset mail
        """
        url = f"{config.HOST}/"

        with self.subTest("Password reset - valid"):
            r = requests.post(url, json=config.password_reset_valid)
            self.assertEqual(r.status_code, 200)

        with self.subTest("Password reset - missing type"):
            r = requests.post(url, json=config.password_reset_missing_type)
            self.assertEqual(r.status_code, 400)

        with self.subTest("Password reset - invalid type"):
            r = requests.post(url, json=config.password_reset_invalid_type)
            self.assertEqual(r.status_code, 400)

        with self.subTest("Password reset - missing recipient"):
            r = requests.post(url, json=config.password_reset_missing_recipient)
            self.assertEqual(r.status_code, 400)

        with self.subTest("Password reset - missing name"):
            r = requests.post(url, json=config.password_reset_missing_name)
            self.assertEqual(r.status_code, 400)

        with self.subTest("Password reset - missing token"):
            r = requests.post(url, json=config.password_reset_missing_token)
            self.assertEqual(r.status_code, 400)

    def test_user_created_mail(self):
        """
        Test user created mail
        """
        url = f"{config.HOST}/"

        with self.subTest("User created - valid"):
            r = requests.post(url, json=config.user_created_valid)
            self.assertEqual(r.status_code, 200)

        with self.subTest("User created - missing type"):
            r = requests.post(url, json=config.user_created_missing_type)
            self.assertEqual(r.status_code, 400)

        with self.subTest("User created - invalid type"):
            r = requests.post(url, json=config.user_created_invalid_type)
            self.assertEqual(r.status_code, 400)

        with self.subTest("User created - missing recipient"):
            r = requests.post(url, json=config.user_created_missing_recipient)
            self.assertEqual(r.status_code, 400)

        with self.subTest("User created - missing token"):
            r = requests.post(url, json=config.user_created_missing_token)
            self.assertEqual(r.status_code, 400)

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
