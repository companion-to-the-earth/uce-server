#https://hub.docker.com/_/python
FROM python:3.8-alpine3.10

ENV PYTHONPATH="$PYTHONPATH:app"
ARG STAGE
ARG CD_STAGE_TEST

# add wait script so we can wait for dependencies to be available
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.8.0/wait /wait
RUN chmod +x /wait

COPY requirements.prod.txt requirements.test.txt ./

#install test dependencies if necessary
RUN [ "$STAGE" = "$CD_STAGE_TEST" ] && pip install -r requirements.test.txt || true

#install MySQLdb
#pymysqldb performs better than mysql connector
#https://charlesnagy.info/it/python/python-mysqldb-vs-mysql-connector-query-performance
RUN apk update \
    && apk add --virtual build-deps gcc python3-dev musl-dev mariadb-dev bash git openssh \
    && apk add --no-cache py-mysqldb \
    && pip install -r requirements.prod.txt \
    && apk del build-deps

#remove unnecessary dependencies that come with base image
RUN python3 -m pip uninstall -y pip setuptools wheel

#copy code
RUN mkdir app
COPY . app/

CMD if [ "$STAGE" = "$CD_STAGE_TEST" ]; then \
    echo "test stage" \
    && /wait \
    && export COVERAGE_FILE=/.coverage \
    && exec coverage run --branch /app/src/main.py; \
    else \
    echo "prod stage" \
    && /wait \
    && exec python /app/src/main.py; \
    fi
