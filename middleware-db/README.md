# Utrecht Companion To The Earth - middleware-db
<INSERT SUMMARY>

## API
### <Close connection>
<Insert summary> 
    Closes the connection to middleware for the middleware-db service.<br/>
    Triggered by sending the message 'exit consume' to  'middleware-db'.<br/>
    The return value of this call is a return message of 'closed connection with middleware' with the same correlation id of the call.<br/>
    This call has no parameters.<br/>


###### Example
<Insert example> 
    self.publish('middleware-db', 'exit consume', 'foo', '10')
    self.consume('foo')

    (In the consume_remote_procedure_call, one could then check for the response:)

    def consume_remote_procedure_call(self, ch, method, properties, body):
        if body == 'Closed connectoin with middleware' and properties.correlation_id == '10':
            (Call succesfully triggered in middleware-db)


## API
### <Database query>
<Insert summary> 
    Any message sent to middleware-db which differs from the 'close connection' call will be treated as a mysql query and sent to the database.<br/>
    Any query results will be sent back to the sender, with the same correlation_id as the sender message.<br/>
    More information on accepted queries can be found in the database-connector API.<br/>
    

###### Example
<Insert example> 
    self.publish('middleware-db', 'SELECT * FROM Universities WHERE University_ID = -1', 'foo', '6')
    self.consume('foo')

    (In the consume_remote_procedure_call, one could then check for the response:)

    def consume_remote_procedure_call(self, ch, method, properties, body):
        if s.correlation_id == '6':
            (body is the response to our query)

## libraries
* MySQLdb: <https://mysqlclient.readthedocs.io/>
