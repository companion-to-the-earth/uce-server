"""The middleware-db service operates as a middleware API for the database.

The service receives queries through middleware, executes them on the database
and sends the query result back to the sender of the query.
"""

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)

