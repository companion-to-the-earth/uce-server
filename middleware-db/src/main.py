"""Receives queries through middleware, executes them on the database
and sends the query result back to the sender of the query.

It basically acts as a bridge between middleware and database. A middleware
API of the database."""

import signal
import sys
from os import environ
from pika import ConnectionParameters
from database_connector import database
from middleware_connector import middleware

class DatabaseMiddleware(middleware.Middleware):
    """Middleware subclass that handles requests to and from the database"""

    database = None

    def __init__(self, connection_params):
        """Create a connection with the database. Then call the parent constructor.

        :param pika.ConnectionParameters connection_params: Params used to create the connection.

        """

        self.database = database.Database('db',
                                          environ['MYSQL_USER'],
                                          environ['MYSQL_PASSWORD'],
                                          environ['MYSQL_DATABASE'])

        super().__init__(connection_params)

    def on_message_received(self, _ch, _method, properties, body):
        """When a message is received, execute the provided SQL query and send
        the result back to the sender of the query. The query send is a tuple
        packed into a string.

        :param callable callback: the callback function passed
        :param str method: the AMQP method to invoke
        :param pika.spec.BasicProperties properties: message properties
        :param str body: an SQL query

        """
        # Query the database. It returns a tuple.
        result = self.database.query(body)
        self.publish(properties.reply_to, f'{result}')

def main():
    """Initialises connection with both the database and and middleware. If
    the connections were achieved, start consuming queries send to the service
    over middleware.
    """

    connection_params = ConnectionParameters(host='middleware')
    middleware = DatabaseMiddleware(connection_params)

    middleware.consume('middleware-db', True)

def handle_sigterm(*_):
    """Handle docker exit gracefully"""
    sys.exit()

if __name__ == "__main__":
    signal.signal(signal.SIGTERM, handle_sigterm)
    main()

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
