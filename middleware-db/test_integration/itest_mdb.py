"""Test module that tests middleware-db"""

import unittest
from pika import ConnectionParameters
from middleware_connector import middleware

class ItestMiddleware(middleware.Middleware):
    """Middleware subclass used for storing main.py's responses"""

    answer = None

    #pylint: disable=W0613
    def on_message_received(self, callback, method, properties, body):
        """On message received, store the result in the answer variable."""

        self.answer = body

class TestDbMwMerge(unittest.TestCase):
    """Test class that tests middleware-db"""

    def test_query(self):
        """Verify whether we can create a table, insert some values and select
        them through middleware-db"""

        queue_self = 'itest'
        queue_middleware_db = 'middleware-db'

        # Establish connection with middleware
        connection_params = ConnectionParameters(host='middleware')
        middleware = ItestMiddleware(connection_params)

        # Create demo table
        create_table = """CREATE TABLE Demo_Users (
                          UserID  		    INT     	    NOT NULL AUTO_INCREMENT,
                          Username  		VARCHAR(20)     NOT NULL,
                          User_Password  	VARCHAR(20) 	NOT NULL,
                          Email		        VARCHAR(40)	    NOT NULL,
                          Salt		        CHAR(8) 	    NOT NULL,
                          PRIMARY KEY(UserID)
                          );|()"""

        middleware.publish(queue_middleware_db, create_table)

        # Insert demo data
        def user_query(username, password, email, salt):
            query = f"""INSERT INTO Demo_Users
                        (Username, User_Password, Email, Salt)
                        VALUES (%s, %s, %s, %s);|('{username}', '{password}', '{email}', '{salt}',)"""

            middleware.publish(queue_middleware_db, query)


        user_query('Epic Geodude',
                   'Cookies123',
                   'g.rock@students.uu.nl',
                   'asdfflkj')
        user_query('Hardcore Onyx',
                   'Stone_brick153',
                   'o.rock@students.uu.nl',
                   'asdasdkj')
        user_query('Brock',
                   'Epic_gymmaster_121',
                   'brock.the.rock@uu.nl',
                   'aoipqwer')

        # Verify whether we can select all values we just inserted
        select = """SELECT Username FROM Demo_Users
                    WHERE Username = %s;|('Dwayne the Brock',)"""

        middleware.publish(queue_middleware_db, select, reply_to=queue_self)
        middleware.consume(queue_self, False)
        print(middleware.answer)

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
