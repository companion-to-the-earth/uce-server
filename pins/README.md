# Utrecht Companion To The Earth - Pins

## Usage
Service that handles the downloading and indexing of pins. For details on the API, see [README_OPENAPI3.yml](../master/README_OPENAPI3.yml). For details on inner workings, see the generated documentation.

## Libraries
- Flask: <https://flask.palletsprojects.com/>
