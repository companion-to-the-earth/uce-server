"""
Initialiser for the pins Flask app
"""

from os import getpid
from threading import get_ident

from flask import Flask
from pika import ConnectionParameters

from src.log import log
from src.pins import pins
from src.pincontent import pincontent
from src.middleware import PinsMiddleware

def get_app():
    """
    Initialises the pins Flask app.

    :returns Flask: the pins Flask app
    """

    # initialise the app
    app = Flask(__name__)
    app.register_blueprint(pins)
    app.register_blueprint(pincontent)

    # initialise the database
    @app.before_first_request
    def init_middleware():
        # print the PID
        pid = getpid()
        log.info(f'Starting Flask app on process {pid}')

        # add the middleware to the app config
        params = ConnectionParameters(host='middleware')
        middleware = PinsMiddleware(params, pid)
        app.config['MW'] = middleware

    @app.before_request
    def print_tid():
        # print the TID
        tid = get_ident()
        log.info(f'Handling request on thread {tid}')

    return app

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
