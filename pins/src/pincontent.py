import os
import glob
import json
from flask import Blueprint, request, jsonify, send_from_directory

from src.log import log
from src.pins import get_user_data_from_header
from src.errors import APIError, BadRequestError, ValueNotFoundError, TokenError, UnauthorizedError
from src import query
from src import utils
from src.middleware import DatabaseException

pincontent = Blueprint('pincontent', __name__)

FILE_FOLDER = "/var/www/content/"
ALLOWED_EXTENSIONS = ['image/png', 'image/jpg', 'image/jpeg', 'video/mp4','text/plain']

@pincontent.route('/pins/<pin_id>/contents', methods=['POST'], provide_automatic_options=False)
def create_pin_content(pin_id):
    """ Create pin content """

    # check the token validity and get the user data
    user_data = get_user_data_from_header(request.headers)

    # check if the request is valid
    try:
        pin_id = int(pin_id)
        file = request.files['file']
        if not file or not allowed_content_type(file):
            raise BadRequestError()
        request_data_raw = request.form['pin_content']
        request_data = json.loads(request_data_raw)
        test_content_nr = int(request_data['content_nr'])
        if request_data['content_type'] not in ['image', 'video', 'quiz']:
            raise BadRequestError()
    except (ValueError, KeyError, TypeError):
        raise BadRequestError()

    # make the request to the database
    try:
        pin_content_id = get_random_pin_content_id()
        queryhandler = query.Query()
        response = queryhandler.upload_pin_content(pin_id, pin_content_id, user_data, request_data)
    except DatabaseException as exception:
        raise DatabaseException()

    # save the file, should not go wrong
    try:
        file_extension = get_file_extension_from_content_type(file.content_type)
        file.filename = str(pin_content_id)
        file.save(os.path.join(FILE_FOLDER, file.filename + '.' + file_extension))
    except Exception:
        raise DatabaseException()

    res = {}
    res['id'] = pin_content_id
    res['id_str'] = str(pin_content_id)
    return jsonify(res), 201

@pincontent.route('/pins/<pin_id>/contents/<pin_content_id>', methods=['GET'], provide_automatic_options=False)
def get_pin_content(pin_id, pin_content_id):
    """ Authorizes the downloading of pin content """

    # get the user data from the supplied JWT
    user_data = get_user_data_from_header(request.headers)

    # check if the request is valid
    try:
        pin_content_id = int(pin_content_id)
        pin_id = int(pin_id)
    except (ValueError, KeyError, TypeError):
        raise BadRequestError()

    # make a request to the database
    try:
        queryhandler = query.Query()
        content_type = queryhandler.download_pin_content(pin_id, pin_content_id, user_data)
    except DatabaseException as exception:
        raise DatabaseException()

    # get and return the file, should not go wrong
    try:
        os.chdir(FILE_FOLDER)
        temp = str(pin_content_id) + '.' + '*'
        filename = ''
        for file in glob.glob(temp):
            filename = file
        return send_from_directory(FILE_FOLDER, filename)
    except Exception:
        raise DatabaseException()

@pincontent.route('/pins/<pin_id>/contents/<pin_content_id>', methods=['PUT'], provide_automatic_options=False)
def update_pin_content(pin_id, pin_content_id):
    """ Updates pincontent """
    
    # get the user data from the supplied JWT
    user_data = get_user_data_from_header(request.headers)

    # check if the request is valid
    try:
        # check if the id's are correct
        pin_content_id = int(pin_content_id)
        pin_id = int(pin_id)

        params = {}
        file = None

        # check the parameters if supplied
        if 'pin_content' in request.form.keys():
            request_data_raw = request.form['pin_content']
            request_data = json.loads(request_data_raw)
            # get the parameters that are supplied
            keys = list(request_data.keys())

            for key in keys:
                if key in ['content_type', 'content_nr']:
                    params[key] = request_data[key]

            # check if the request params have the correct type
            # maybe an inefficient way to check the types but it works for now
            for key in params.keys():
                if(key == 'content_type'):
                    if request_data['content_type'] not in ['image', 'video', 'quiz']:
                        raise BadRequestError()
                if(key == 'content_nr'):
                    params['content_nr'] = int(request_data['content_nr'])

        # check if a file is supplied
        if 'file' in request.files.keys():
            file = request.files['file']

        #check if the request contains any of the valid parameters or a file
        if not params and not file:
            raise BadRequestError()
        if file:
            if not allowed_content_type(file):
                raise BadRequestError()
    except (ValueError, KeyError, TypeError):
        raise BadRequestError()

    # make a request to the database
    try:
        queryhandler = query.Query()
        response = queryhandler.update_pin_content(pin_id, pin_content_id, user_data, params)
    except DatabaseException as exception:
        raise DatabaseException()

    # only save the file if it is supplied
    if file:
        try:
            # first remove the old file
            os.chdir(FILE_FOLDER)
            temp = str(pin_content_id) + '.' + '*'
            filename = ''
            for f in glob.glob(temp):
                filename = f
            os.remove(filename)

            # upload the new file
            file_extension = get_file_extension_from_content_type(file.content_type)
            file.filename = str(pin_content_id)
            file.save(os.path.join(FILE_FOLDER, file.filename + '.' + file_extension))
        except Exception:
            raise DatabaseException()

    return utils.simple_json_response(200,
        'Pin content updated')

@pincontent.route('/pins/<pin_id>/contents/<pin_content_id>', methods=['DELETE'], provide_automatic_options=False)
def delete_pin_content(pin_id, pin_content_id):
    """ Delete pin content """

    # get the user data from the supplied JWT
    user_data = get_user_data_from_header(request.headers)

    # check if the request is valid
    try:
        pin_content_id = int(pin_content_id)
        pin_id = int(pin_id)
    except (ValueError, KeyError, TypeError):
        raise BadRequestError()

    try:
        queryhandler = query.Query()
        content_type = queryhandler.delete_pin_content(pin_id, pin_content_id, user_data)
    except DatabaseException as exception:
        raise DatabaseException()

    # remove the pin content file
    try:
        os.chdir(FILE_FOLDER)
        temp = str(pin_content_id) + '.' + '*'
        filename = ''
        for f in glob.glob(temp):
            filename = f
        os.remove(filename)
    except Exception:
        raise DatabaseException()

    return utils.simple_json_response(200,
        'Pin content deleted')

def get_random_pin_content_id():
    """ generates a random non duplicate pin id """

    queryhandler = query.Query()

    # generate a random pinID until no duplicate has been made
    randompinid = utils.generate_random_id()
    while not queryhandler.is_pin_content_id_valid(randompinid):
        randompinid = utils.generate_random_id()

    return randompinid

@pincontent.errorhandler(Exception)
def handle_error(error):
    """
    Flask error handler

    APIErrors are returned to the client, any other exception is logged
    and returned as an internal server error so no internal details are leaked
    """
    if isinstance(error, APIError):
        return (error.message, error.status_code)

    log.error("%s: %s", type(error).__name__, error)

    return ("Internal server error", 500)

def allowed_content_type(file):
    """ Checks if the content type header is allowed """
    return file.content_type in ALLOWED_EXTENSIONS

def get_file_extension_from_content_type(content_type):
    """ Returns the the file extension based on a given content type header """
    if content_type == 'text/plain':
        return 'txt'
    return content_type.split('/')[1]

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
