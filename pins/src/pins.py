"""
Pins blueprint;
"""

from flask import Blueprint, request, jsonify
from src.errors import APIError, BadRequestError, ValueNotFoundError, TokenError, UnauthorizedError
from src.log import log
from src import query
from src import utils
from src.middleware import DatabaseException

pins = Blueprint('pins', __name__)

@pins.route('/pins/', methods=['GET'], provide_automatic_options=False)
def query_pins():
    """
    returns the pin id's as requested (granted the token has the access to them)
    """

    # check the JWT and get the user data
    user_data = get_user_data_from_header(request.headers)

    # check if parameter is of supported type
    try:
        # check if any argument is given
        if not request.args or len(request.args) > 1:
            raise BadRequestError()

        param = list(request.args.keys())[0]
        if param not in ['class', 'group', 'user', 'class_all']:
            raise BadRequestError()

        value = int(list(request.args.values())[0])
    except ValueError or KeyError or TypeError:
        raise BadRequestError()

    # make a request to the database
    try:
        queryhandler = query.Query()
        result = queryhandler.query_pins(param, value, user_data)
    except DatabaseException as exception:
        raise DatabaseException()

    return jsonify(result), 200

@pins.route('/pins/', methods=['POST'], provide_automatic_options=False)
def upload_pin():
    """
    inserts a new pin in the database with the information provided as json
    """

    # check the JWT and get the user data
    user_data = get_user_data_from_header(request.headers)

    # get the request data
    request_data = request.get_json(force=True, silent=True)
    if request_data is None:
        raise BadRequestError()

    #check if the request params have the correct type and contains the valid parameters
    try:
        # test if title and text can be converted to string
        if (not isinstance(request_data['title'], str) or
            not isinstance(request_data['text'], str)):
            return BadRequestError()
        # test if lat and lon can be converted to float
        lat = float(request_data['lat'])
        lon = float(request_data['lon'])
    except KeyError or TypeError:
        raise BadRequestError()

    # generate a random pinID
    randompinid = get_random_pin_id()
    timestamp = utils.get_timestamp()
    # upload the pin
    try:
        queryhandler = query.Query()
        result = queryhandler.upload_pin(user_data, randompinid,
            timestamp, request_data)
    except DatabaseException as exception:
        raise DatabaseException()

    res = {}
    res['id'] = randompinid
    res['id_str'] = str(randompinid)
    res['last_edited'] = timestamp
    return jsonify(res), 200

@pins.route('/pins/', methods=['PUT', 'DELETE'], provide_automatic_options=False)
def catch_wrong_url():
    """ Error catch when user forgets to pass an id in the url """

    raise BadRequestError()

@pins.route('/pins/<url_pin_id>', methods=['GET'], provide_automatic_options=False)
def query_specific_pin(url_pin_id):
    """ gets all the information of a specific pin """
    # check the JWT and get the user data
    user_data = get_user_data_from_header(request.headers)

    # check if the id is a valid integer value
    try:
        pin_id = int(url_pin_id)
    except ValueError:
        raise BadRequestError()

    # query the pin
    try:
        queryhandler = query.Query()
        result = queryhandler.query_pin_by_id(user_data, pin_id)
    except DatabaseException as exception:
        raise DatabaseException()

    return jsonify(result), 200

@pins.route('/pins/<url_pin_id>', methods=['PUT'], provide_automatic_options=False)
def update_specific_pin(url_pin_id):
    """ Updates a specific pin """

    # check the JWT and get the user data
    user_data = get_user_data_from_header(request.headers)

    # check if the id is a valid integer value
    try:
        pin_id = int(url_pin_id)
    except ValueError:
        raise BadRequestError()

    # get the request data
    request_data = request.get_json(force=True, silent=True)
    if request_data is None:
        raise BadRequestError()

    # get the parameters that are supplied
    keys = list(request_data.keys())
    params = {}
    for key in keys:
        if key in ['title', 'lat', 'lon', 'text']:
            params[key] = request_data[key]

    #check if the request contains any of the valid parameters
    if not params:
        raise BadRequestError()

    # check if the request params have the correct type
    # maybe an inefficient way to check the types but it works for now
    for key in params.keys():
        if(key == 'title' or key == 'text'):
            if not isinstance((params[key]), str):
                raise BadRequestError()
        if(key == 'lat' or key == 'lon'):
            try:
                lat = float(params[key])
            except TypeError or ValueError:
                raise BadRequestError()

    timestamp = utils.get_timestamp()

    # query the db
    try:
        queryhandler = query.Query()
        result = queryhandler.update_pin(pin_id, user_data, timestamp, params)
    except DatabaseException as exception:
        raise DatabaseException()

    res = {}
    res['last_edited'] = timestamp
    return jsonify(res), 200

@pins.route('/pins/<url_pin_id>', methods=['DELETE'], provide_automatic_options=False)
def delete_specific_pin(url_pin_id):
    """ Deletes a specific pin from the database """

    # check the JWT and get the user data
    user_data = get_user_data_from_header(request.headers)

    # check if the id is a valid integer value
    try:
        pin_id = int(url_pin_id)
    except ValueError:
        raise BadRequestError()

    # query the db
    try:
        queryhandler = query.Query()
        result = queryhandler.delete_pin(pin_id, user_data)
    except DatabaseException as exception:
        raise DatabaseException()

    return utils.simple_json_response(200, "Pin deleted")

@pins.route('/pins/<url_pin_id>/share', methods=['POST'], provide_automatic_options=False)
def share_specific_pin(url_pin_id):
    """ Share a specific pin with a usergroup or class """

    # check the JWT and get the user data
    user_data = get_user_data_from_header(request.headers)

    # check if the id is a valid integer value
    # check if the request data contains the necessary parameters
    # check if the request params have the correct type
    try:
        pin_id = int(url_pin_id)
        request_data = request.get_json(force=True, silent=True)
        test_group_id = int(request_data['id'])
        if not isinstance((request_data['type']), str):
            raise BadRequestError()
    except (KeyError, ValueError, TypeError):
        raise BadRequestError()

    # check if the type is either group or class
    if request_data['type'] != 'group' and request_data['type'] != 'class':
        raise BadRequestError()

    # query the db
    try:
        queryhandler = query.Query()
        result = queryhandler.share_specific_pin(pin_id, user_data, request_data)
    except DatabaseException as exception:
        raise DatabaseException()

    return utils.simple_json_response(200, "Pin shared")

@pins.route('/pins/<url_pin_id>/unshare', methods=['POST'], provide_automatic_options=False)
def unshare_specific_pin(url_pin_id):
    """ Unshare a specific pin with a usergroup or class """

    # check the JWT and get the user data
    user_data = get_user_data_from_header(request.headers)

    # check if the id is a valid integer value
    # check if the request params have the correct type
    try:
        pin_id = int(url_pin_id)
        request_data = request.get_json(force=True, silent=True)
        test_group_id = int(request_data['id'])
        if not isinstance((request_data['type']), str):
            raise BadRequestError()
    except (ValueError, KeyError, TypeError):
        raise BadRequestError()

    # check if the type is either group or class
    if request_data['type'] != 'group' and request_data['type'] != 'class':
        raise BadRequestError()

    # query the db
    try:
        queryhandler = query.Query()
        result = queryhandler.unshare_specific_pin(pin_id, user_data, request_data)
    except DatabaseException as exception:
        raise DatabaseException()

    return utils.simple_json_response(200, "Pin unshared")

def get_random_pin_id():
    """ generates a random non duplicate pin id """

    queryhandler = query.Query()

    # generate a random pinID until no duplicate has been made
    randompinid = utils.generate_random_id()
    while not queryhandler.is_pin_id_valid(randompinid):
        randompinid = utils.generate_random_id()

    return randompinid

def get_user_data_from_header(headers):
    """ gets the user data from the header """

    jwt = utils.get_JWT(headers)
    queryhandler = query.Query()
    user_data = queryhandler.get_user_data(jwt)
    return user_data

@pins.errorhandler(Exception)
def handle_error(error):
    """
    Flask error handler

    APIErrors are returned to the client, any other exception is logged
    and returned as an internal server error so no internal details are leaked
    """
    if isinstance(error, APIError):
        return (error.message, error.status_code)

    log.error("%s: %s", type(error).__name__, error)

    return ("Internal server error", 500)

#
# Internal API endpoints
#

@pins.route('/internal/pin-groups', methods=['POST'])
def create_pin_group():
    """ creates a new pin group """
    try:
        name = request.json['name']
    except (KeyError, TypeError):
        raise BadRequestError()

    group_id = utils.generate_random_id()

    try:
        queryhandler = query.Query()
        result = queryhandler.create_pin_group(group_id, name)
    except DatabaseException as exception:
        raise DatabaseException()

    return {
        'id': group_id
    }, 201

@pins.route('/internal/pin-groups/<pin_group_id>', methods=['DELETE'])
def delete_pin_group(pin_group_id):
    """ deletes the specified pin group """
    try:
        pin_group_id = int(pin_group_id)
    except ValueError:
        raise BadRequestError()

    try:
        queryhandler = query.Query()
        queryhandler.delete_pin_group(pin_group_id)
    except DatabaseException as exception:
        raise DatabaseException()

    return '', 200

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
