"""
Queries for other services
"""

from flask import current_app as app
from .schemas.auth import *
from src.errors import APIError, BadRequestError, ValueNotFoundError, TokenError, UnauthorizedError
from src.utils import validate_user_text_input

class Query:

    """Class containing pin query procedures"""

    def __init__(self):
        self.MW = app.config['MW']

    def query_pins(self, param, id, user_data):
        """ Gets all the pins of a specific class, group or user """
        query_allowed = False

        #construct the query
        # class query
        if param == 'class':
            if user_data.class_id == id or user_data.auth_level >= 2:
                query_allowed = True
                self.MW.publish_message('middleware-db',
                    "SELECT pins.pin_id, pins.title, pins.last_edited, pins.latitude, " \
                    "pins.longitude, pins.user_id, pins.text_content FROM ((pins_per_pin_group AS pppg " \
                    "INNER JOIN classes ON " \
                    f"pppg.pin_group_id = classes.pin_group_id AND classes.class_id = %s) " \
                    f"INNER JOIN pins ON pins.pin_id = pppg.pin_id);|('{id}',)")
        # class with user-groups query
        if param == 'class_all':
            if user_data.class_id == id or user_data.auth_level >= 2:
                query_allowed = True
                self.MW.publish_message('middleware-db',
                    "SELECT p.pin_id, p.title, p.last_edited, p.latitude, p.longitude, p.user_id, p.text_content " \
                    f"FROM ((user_groups AS u JOIN classes AS c ON u.class_id = c.class_id AND c.class_id = %s) " \
                    "INNER JOIN pins_per_pin_group AS pppg ON pppg.pin_group_id = c.pin_group_id OR pppg.pin_group_id = u.pin_group_id) " \
                    f"INNER JOIN pins AS p ON pppg.pin_id = p.pin_id GROUP BY p.pin_id;|('{id}',)")
        # user-group query
        if param == 'group':
            # get the class id from the group
            self.MW.publish_message('middleware-db',
                    "SELECT class_id " \
                    f"FROM user_groups WHERE user_group_id = %s;|('{id}',)")
            class_id = self.MW.get_thread_response()
            if len(class_id) > 0:
                class_id = class_id[0][0]
            if user_data.class_id == class_id or user_data.auth_level >= 2:
                query_allowed = True
                self.MW.publish_message('middleware-db',
                    "SELECT pins.pin_id, pins.title, pins.last_edited, pins.latitude, " \
                    "pins.longitude, pins.user_id, pins.text_content FROM " \
                    "((pins_per_pin_group AS pppg " \
                    "INNER JOIN user_groups ON " \
                    f"pppg.pin_group_id = user_groups.pin_group_id AND " \
                    f"user_groups.user_group_id = %s) " \
                    f"INNER JOIN pins ON pins.pin_id = pppg.pin_id);|('{id}',)")
        # user query
        if param == 'user':
            user_data_from_id = self.get_user_data_by_id(id)
            if id == user_data.id or user_data.auth_level >= 2:
                query_allowed = True
                self.MW.publish_message('middleware-db',
                    "SELECT pins.pin_id, pins.title, pins.last_edited, pins.latitude, " \
                    "pins.longitude, pins.user_id, pins.text_content FROM (pins_per_pin_group AS pppg " \
                    "INNER JOIN pins ON pins.pin_id = pppg.pin_id " \
                    f"AND pppg.pin_group_id = %s);|('{user_data_from_id.pin_group_id}',)")

        # get the pindata from the response
        if query_allowed:
            pin_data = self.MW.get_thread_response()
        else:
            raise UnauthorizedError()

        result = []
        # return empty list if the group is empty
        if pin_data:
            # parse the response
            for pin in pin_data:
                p = {}
                p['id'] = pin[0]
                p['id_str'] = str(pin[0])
                p['title'] = pin[1]
                p['last_edited'] = pin[2]
                p['latitude'] = pin[3]
                p['latitude_str'] = str(pin[3])
                p['longitude'] = pin[4]
                p['longitude_str'] = str(pin[4])
                p['user_id'] = pin[5]
                p['user_id_str'] = str(pin[5])
                p['text'] = pin[6]
                result.append(p)

        return result

    def upload_pin(self, user_data, pin_id, timestamp, request_data):
        """ uploads a pin to the database """

        # get user data neccesary
        user_id = user_data.id
        user_pin_group_id = user_data.pin_group_id

        # insert the new pin into the database
        self.MW.publish_message('middleware-db',
            f"INSERT INTO pins VALUES (%s, %s, %s, %s, %s, %s, %s);" \
            f"|('{pin_id}','{str(request_data['title'])}','{timestamp}'," \
            f"'{float(request_data['lat'])}','{float(request_data['lon'])}'," \
            f"'{user_id}','{validate_user_text_input(str(request_data['text']))}')")
        resp = self.MW.get_thread_response()

        # also add the pin to the personal pingroup of the user
        self.MW.publish_message('middleware-db',
            f"INSERT INTO pins_per_pin_group VALUES (%s, %s);" \
            f"|('{pin_id}','{user_pin_group_id}')")
        resp = self.MW.get_thread_response()

        return resp

    def query_pin_by_id(self, user_data, pin_id):
        """ gets the pin information and content from the server """

        # get the pin data from the database
        self.MW.publish_message('middleware-db',
                "SELECT pin_id, title, latitude, longitude, last_edited, user_id, text_content " \
                f"FROM pins WHERE pin_id = %s;|('{pin_id}',)")
        pin_data = self.MW.get_thread_response()
        if not pin_data:
            raise ValueNotFoundError()

        # check if user is authorized for this action
        if not self.has_user_access_to_pin(pin_id, user_data):
            raise UnauthorizedError()

        # get the pin content from the database
        self.MW.publish_message('middleware-db',
                "SELECT pin_content_id, content_type, content_nr " \
                f"FROM pin_content WHERE pin_id = %s;|('{pin_id}',)")
        pin_content = self.MW.get_thread_response()

        # gather the pin information
        # get only the first pin from the result because there can only be 1
        result = {}
        result['id'] = pin_data[0][0]
        result['id_str'] = str(pin_data[0][0])
        result['title'] = pin_data[0][1]
        result['latitude'] = pin_data[0][2]
        result['latitude_str'] = str(pin_data[0][2])
        result['longitude'] = pin_data[0][3]
        result['longitude_str'] = str(pin_data[0][3])
        result['last_edited'] = pin_data[0][4]
        result['user_id'] = pin_data[0][5]
        result['user_id_str'] = str(pin_data[0][5])
        result['text'] = pin_data[0][6]

        # gather the content
        # only get the pin content when there actually is content
        contentlist = []
        if pin_content:
            for content in pin_content:
                res = {}
                res['id'] = content[0]
                res['id_str'] = str(content[0])
                res['content_type'] = content[1]
                res['content_nr'] = content[2]
                contentlist.append(res)
        result['contents'] = contentlist

        return result

    def update_pin(self, pin_id, user_data, timestamp, params):
        """" Updates a specific pin with the information provided by the header """

        # Checks if the pin id does exist
        if not self.does_pin_exist(pin_id):
            raise ValueNotFoundError()

        # check if user is authorized for this action
        if user_data.auth_level < 2:
            self.MW.publish_message('middleware-db',
                    "SELECT 1 " \
                    f"FROM pins_per_pin_group WHERE pin_id = %s AND " \
                    f"pin_group_id = %s;|('{pin_id}','{user_data.pin_group_id}')")
            if len(self.MW.get_thread_response()) == 0:
                raise UnauthorizedError()

        # parse params to sql table column names
        # very annoying
        actual_db_parameters = {}
        for key in params.keys():
            if key == 'title':
                actual_db_parameters["title"] = params['title']
            if key == 'lat':
                actual_db_parameters["latitude"] = float(params['lat'])
            if key == 'lon':
                actual_db_parameters["longitude"] = float(params['lon'])
            if key == 'text':
                actual_db_parameters["text_content"] = validate_user_text_input(params['text'])

        # create the query, it has to be done this way in order to give the information
        # to the middleware in the correct way
        set_string = ""
        set_values = ""
        for key in actual_db_parameters.keys():
            set_string += f"{key} = %s,"
            set_values += f"'{actual_db_parameters[key]}',"

        # set_values and set_string both end on a ',' because at least 1 parameter is given
        # so these parameters can be appended safely at the end of the string
        # add the timestamp and pin_id to the query
        set_values += f"'{timestamp}','{pin_id}'"
        set_string += f"last_edited = %s"

        # send the query to the database
        self.MW.publish_message('middleware-db',
                "UPDATE pins " \
                f"SET {set_string} " \
                f"WHERE pin_id = %s;|({set_values})")
        response = self.MW.get_thread_response()

        return response

    def delete_pin(self, pin_id, user_data):
        """ Deletes a specific pin from the database, pincontent DOES! get affected """

        # Checks if the pin id does exist
        if not self.does_pin_exist(pin_id):
            raise ValueNotFoundError()

        # check if user is authorized for this action
        if user_data.auth_level < 2:
            self.MW.publish_message('middleware-db',
                    "SELECT 1 " \
                    f"FROM pins_per_pin_group WHERE pin_id = %s AND " \
                    f"pin_group_id = %s;|('{pin_id}','{user_data.pin_group_id}')")
            if len(self.MW.get_thread_response()) == 0:
                raise UnauthorizedError()

        self.MW.publish_message('middleware-db',
                "DELETE FROM pins " \
                f"WHERE pin_id = %s;|('{pin_id}',)")
        response = self.MW.get_thread_response()

        return response

    def share_specific_pin(self, pin_id, user_data, params):
        """ Shares a specific pin with a user group or class """

        # Checks if the pin id does exist
        if not self.does_pin_exist(pin_id):
            raise ValueNotFoundError()

        # check if user is authorized for this action
        if user_data.auth_level < 2:
            self.MW.publish_message('middleware-db',
                    "SELECT 1 " \
                    f"FROM pins_per_pin_group WHERE pin_id = %s AND " \
                    f"pin_group_id = %s;|('{pin_id}','{user_data.pin_group_id}')")
            if len(self.MW.get_thread_response()) == 0:
                raise UnauthorizedError()

        # Checks if the group/class id does exist
        # a bit messy but its necessary to prevent db injection
        if params['type'] == 'group':
            self.MW.publish_message('middleware-db',
                "SELECT pin_group_id " \
                "FROM user_groups " \
                f"WHERE user_group_id = %s;|('{params['id']}',)")
        if params['type'] == 'class':
            self.MW.publish_message('middleware-db',
                "SELECT pin_group_id " \
                "FROM classes " \
                f"WHERE class_id = %s;|('{params['id']}',)")
        response = self.MW.get_thread_response()
        if len(response) == 0:
            raise ValueNotFoundError()

        # get the pingroup from the response
        pin_group_id = int(response[0][0])

        # check if the pin is already shared in this group
        self.MW.publish_message('middleware-db',
                "SELECT 1 " \
                "FROM pins_per_pin_group " \
                f"WHERE pin_group_id = %s AND pin_id = %s;|('{pin_group_id}','{pin_id}')")
        response = self.MW.get_thread_response()
        if len(response) > 0:
            return 4

        # insert the pin in the pin group
        self.MW.publish_message('middleware-db',
            f"INSERT INTO pins_per_pin_group VALUES (%s, %s);" \
            f"|('{pin_id}','{pin_group_id}')")
        response = self.MW.get_thread_response()

        return response

    def unshare_specific_pin(self, pin_id, user_data, params):
        """ Unshares a specific pin with a user group or class """

        # Checks if the pin id does exist
        if not self.does_pin_exist(pin_id):
            raise ValueNotFoundError()

        # check if user is authorized for this action
        if user_data.auth_level < 2:
            self.MW.publish_message('middleware-db',
                    "SELECT 1 " \
                    f"FROM pins_per_pin_group WHERE pin_id = %s AND " \
                    f"pin_group_id = %s;|('{pin_id}','{user_data.pin_group_id}')")
            if len(self.MW.get_thread_response()) == 0:
                raise UnauthorizedError()

        # Checks if the group/class id does exist
        # a bit messy but its necessary to prevent db injection
        if params['type'] == 'group':
            self.MW.publish_message('middleware-db',
                "SELECT pin_group_id " \
                "FROM user_groups " \
                f"WHERE user_group_id = %s;|('{params['id']}',)")
        if params['type'] == 'class':
            self.MW.publish_message('middleware-db',
                "SELECT pin_group_id " \
                "FROM classes " \
                f"WHERE class_id = %s;|('{params['id']}',)")
        response = self.MW.get_thread_response()
        if len(response) == 0:
            raise ValueNotFoundError()

        # get the pingroup from the response
        pin_group_id = int(response[0][0])

        # insert the pin in the pin group
        self.MW.publish_message('middleware-db',
            f"DELETE FROM pins_per_pin_group WHERE pin_group_id = %s AND pin_id = %s;" \
            f"|('{pin_group_id}','{pin_id}')")
        response = self.MW.get_thread_response()

        return response

    def download_pin_content(self, pin_id, pin_content_id, user_data):
        """ Checks if the token is authorized to access the requested content """

        # Checks if the pin id does exist
        if not self.does_pin_exist(pin_id):
            raise ValueNotFoundError()

        # check if the user has access to this pin content
        if user_data.auth_level < 2:
            if not self.has_user_access_to_pin(pin_id, user_data):
                raise UnauthorizedError()

        self.MW.publish_message('middleware-db',
                "SELECT content_type, content_nr " \
                "FROM pin_content " \
                f"WHERE pin_content_id = %s AND pin_id = %s;|('{pin_content_id}','{pin_id}')")
        response = self.MW.get_thread_response()

        # check if the content does exist
        if len(response) == 0:
            raise ValueNotFoundError()

        return response[0][0]

    def upload_pin_content(self, pin_id, pin_content_id, user_data, params):
        """ Uploads the pin content and updates the db """

        # Checks if the pin id does exist
        if not self.does_pin_exist(pin_id):
            raise ValueNotFoundError()

        # check if user is authorized for this action
        if user_data.auth_level < 2:
            self.MW.publish_message('middleware-db',
                    "SELECT 1 " \
                    f"FROM pins_per_pin_group WHERE pin_id = %s AND " \
                    f"pin_group_id = %s;|('{pin_id}','{user_data.pin_group_id}')")
            if len(self.MW.get_thread_response()) == 0:
                raise UnauthorizedError()

        # parse request parameters
        content_nr = int(params['content_nr'])
        content_type = params['content_type']

        self.MW.publish_message('middleware-db',
                "INSERT INTO pin_content " \
                f"VALUES (%s, %s, %s, %s); " \
                f"|('{pin_content_id}','{pin_id}','{content_nr}','{content_type}')")
        response = self.MW.get_thread_response()

        return response

    def update_pin_content(self, pin_id, pin_content_id, user_data, params):
        """ Updates the pin content and updates the db """

        # Checks if the pin id does exist
        if not self.does_pin_exist(pin_id):
            raise ValueNotFoundError()
        if not self.does_pin_content_exist(pin_content_id):
            raise ValueNotFoundError()

        # check if user is authorized for this action
        if user_data.auth_level < 2:
            self.MW.publish_message('middleware-db',
                    "SELECT 1 " \
                    f"FROM pins_per_pin_group WHERE pin_id = %s AND " \
                    f"pin_group_id = %s;|('{pin_id}','{user_data.pin_group_id}')")
            if len(self.MW.get_thread_response()) == 0:
                raise UnauthorizedError()

         # if parameters are given, try to update them
        if params:
            # update the pincontent values
            set_string = ""
            set_values = ""
            for key in params.keys():
                set_string += f"{key} = %s,"
                set_values += f"'{params[key]}',"

            # remove the last ',' if only one parameter is given
            # because the middleware is amazing
            set_string = set_string[:-1]
            set_values += f"'{pin_content_id}'"

            self.MW.publish_message('middleware-db',
                    "UPDATE pin_content " \
                    f"SET {set_string} " \
                    f"WHERE pin_content_id = %s;" \
                    f"|({set_values})")
            response = self.MW.get_thread_response()
            return response

        # no parameters given, return 0
        return 0

    def delete_pin_content(self, pin_id, pin_content_id, user_data):
        """ Deletes pin content from the files and database """
        # Checks if the pin id does exist
        if not self.does_pin_exist(pin_id):
            raise ValueNotFoundError()
        # get pin content type and check if it exists
        self.MW.publish_message('middleware-db',
                "SELECT content_type " \
                "FROM pin_content " \
                f"WHERE pin_content_id = %s;|('{pin_content_id}',)")
        response = self.MW.get_thread_response()
        if len(response) == 0:
            raise ValueNotFoundError()
        content_type = response[0][0]

        # check if user is authorized for this action
        if user_data.auth_level < 2:
            self.MW.publish_message('middleware-db',
                    "SELECT 1 " \
                    f"FROM pins_per_pin_group WHERE pin_id = %s AND " \
                    f"pin_group_id = %s;|('{pin_id}','{user_data.pin_group_id}')")
            if len(self.MW.get_thread_response()) == 0:
                raise UnauthorizedError()

        self.MW.publish_message('middleware-db',
            "DELETE FROM pin_content " \
            f"WHERE pin_content_id = %s;|('{pin_content_id}',)")
        response = self.MW.get_thread_response()

        return content_type

    def has_user_access_to_pin(self, pin_id, user_data):
        """ tests if the user has access to pin_id """

        # check if user is authorized for this action
        # a auth 1 or 2 has access to a pin when:
        # the pin is in the personal pin group of the user
        # the user is in a user-group which has the pin
        # the user is part of a class which has the pin
        if user_data.auth_level < 2:
            # get all the pin groups the pin_id is in
            self.MW.publish_message('middleware-db',
                    "SELECT pin_group_id " \
                    f"FROM pins_per_pin_group WHERE pin_id = %s;|('{pin_id}',)")
            resp = self.MW.get_thread_response()
            pin_groups = []
            for group_id in resp:
                pin_groups.append(group_id[0])

            # get all the pin groups the user has access to
            # class pin group, user-group pin groups, user pin group
            user_pin_groups = []
            user_pin_groups.append(user_data.pin_group_id)
            self.MW.publish_message('middleware-db',
                    "SELECT pin_group_id " \
                    f"FROM classes WHERE class_id = %s;|('{user_data.class_id}',)")
            resp = self.MW.get_thread_response()
            if len(resp) != 0:
                user_pin_groups.append(resp[0][0])
            self.MW.publish_message('middleware-db',
                    "SELECT pin_group_id " \
                    f"FROM (user_groups AS ug INNER JOIN users_per_user_group AS upug ON " \
                    f"upug.user_id = %s AND " \
                    f"ug.user_group_id = upug.user_group_id);|('{user_data.id}',)")
            resp = self.MW.get_thread_response()
            if len(resp) != 0:
                user_pin_groups.append(resp[0][0])

            # return false if the user does not has access
            if not any(item in pin_groups for item in user_pin_groups):
                return False

        # the user has acces, return true
        return True

    def get_user_data(self, access_token):
        """ gets user data associated with the token """

        # return if no token is supplied
        if access_token is None:
            raise TokenError()

        # prepare the bare msg to the auth service
        body = UserInfoRequest()
        body.access_token = access_token
        request = AuthRequest()
        request.request_type = RequestType.USER_INFO_REQUEST
        request.request_body = body.pack()

        # do the request
        self.MW.publish_message('auth', request.pack())
        response = self.MW.get_thread_binary_response()
        response = AuthResponse.unpack(response)

        # check the response
        if response.response_type == ResponseType.ERROR_RESPONSE:
            raise TokenError()

        user_info = UserInfo.unpack(response.response_body)
        return user_info

    def get_user_data_by_id(self, user_id):
        """ gets user data associated with a user id """
        # prepare the bare request body
        body = UserInfoRequest()
        body.user_id = user_id
        request = AuthRequest()
        request.request_type = RequestType.USER_INFO_REQUEST
        request.request_body = body.pack()

        # do the request
        self.MW.publish_message('auth', request.pack())
        response = self.MW.get_thread_binary_response()
        response = AuthResponse.unpack(response)

        if response.response_type == ResponseType.ERROR_RESPONSE:
            raise BadRequestError()

        user_info = UserInfo.unpack(response.response_body)
        return user_info

    def does_pin_exist(self, pin_id):
        """ Checks if the pin id does exist """

        self.MW.publish_message('middleware-db',
                "SELECT 1 " \
                "FROM pins " \
                f"WHERE pin_id = %s;|('{pin_id}',)")
        response = self.MW.get_thread_response()
        if len(response) == 0:
            return False
        return True

    def is_pin_id_valid(self, pk):
        """ checks if pk exists in the pin database, usefull for generating unique ids """

        self.MW.publish_message('middleware-db',
            f"SELECT pin_id FROM pins WHERE pin_id = %s;|('{pk}',)")

        response = self.MW.get_thread_response()
        # if the response is null it means the primary key does NOT exist in the database
        if len(response) == 0:
            return True
        return False

    def does_pin_content_exist(self, pin_content_id):
        """ Checks if the pin id does exist """

        self.MW.publish_message('middleware-db',
                "SELECT 1 " \
                "FROM pin_content " \
                f"WHERE pin_content_id = %s;|('{pin_content_id}',)")
        response = self.MW.get_thread_response()
        if len(response) == 0:
            return False
        return True

    def is_pin_content_id_valid(self, pk):
        """ checks if pk exists in the pin_content database, usefull for generating unique ids """

        self.MW.publish_message('middleware-db',
            f"SELECT pin_content_id FROM pin_content WHERE pin_content_id = %s;|('{pk}',)")

        response = self.MW.get_thread_response()
        # if the response is null it means the primary key does NOT exist in the database
        if len(response) == 0:
            return True
        return False

    def create_pin_group(self, group_id, name):
        """ inserts a new pin group into the database """

        # insert the new pin group into the database
        self.MW.publish_message('middleware-db',
            f"INSERT INTO pin_groups VALUES (%s, %s);" \
            f"|('{group_id}','{name}')")

        resp = self.MW.get_thread_response()

        return resp

    def delete_pin_group(self, group_id):
        """ deletes an existing pin group """

        self.MW.publish_message('middleware-db',
            f"DELETE FROM pin_groups WHERE pin_group_id = %s;|('{group_id}',)")

        resp = self.MW.get_thread_response()

        return resp

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
