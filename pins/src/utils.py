"""
Miscellaneous util functions
"""

from json import dumps
from secrets import randbits
import datetime

def generate_random_id():
    """ generates a random int64 """
    return randbits(63)

def get_timestamp():
    """ generates a timestamp """
    return int(datetime.datetime.utcnow().timestamp())

def get_JWT(headers):
    """Obtains the JWT from the request headers"""

    # get the JWT from the header
    if 'authorization' not in headers:
        return None
    authorization = headers['authorization']

    # make sure the JWT is valid
    if not authorization.startswith('Bearer '):
        return None

    # cut off the prefix
    return authorization[7:]

def simple_json_response(status_code, message, headers={}):
    """
    Generate a simple JSON response with a status code, message and headers.

    :param int status_code: the status code
    :param str message: the message
    :param dict headers: the headers
    :return tuple: http data for response
    """

    # match the correct status to the status code
    if 200 <= status_code <= 299:
        status = 'success'
    elif 400 <= status_code <= 599:
        status = 'error'
    else:
        raise NotImplementedError(f'Status code not supported: {status_code}')

    # format a simple response
    response = dumps({status: message})

    # set the application type
    headers['Content-Type'] = 'application/json'

    return response, status_code, headers

def validate_user_text_input(input):
    """ Prevents user from escaping an input string """
    return input.replace("'", "\\'")

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
