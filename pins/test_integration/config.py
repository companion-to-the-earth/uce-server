"""Config for itests"""

# host
host = 'http://pins:5000/pins'
jwt = ''
token_password_valid = {
    'grant_type': 'password',
    'email': 'admin@uce.nl',
    'password': '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8'
}

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
