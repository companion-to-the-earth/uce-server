""""Test module that tests connections to the pins service"""

import unittest
import requests
from random import randint, random
from json import loads, dumps
from time import sleep

from . import config

class TestPins(unittest.TestCase):
    """Test dynamic content"""

    @classmethod
    def setUpClass(cls):
        """Wait 20 seconds to allow for setup"""
        sleep(20)

        res = requests.post('http://auth:5000/auth/token', json=config.token_password_valid)
        config.jwt = "Bearer " + res.json()['access_token']

    def test_query_correct_class(self):
        """Test if query of an existing class works"""

        r = requests.get(f'{config.host}/', params={'class': '10000'},
            headers={"authorization":config.jwt})

        self.assertEqual(r.status_code, 200)

    def test_query_correct_class_all(self):
        """Test if query of an existing class works"""

        r = requests.get(f'{config.host}/', params={'class_all': '10000'},
            headers={"authorization":config.jwt})

        self.assertEqual(r.status_code, 200)

    def test_query_correct_group(self):
        """Test if query of an existing group works"""

        r = requests.get(f'{config.host}/', params={'group': '1000'},
            headers={"authorization":config.jwt})

        self.assertEqual(r.status_code, 200)

    def test_query_no_params(self):
        """Test if query without params returns 400"""

        r = requests.get(f'{config.host}/', params={},
            headers={"authorization":config.jwt})

        self.assertEqual(r.status_code, 400)

    def test_query_too_many_params(self):
        """Test if query with too many params returns 400"""

        r = requests.get(f'{config.host}/', params={'group': '1000', 'class': '10000'},
            headers={"authorization":config.jwt})

        self.assertEqual(r.status_code, 400)

    def test_query_invalid_params(self):
        """Test if query with invalid params returns 400"""

        r = requests.get(f'{config.host}/', params={'kaas': '1000'},
            headers={"authorization":config.jwt})

        #self.assertIn('JWT missing or malformed', r.json().values())
        self.assertEqual(r.status_code, 400)

    def test_upload_pin_correctly(self):
        """Test if uploading a pin the correct way returns 200"""

        r = requests.post(f'{config.host}/', json={
            "title": "test insert pin",
            "lat": 1.986184,
            "lon": -157.477973,
            'text': 'hoi'},
            headers={"authorization":config.jwt})

        self.assertEqual(r.status_code, 200)

    def test_upload_pin_no_json(self):
        """Test if uploading a pin without json returns 200"""

        r = requests.post(f'{config.host}/',
            headers={"authorization":config.jwt})

        self.assertEqual(r.status_code, 400)

    def test_upload_pin_wrong_parameter_type(self):
        """Test if uploading a pin with a scientificely notated string latitude returns 200 """

        r = requests.post(f'{config.host}/', json={
            "title": "test insert pin",
            "lat": "0.123745e3",
            "lon": -157.477973,
            'text': 'hoi'},
            headers={"authorization":config.jwt})

        self.assertEqual(r.status_code, 200)

    def test_queryid_correct_query(self):
        """Test if a correct query of a specific pin returns 200 """

        r = requests.get(f'{config.host}/10',
            headers={"authorization":config.jwt})

        self.assertEqual(r.status_code, 200)

    def test_queryid_invalid_id_type(self):
        """Test if a specific query with an id of an invalid type returns 400 """

        r = requests.get(f'{config.host}/kaas',
            headers={"authorization":config.jwt})

        self.assertEqual(r.status_code, 400)

    def test_queryid_id_does_not_exist(self):
        """Test if a specific query for a pin that does not exist returns 400 """

        r = requests.get(f'{config.host}/9',
            headers={"authorization":config.jwt})

        self.assertEqual(r.status_code, 400)

    def test_update_pinid_correct_query(self):
        """ Test if a correct update query returns 200 """

        r = requests.put(f'{config.host}/10', json={
            "title": str(randint(0, 100000)),
            "lat": random(),
            "lon": random(),
            "text": 'hoi'},
            headers={"authorization":config.jwt})

        self.assertEqual(r.status_code, 200)

    def test_update_pinid_no_correct_params(self):
        """ Test if an update query without any valid parameters returns 400 """

        r = requests.put(f'{config.host}/10', json={
            "title3": "Hai",
            "latt": random(),
            "lotn": random()},
            headers={"authorization":config.jwt})

        self.assertEqual(r.status_code, 400)

    def test_update_pinid_incorrect_type(self):
        """ Test if an update query with incorrect parameter types returns 400 """

        r = requests.put(f'{config.host}/10', json={
            "title": 123123,
            "lat": "193.296432",
            "lon": random()},
            headers={"authorization":config.jwt})

        self.assertEqual(r.status_code, 400)

    def test_update_pinid_id_does_not_exist(self):
        """ Test if an update query on a non existing pinid returns 400 """

        r = requests.put(f'{config.host}/9', json={
            "title3": "new title",
            "latt": random(),
            "lotn": random()},
            headers={"authorization":config.jwt})

        self.assertEqual(r.status_code, 400)

    def test_delete_pin_correct_query(self):
        """ Test if a correct query deletes the pin """
        # first upload a pin so we can delete it immediately
        upload = requests.post(f'{config.host}/', json={
            "title": "temporary",
            "lat": 1.986184,
            "lon": -157.477973,
            "text": "hallo testpin"},
            headers={"authorization":config.jwt})
        jsonresponse = upload.json()
        pin_id = jsonresponse['id']
        r = requests.delete(f'{config.host}/{pin_id}',
            headers={"authorization":config.jwt})

        self.assertEqual(r.status_code, 200)

    def test_delete_pin_pin_doesnt_exist(self):
        """ Test if a correct query deletes the pin """
        r = requests.delete(f'{config.host}/9',
            headers={"authorization":config.jwt})

        self.assertEqual(r.status_code, 400)

    def test_share_pin_correctly(self):
        """ Tests if a pin can be shared correctly """
        r = requests.post(f'{config.host}/40/share', json={
            "type": "class",
            "id": 10000},
            headers={"authorization":config.jwt})

        self.assertEqual(r.status_code, 200)

    def test_unshare_pin_correctly(self):
        """ Tests if a pin can be shared correctly """

        requests.post(f'{config.host}/40/share', json={
            "type": "class",
            "id": 10000},
            headers={"authorization":config.jwt})

        r = requests.post(f'{config.host}/40/unshare', json={
            "type": "class",
            "id": 10000},
            headers={"authorization":config.jwt})

        self.assertEqual(r.status_code, 200)

    def test_upload_pin_content(self):
        """ Test uploading of pin content """

        testfile = open("test.txt","w+")
        testfile.write("Mooie quiz.\n")
        testfile.close()

        pin_content = { "content_type": "quiz", "content_nr": 6 }
        uploadform = {"pin_content": dumps(pin_content)}
        uploadfile = {"file": ("test.txt", open("test.txt", "r"), 'text/plain')}

        with self.subTest("correct upload"):
            # test a correct upload of a file
            r = requests.post(f'{config.host}/10/contents',
                data=uploadform,
                files=uploadfile,
                headers={"authorization":config.jwt})

            self.assertEqual(r.status_code, 201)

        with self.subTest("no file attached"):
            # test an upload with no file attached
            r = requests.post(f'{config.host}/10/contents',
                data=uploadform,
                headers={"authorization":config.jwt})

            self.assertEqual(r.status_code, 400)

        with self.subTest("pin does not exist"):
            # test an upload where the given pin does not exist
            r = requests.post(f'{config.host}/11/contents',
                data=uploadform,
                files=uploadfile,
                headers={"authorization":config.jwt})

            self.assertEqual(r.status_code, 400)

        with self.subTest("illegal file type"):
            # test if a file of a non-whitelisted type gets denied
            illegalfile = open("test.blub","w+")
            illegalfile.write("Mooie quiz.\n")
            illegalfile.close()

            readfile = open("test.blub", "r")
            uploadfile = {"file": ("test.blub", readfile, 'test/blub')}

            r = requests.post(f'{config.host}/10/contents',
                data=uploadform,
                files=uploadfile,
                headers={"authorization":config.jwt})

            self.assertEqual(r.status_code, 400)

        with self.subTest("illegal content type"):
            # test if an illegal content type gets denied
            illegal_pin_content = { "content_type": "blubblublub", "content_nr": 6 }
            illegal_uploadform = {"pin_content": dumps(illegal_pin_content)}

            r = requests.post(f'{config.host}/10/contents',
                data=illegal_uploadform,
                files=uploadfile,
                headers={"authorization":config.jwt})

            self.assertEqual(r.status_code, 400)

    def test_download_pin_content(self):
        """ Test downloading of pin content """

        with self.subTest("correct download"):
            # test a correct upload of a file
            r = requests.get(f'{config.host}/10/contents/10',
                headers={"authorization":config.jwt})

            self.assertEqual(r.status_code, 200)

        with self.subTest("download content does not exist"):
            # test a correct upload of a file
            r = requests.get(f'{config.host}/10/contents/11',
                headers={"authorization":config.jwt})

            self.assertEqual(r.status_code, 400)

        with self.subTest("download wrong pin id"):
            # test a correct upload of a file
            r = requests.get(f'{config.host}/11/contents/10',
                headers={"authorization":config.jwt})

            self.assertEqual(r.status_code, 400)

    def test_update_pin_content(self):
        """ Test updating of pin content """

        testfile = open("test.txt","w+")
        testfile.write("Mooie quiz.\n")
        testfile.close()

        pin_content = { "content_type": "quiz", "content_nr": 6 }
        uploadform = {"pin_content": dumps(pin_content)}
        uploadfile = {"file": ("test.txt", open("test.txt", "r"), 'text/plain')}

        with self.subTest("correct update"):
            # test a correct update of pincontent
            r = requests.put(f'{config.host}/10/contents/10',
                headers={"authorization":config.jwt},
                data=uploadform,
                files=uploadfile)

            self.assertEqual(r.status_code, 200)

        with self.subTest("correct update only file"):
            # test a correct update of pincontent
            r = requests.put(f'{config.host}/10/contents/10',
                headers={"authorization":config.jwt},
                files=uploadfile)

            self.assertEqual(r.status_code, 200)

        with self.subTest("correct update only form"):
            # test a correct update of pincontent
            r = requests.put(f'{config.host}/10/contents/10',
                headers={"authorization":config.jwt},
                data=uploadform)

            self.assertEqual(r.status_code, 200)

        with self.subTest("update pincontent no parameters or file supplied"):
            # test a correct update of pincontent
            r = requests.put(f'{config.host}/10/contents/10',
                headers={"authorization":config.jwt})

            self.assertEqual(r.status_code, 400)

    def test_z_delete_pin_content(self):
        """ Test deleting of pin content """

        testfile = open("test.txt","w+")
        testfile.write("Mooie quiz.\n")
        testfile.close()

        pin_content = { "content_type": "quiz", "content_nr": 6 }
        uploadform = {"pin_content": dumps(pin_content)}
        uploadfile = {"file": ("test.txt", open("test.txt", "r"), 'text/plain')}

        with self.subTest("delete pincontent correctly"):
            r = requests.delete(f'{config.host}/10/contents/20',
                headers={"authorization":config.jwt})

            self.assertEqual(r.status_code, 200)

        with self.subTest("delete pincontent id does not exist"):
            r = requests.delete(f'{config.host}/10/contents/21',
                headers={"authorization":config.jwt})

            self.assertEqual(r.status_code, 400)

    def test_internal_pin_group(self):
        """ Test internal endpoint to create and delete a pin group """

        res = requests.post(f'http://pins:5000/internal/pin-groups',
                            json={'name': 'test'})

        self.assertEqual(res.status_code, 201)

        pin_group_id = res.json()['id']
        self.assertIsInstance(pin_group_id, int)

        res = requests.delete(f'http://pins:5000/internal/pin-groups/{pin_group_id}')

        self.assertEqual(res.status_code, 200)

if __name__ == '__main__':
    unittest.main()

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
