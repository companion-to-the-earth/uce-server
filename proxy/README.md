# Utrecht Companion To The Earth - proxy

## usage
Reverse proxy connecting the user to the relevant webservices. Static content is also served here, as well as protected files. See `download/` for details on the authentication.

## used resources
- nginx: <https://www.nginx.com/>
