##
## uce-server vhost
## This file will be copied to /etc/nginx/conf.d/ after substituting environment
## variables. This is a feature of the nginx docker image, not nginx itself.
##
## Do not configure SSL in this file, this is handled by certbot.
## Certbot will add the configuration to listen on port 443 and redirect all
## HTTP requests to HTTPS.
##

server {
    listen 80;                  # ipv4
    # listen [::]:80;           # ipv6, disabled because it is not supported by our server from the uni
    server_name ${DOMAIN_NAME}; # get domain name from environment variable

    # Static content
    location /static {
        include /etc/nginx/include/security-headers.conf;
        include /etc/nginx/include/misc-headers.conf;
        root /var/www;
        index index.html;
    }

    # Account verification
    location /verify {
        include /etc/nginx/include/security-headers.conf;
        include /etc/nginx/include/misc-headers.conf;
        include /etc/nginx/include/cors-preflight.conf;
        limit_except GET POST { deny all; }
        proxy_pass http://auth:5000/verify;
    }

    # Password reset
    location /reset-password {
        include /etc/nginx/include/security-headers.conf;
        include /etc/nginx/include/misc-headers.conf;
        include /etc/nginx/include/cors-preflight.conf;
        limit_except GET POST { deny all; }
        proxy_pass http://auth:5000/reset-password;
    }

    # API endpoints
    location /api {

        include /etc/nginx/include/cors.conf; # include CORS headers
        include /etc/nginx/include/security-headers.conf; # incude security headers
        include /etc/nginx/include/misc-headers.conf;

        # -STATUS-
        location /api/status {
            limit_except GET { deny all; }
            default_type application/json;
            return 200 '{"status":"online", "version":"${VERSION}"}';
        }
        
        # -PINS-
        location /api/pins/ {
            include /etc/nginx/include/cors-preflight.conf;
            limit_except GET POST PUT DELETE OPTIONS { deny all; }
            proxy_pass http://pins:5000/pins/;
        }

        # -AUTH-
        location /api/auth/ {
            include /etc/nginx/include/cors-preflight.conf;
            limit_except GET POST DELETE OPTIONS { deny all; }
            proxy_pass http://auth:5000/auth/;
        }

        # -USER-
        location /api/users/ {
            include /etc/nginx/include/cors-preflight.conf;
            limit_except GET POST PUT DELETE OPTIONS { deny all; }
            proxy_pass http://auth:5000/users/;
        }

        # -LIBRARY-
        location /api/books/ {
            include /etc/nginx/include/cors-preflight.conf;
            limit_except GET POST PUT DELETE OPTIONS { deny all; }
            proxy_pass http://library:5000/books/;
        }

        # -GROUP_MANAGEMENT-
        location /api/universities/ {
            include /etc/nginx/include/cors-preflight.conf;
            limit_except GET POST DELETE PUT OPTIONS { deny all; }
            proxy_pass http://group_management:5000/universities/;
        }

        location /api/courses/ {
            include /etc/nginx/include/cors-preflight.conf;
            limit_except GET POST DELETE PUT OPTIONS { deny all; }
            proxy_pass http://group_management:5000/courses/;
        }

        location /api/classes/ {
            include /etc/nginx/include/cors-preflight.conf;
            limit_except GET POST DELETE PUT OPTIONS { deny all; }
            proxy_pass http://group_management:5000/classes/;
        }

        location /api/user-groups/ {
            include /etc/nginx/include/cors-preflight.conf;
            limit_except GET POST DELETE PUT OPTIONS { deny all; }
            proxy_pass http://group_management:5000/user-groups/;
        }
    }
}
