#!/bin/sh

#
# script to start nginx and certbot
# when USE_CERTBOT is set to 1, certbot is used to request SSL certificates
# otherwise, this step is ignored and only nginx is started
#
# based on: https://github.com/JonasAlfredsson/docker-nginx-certbot/blob/master/src/scripts/start_nginx_certbot.sh
#

echo "Running start_nginx_certbot.sh"

echo "Starting nginx"
# start nginx and get pid
nginx -g "daemon off;" &
NGINX_PID=$!

# check if certbot should be started
if [ "${USE_CERTBOT}" -eq 1 ]; then
    if [ -z "${CERTBOT_EMAIL}" ]; then
        echo "CERTBOT_EMAIL not set, registering without email (not recommended)"
        email_option="--register-unsafely-without-email"
    else
        email_option="--email ${CERTBOT_EMAIL}"
    fi

    echo "Running certbot"
    certbot \
        --non-interactive \
        --agree-tos \
        $email_option \
        --keep-until-expiring \
        --nginx \
        --domains "${DOMAIN_NAME}"

    echo "Add certbot renew cron job"
    echo -e "12	2	*	*	*	certbot renew -q" >> /etc/crontabs/root

    echo "Starting crond"
    crond -b -L "/var/log/crond"
fi

# gracefully shut down nginx when requested
clean_exit() {
    echo "Shutting down"
    if kill -0 ${NGINX_PID} 2>/dev/null; then # if NGINX_PID is valid
        kill -SIGTERM "${NGINX_PID}"          # send SIGTERM to nginx
        wait ${NGINX_PID}                     # wait for it to exit
        exit 0
    fi
}

trap "clean_exit" TERM INT QUIT # trigger clean_exit on SIGTERM, SIGINT, SIGQUIT

# wait for nginx to exit, then exit with the same exit code
wait ${NGINX_PID}
exit_code=$?
exit ${exit_code}

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
