"""
Config for end2end tests
"""

from os import popen

# the domain where nginx can be accessed
HOST_IP = popen('ip -4 route list match 0/0 | cut -d\' \' -f3').read()[:-1]
HOST = f"http://{HOST_IP}:8080"

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
