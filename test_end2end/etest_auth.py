"""
Auth service end2end tests
"""

import logging
import unittest
import threading
from time import sleep
import requests
from test_end2end import config

class TestAuth(unittest.TestCase):
    """
    Test auth service
    """

    @classmethod
    def setUpClass(cls):
        """
        Wait 30 seconds to allow for setup
        """
        logging.basicConfig(format="%(module)s %(levelname)s | %(message)s", level=logging.INFO)

        logging.info("Waiting 30 seconds before executing tests")
        sleep(30)
        logging.info("Starting tests")

    def test_password_stress(self):
        """
        Password grant stress test
        """
        def login(results, index):
            url = f"{config.HOST}/api/auth/token"
            login_data = {
                'grant_type': 'password',
                'email': 'admin@uce.nl',
                'password': '5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8'
            }
            res = requests.post(url, json=login_data)
            results[index] = res.status_code

        thread_count = 100
        threads = [None] * thread_count
        results = [None] * thread_count
        for index in range(thread_count):
            threads[index] = threading.Thread(target=login, args=(results, index))
        for thread in threads:
            thread.start()
        for thread in threads:
            thread.join()

        results_expected = [200] * thread_count
        self.assertEqual(results, results_expected)

if __name__ == "__main__":
    unittest.main()

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
